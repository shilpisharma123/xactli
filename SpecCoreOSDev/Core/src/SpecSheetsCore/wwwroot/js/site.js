﻿//var ReportColorRGB = { Red: 255, Green: 192, Blue: 0 };
var ReportColorRGB = { Red: 233, Green: 175, Blue: 0 };
var WhiteRGB = {Red: 255, Green: 255, Blue: 255};

var ReportsLibrary = {

	totalPagesExp: "{total_pages_count_string}",

	ReportTradesByLocation: function (filterText, project) {

		var dateTime = moment().format('LLLL');

		var doc = new jsPDF('p', 'pt');
		var columns = ["Material", "Cost", "Quantity", "Estimate"];

		var rows = [];
		var grandTotal = 0;

		for (var i = 0; i < project.locations.length; i++) {

			var location = project.locations[i];
			rows.push(["Location", location.locationName, "", ""]);

			var locationTotal = { cost: 0, quantity: 0, estimate: 0 };
			for (var j = 0; j < location.trades.length; j++) {
				var trade = location.trades[j];
				rows.push(["Trade", trade.tradeName, "", ""]);

				var tradeTotal = { cost: 0, quantity: 0, estimate: 0 };
				for (var k = 0; k < trade.materials.length; k++) {

					var material = trade.materials[k];

					var desc1 = (material.description && material.description.length > 0) ? material.description.slice(0, 48) : '';
					var desc2 = (material.description && material.description.length > 0) ? material.description.slice(48) : '';

					rows.push(["Material", material.materialName, [material.cost, material.defaultQuantity, material.estimate], ""]);
					rows.push(["Manufacturer", material.manufacturer, desc1, ""]);
					rows.push(["Model", material.model, desc2, ""]);

					tradeTotal.cost += material.cost;
					tradeTotal.quantity += material.defaultQuantity;
					tradeTotal.estimate += material.estimate;
				}

				locationTotal.cost += tradeTotal.cost;
				locationTotal.quantity += tradeTotal.quantity;
				locationTotal.estimate += tradeTotal.estimate;

				var TradeSummaryText = "Trade summary for " + trade.tradeName;
				rows.push(["Trade Summary Total", TradeSummaryText, [tradeTotal.cost, tradeTotal.quantity, tradeTotal.estimate], ""]);
				grandTotal += tradeTotal.estimate;
			}

			var LocationSummaryText = "Location summary for " + location.locationName;
			rows.push(["Location Summary Total", LocationSummaryText, [locationTotal.cost, locationTotal.quantity, locationTotal.estimate], ""]);
			//grandTotal += tradeTotal.estimate;
		}

		rows.push(["Grand Total", "$" + grandTotal, "", ""]);

		var HeaderInformation = {
			doc: doc,
			ReportTitle: filterText,
			projectId: project.projectName,
			projectOwner: project.ownerName,
			address: project.address,
			phone: project.phone
		};

		// HEADER
		ReportsLibrary.MainReportHeader(HeaderInformation);

		var pageContent = function (data) {

			// FOOTER
			var pageNumber = "Page " + data.pageCount;

			if (typeof doc.putTotalPages === 'function') {
				pageNumber = pageNumber + " of " + ReportsLibrary.totalPagesExp;
			}

			ReportsLibrary.Footer(doc, pageNumber, dateTime);
		};

		doc.autoTable(columns, rows, {
			addPageContent: pageContent,
			startY: 80,
			headerStyles: { fillColor: [ReportColorRGB.Red, ReportColorRGB.Green, ReportColorRGB.Blue] },
			columnStyles: {
				0: {columnWidth: 290},
				1: {columnWidth: 75},
				2: {columnWidth: 75},
				3: {columnWidth: 75}
			},
			margin: { top: 30 },

			drawCell: function (cell, data) {

				// Rowspan

				var locationText = "Location";
				if (data.row.cells[0].raw == locationText) {
					if (data.column.dataKey === 0) {

						doc.setFillColor(ReportColorRGB.Red, ReportColorRGB.Green, ReportColorRGB.Blue);
						doc.rect(data.settings.margin.left + 0.5, data.row.y, data.table.width - 0.5, 20, 'F');

						doc.setTextColor(WhiteRGB.Red, WhiteRGB.Green, WhiteRGB.Blue);
						doc.autoTableText(locationText, cell.x + 5, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});

						doc.setFillColor(WhiteRGB.Red, WhiteRGB.Green, WhiteRGB.Blue);
						doc.rect(data.settings.margin.left + 55, data.row.y + 3, data.table.width - 250, 13, 'F');

						doc.setTextColor(ReportColorRGB.Red, ReportColorRGB.Green, ReportColorRGB.Blue);
						doc.autoTableText(data.row.cells[1].raw, cell.x + 65, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});
					}

					return false;
				}

				var tradeText = "Trade";
				if (data.row.cells[0].raw == tradeText) {
					if (data.column.dataKey === 0) {

						doc.setDrawColor(ReportColorRGB.Red, ReportColorRGB.Green, ReportColorRGB.Blue);
						doc.setFillColor(ReportColorRGB.Red, ReportColorRGB.Green, ReportColorRGB.Blue);
						doc.rect(data.settings.margin.left + 0.5, data.row.y - 2, data.table.width - 0.5, 20, 'F');

						doc.setTextColor(WhiteRGB.Red, WhiteRGB.Green, WhiteRGB.Blue);
						doc.autoTableText(tradeText, cell.x + 60, cell.y - 2 + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});

						doc.setFillColor(WhiteRGB.Red, WhiteRGB.Green, WhiteRGB.Blue);
						doc.rect(data.settings.margin.left + 100, data.row.y + 1, data.table.width - 250, 13, 'F');

						doc.setTextColor(ReportColorRGB.Red, ReportColorRGB.Green, ReportColorRGB.Blue);
						doc.autoTableText(data.row.cells[1].raw, cell.x + 110, cell.y - 2 + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});
					}

					return false;
				}

				var materialText = "Material";
				if (data.row.cells[0].raw == materialText) {
					if (data.column.dataKey === 0) {

						var materialName = data.row.cells[1].raw;
						doc.autoTableText(materialName, cell.x + 60, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});

						var materialDetails = data.row.cells[2].raw;

						var materialCost = "$" + materialDetails[0];
						doc.autoTableText(materialCost, cell.x + 300, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});

						var materialQuantity = materialDetails[1] + "";
						doc.autoTableText(materialQuantity, cell.x + 380, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});

						var materialEstimate = materialDetails[2] + "";
						doc.autoTableText(materialEstimate, cell.x + 460, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});
					}

					return false;
				}

				var manufacturer = "Manufacturer";
				if (data.row.cells[0].raw == manufacturer) {
					if (data.column.dataKey === 0) {

						doc.setTextColor(128, 128, 128);
						doc.autoTableText(manufacturer, cell.x + 80, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});

						var manufacturerName = data.row.cells[1].raw;

						doc.setTextColor(100);
						doc.autoTableText(manufacturerName, cell.x + 200, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});

						var description = data.row.cells[2].raw;

						doc.setTextColor(100);
						doc.autoTableText(description, cell.x + 300, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});
					}

					return false;
				}

				var model = "Model";
				if (data.row.cells[0].raw == model) {
					if (data.column.dataKey === 0) {

						doc.setTextColor(128, 128, 128);
						doc.autoTableText(model, cell.x + 80, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});

						var modelName = data.row.cells[1].raw;

						doc.setTextColor(100);
						doc.autoTableText(modelName, cell.x + 200, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});

						var description = data.row.cells[2].raw;

						doc.setTextColor(100);
						doc.autoTableText(description, cell.x + 300, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});
					}

					return false;
				}

				var tradeSummaryTotal = "Trade Summary Total";
				if (data.row.cells[0].raw == tradeSummaryTotal) {
					if (data.column.dataKey === 0) {

						doc.setLineWidth(2);
						doc.line(cell.x + 60, cell.y + 2, cell.x + 500, cell.y + 2);
						doc.line(cell.x + 60, cell.y + cell.height - 2, cell.x + 500, cell.y + cell.height - 2);

						doc.autoTableText(data.row.cells[1].raw, cell.x + 60, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});

						var tradeDetails = data.row.cells[2].raw;
						var tradeTotal = "$" + tradeDetails[2];
						doc.autoTableText(tradeTotal, cell.x + 450, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});
					}

					return false;
				}

				var locationSummaryTotal = "Location Summary Total";
				if (data.row.cells[0].raw == locationSummaryTotal) {
					if (data.column.dataKey === 0) {

						doc.setLineWidth(2);
						doc.line(cell.x + 60, cell.y + 2, cell.x + 500, cell.y + 2);
						doc.line(cell.x + 60, cell.y + cell.height - 2, cell.x + 500, cell.y + cell.height - 2);

						doc.autoTableText(data.row.cells[1].raw, cell.x + 60, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});

						var tradeDetails = data.row.cells[2].raw;
						var tradeTotal = "$" + tradeDetails[2];
						doc.autoTableText(tradeTotal, cell.x + 450, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});
					}

					return false;
				}

				var grandTotal = "Grand Total";
				if (data.row.cells[0].raw == grandTotal) {
					if (data.column.dataKey === 0) {

						doc.setTextColor(128, 128, 128);
						doc.autoTableText(grandTotal, cell.x, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});

						doc.rect(data.settings.margin.left + 420, data.row.y + 3, data.table.width - 460, 13, 'S');

						doc.setTextColor(0);
						doc.autoTableText(data.row.cells[1].raw, cell.x + 440, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});
					}

					return false;
				}
			}
		});

		if (typeof doc.putTotalPages === 'function') {
			doc.putTotalPages(ReportsLibrary.totalPagesExp);
		}

		doc.save(filterText + '.pdf');
	},

	ReportLocationsByTrade: function (filterText, project) {

		var dateTime = moment().format('LLLL');

		var doc = new jsPDF('p', 'pt');
		var columns = ["Material", "Cost", "Quantity", "Estimate"];

		var rows = [];
		var grandTotal = 0;
		for (var i = 0; i < project.trades.length; i++) {
		
			var trade = project.trades[i];
			rows.push(["Trade", trade.tradeName, "", ""]);

			var tradeTotal = { cost: 0, quantity: 0, estimate: 0 };
			for (var j = 0; j < trade.locations.length; j++) {
				var location = trade.locations[j];
				rows.push(["Location", location.locationName, "", ""]);

				var locationTotal = { cost: 0, quantity: 0, estimate: 0 };
				for (var k = 0; k < location.materials.length; k++) {

					var material = location.materials[k];

					var desc1 = (material.description && material.description.length > 0) ? material.description.slice(0, 48) : '';
					var desc2 = (material.description && material.description.length > 0) ? material.description.slice(48) : '';

					rows.push(["Material", material.materialName, [material.cost, material.defaultQuantity, material.estimate], ""]);
					rows.push(["Manufacturer", material.manufacturer, desc1, ""]);
					rows.push(["Model", material.model, desc2, ""]);

					locationTotal.cost += material.cost;
					locationTotal.quantity += material.defaultQuantity;
					locationTotal.estimate += material.estimate;
				}

				tradeTotal.cost += locationTotal.cost;
				tradeTotal.quantity += locationTotal.quantity;
				tradeTotal.estimate += locationTotal.estimate;

				var LocationSummaryText = "Location Summary for " + location.locationName;
				rows.push(["Location Summary Total", LocationSummaryText, [locationTotal.cost, locationTotal.quantity, locationTotal.estimate], ""]);
				grandTotal += locationTotal.estimate;
			}

			var TradeSummaryText = "Trade summary for " + trade.tradeName;
			rows.push(["Trade Summary Total", TradeSummaryText, [tradeTotal.cost, tradeTotal.quantity, tradeTotal.estimate], ""]);
			grandTotal += tradeTotal.estimate;
		}

		rows.push(["Grand Total", "$" + grandTotal, "", ""]);

		var HeaderInformation = {
			doc: doc,
			ReportTitle: filterText,
			projectId: project.projectName,
			projectOwner: project.ownerName,
			address: project.address,
			phone: project.phone
		};

		// HEADER
		ReportsLibrary.MainReportHeader(HeaderInformation);

		var pageContent = function (data) {

			// FOOTER
			var pageNumber = "Page " + data.pageCount;

			if (typeof doc.putTotalPages === 'function') {
				pageNumber = pageNumber + " of " + ReportsLibrary.totalPagesExp;
			}

			ReportsLibrary.Footer(doc, pageNumber, dateTime);
		};

		doc.autoTable(columns, rows, {
			addPageContent: pageContent,
			startY: 80,
			headerStyles: { fillColor: [ReportColorRGB.Red, ReportColorRGB.Green, ReportColorRGB.Blue] },
			columnStyles: {
				0: {columnWidth: 290},
				1: {columnWidth: 75},
				2: {columnWidth: 75},
				3: {columnWidth: 75}
			},
			margin: { top: 30 },

			drawCell: function (cell, data) {

				// Rowspan

				var tradeText = "Trade";
				if (data.row.cells[0].raw == tradeText) {
					if (data.column.dataKey === 0) {

						doc.setFillColor(ReportColorRGB.Red, ReportColorRGB.Green, ReportColorRGB.Blue);
						doc.rect(data.settings.margin.left + 0.5, data.row.y, data.table.width - 0.5, 20, 'F');

						doc.setTextColor(WhiteRGB.Red, WhiteRGB.Green, WhiteRGB.Blue);
						doc.autoTableText(tradeText, cell.x + 5, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});

						doc.setFillColor(WhiteRGB.Red, WhiteRGB.Green, WhiteRGB.Blue);
						doc.rect(data.settings.margin.left + 55, data.row.y + 3, data.table.width - 250, 13, 'F');

						doc.setTextColor(ReportColorRGB.Red, ReportColorRGB.Green, ReportColorRGB.Blue);
						doc.autoTableText(data.row.cells[1].raw, cell.x + 65, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});
					}

					return false;
				}

				var locationText = "Location";
				if (data.row.cells[0].raw == locationText) {
					if (data.column.dataKey === 0) {

						doc.setDrawColor(ReportColorRGB.Red, ReportColorRGB.Green, ReportColorRGB.Blue);
						doc.setFillColor(ReportColorRGB.Red, ReportColorRGB.Green, ReportColorRGB.Blue);
						doc.rect(data.settings.margin.left + 0.5, data.row.y - 2, data.table.width - 0.5, 20, 'F');

						doc.setTextColor(WhiteRGB.Red, WhiteRGB.Green, WhiteRGB.Blue);
						doc.autoTableText(locationText, cell.x + 60, cell.y - 2 + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});

						doc.setFillColor(WhiteRGB.Red, WhiteRGB.Green, WhiteRGB.Blue);
						doc.rect(data.settings.margin.left + 100, data.row.y + 1, data.table.width - 250, 13, 'F');

						doc.setTextColor(ReportColorRGB.Red, ReportColorRGB.Green, ReportColorRGB.Blue);
						doc.autoTableText(data.row.cells[1].raw, cell.x + 110, cell.y - 2 + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});
					}

					return false;
				}

				var materialText = "Material";
				if (data.row.cells[0].raw == materialText) {
					if (data.column.dataKey === 0) {

						var materialName = data.row.cells[1].raw;
						doc.autoTableText(materialName, cell.x + 60, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});

						var materialDetails = data.row.cells[2].raw;

						var materialCost = "$" + materialDetails[0];
						doc.autoTableText(materialCost, cell.x + 300, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});

						var materialQuantity = materialDetails[1] + "";
						doc.autoTableText(materialQuantity, cell.x + 380, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});

						var materialEstimate = materialDetails[2] + "";
						doc.autoTableText(materialEstimate, cell.x + 460, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});
					}

					return false;
				}

				var manufacturer = "Manufacturer";
				if (data.row.cells[0].raw == manufacturer) {
					if (data.column.dataKey === 0) {

						doc.setTextColor(128, 128, 128);
						doc.autoTableText(manufacturer, cell.x + 80, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});

						var manufacturerName = data.row.cells[1].raw;

						doc.setTextColor(100);
						doc.autoTableText(manufacturerName, cell.x + 200, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});

						var description = data.row.cells[2].raw;

						doc.setTextColor(100);
						doc.autoTableText(description, cell.x + 300, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});
					}

					return false;
				}

				var model = "Model";
				if (data.row.cells[0].raw == model) {
					if (data.column.dataKey === 0) {

						doc.setTextColor(128, 128, 128);
						doc.autoTableText(model, cell.x + 80, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});

						var modelName = data.row.cells[1].raw;

						doc.setTextColor(100);
						doc.autoTableText(modelName, cell.x + 200, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});

						var description = data.row.cells[2].raw;

						doc.setTextColor(100);
						doc.autoTableText(description, cell.x + 300, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});
					}

					return false;
				}

				var tradeSummaryTotal = "Trade Summary Total";
				if (data.row.cells[0].raw == tradeSummaryTotal) {
					if (data.column.dataKey === 0) {

						doc.setLineWidth(2);
						doc.line(cell.x + 60, cell.y + 2, cell.x + 500, cell.y + 2);
						doc.line(cell.x + 60, cell.y + cell.height - 2, cell.x + 500, cell.y + cell.height - 2);

						doc.autoTableText(data.row.cells[1].raw, cell.x + 60, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});

						var tradeDetails = data.row.cells[2].raw;
						var tradeTotal = "$" + tradeDetails[2];
						doc.autoTableText(tradeTotal, cell.x + 450, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});
					}

					return false;
				}

				var locationSummaryTotal = "Location Summary Total";
				if (data.row.cells[0].raw == locationSummaryTotal) {
					if (data.column.dataKey === 0) {

						doc.setLineWidth(2);
						doc.line(cell.x + 60, cell.y + 2, cell.x + 500, cell.y + 2);
						doc.line(cell.x + 60, cell.y + cell.height - 2, cell.x + 500, cell.y + cell.height - 2);

						doc.autoTableText(data.row.cells[1].raw, cell.x + 60, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});

						var tradeDetails = data.row.cells[2].raw;
						var tradeTotal = "$" + tradeDetails[2];
						doc.autoTableText(tradeTotal, cell.x + 450, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});
					}

					return false;
				}

				var grandTotal = "Grand Total";
				if (data.row.cells[0].raw == grandTotal) {
					if (data.column.dataKey === 0) {

						doc.setTextColor(128, 128, 128);
						doc.autoTableText(grandTotal, cell.x, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});

						doc.rect(data.settings.margin.left + 420, data.row.y + 3, data.table.width - 460, 13, 'S');

						doc.setTextColor(0);
						doc.autoTableText(data.row.cells[1].raw, cell.x + 440, cell.y + cell.height / 2, {
							halign: 'left', valign: 'middle'
						});
					}

					return false;
				}
			}
		});

		if (typeof doc.putTotalPages === 'function') {
			doc.putTotalPages(ReportsLibrary.totalPagesExp);
		}

		doc.save(filterText + '.pdf');
	},


	AddHeadingInPDF: function (text, options) {

		var imgHeight = 20;

		if (options.heightLeft < imgHeight) {
			options.doc.addPage();
			options.position = 0;
			options.heightLeft = options.pageHeight;
		}

		options.doc.text(10, 10, text);

		options.heightLeft = options.heightLeft - imgHeight;
		options.position += imgHeight;

		return options;
	},

	AddImageInPDF: function (options) {

		var canvas = options.canvas;
		var imgData = canvas.toDataURL('image/png');

		var imgWidth = 210;
		var imgHeight = canvas.height * imgWidth / canvas.width;

		if (options.heightLeft < imgHeight) {
			options.doc.addPage();
			options.position = 0;
			options.heightLeft = options.pageHeight;
		}

		options.doc.addImage(imgData, 'PNG', 0, options.position, imgWidth, imgHeight);
		options.heightLeft = options.heightLeft - imgHeight;
		options.position += imgHeight;

		return options;
	},

	AddHtmlInPDF: function (element, options, imgHeight) {

		if (options.heightLeft < imgHeight) {
			options.doc.addPage();
			options.position = 0;
			options.heightLeft = options.pageHeight;
		}

		options.doc.fromHTML(element, 15, options.position, {}); //, 'elementHandlers': elementHandler

		options.heightLeft = options.heightLeft - imgHeight;
		options.position += imgHeight;

		return options;
	},

	ProjectReport: function (project) {

		var dateTime = moment().format('LLLL');

		var doc = new jsPDF('p', 'pt');
		var columns = ["Location Name", "Trade Name"];

		var rows = [];

		for (var i = 0; i < project.locations.length; i++) {

			var location = project.locations[i];
			rows.push([location.locationName, ""]);

			for (var j = 0; j < location.trades.length; j++) {
				var trade = location.trades[j];
				rows.push(["", trade.tradeName]);
			}
		}

		var HeaderInformation = {
			doc: doc,
			ReportTitle: project.projectName,
			projectId: project.projectId,
			projectOwner: project.owner,
			address: project.address,
			phone: project.phone
		};

		// HEADER
		ReportsLibrary.MainReportHeader(HeaderInformation);

		var pageContent = function (data) {

			// FOOTER
			var pageNumber = "Page " + data.pageCount;

			if (typeof doc.putTotalPages === 'function') {
				pageNumber = pageNumber + " of " + ReportsLibrary.totalPagesExp;
			}

			ReportsLibrary.Footer(doc, pageNumber, dateTime);
		};

		doc.autoTable(columns, rows, {
			addPageContent: pageContent,
			startY: 80,
			headerStyles: { fillColor: [194, 146, 0] },
			margin: { top: 30 },
		});

		if (typeof doc.putTotalPages === 'function') {
			doc.putTotalPages(ReportsLibrary.totalPagesExp);
		}

		doc.save('All Trades by All Locations.pdf');
	},

	ProjectPageReport: function (project) {

		var position = 0;
		var pageHeight = 295;
		var doc = new jsPDF('p', 'pt');

		var options = { doc: doc, canvas: '', position: 0, pageHeight: pageHeight, heightLeft: pageHeight };

		var columns = ["Location", "Type"];

		var rows = [];

		for (var i = 0; i < project.locations.length; i++) {

			var location = project.locations[i];
			rows.push([location.locationName, location.type.name]);
		}

		options.doc.text('Project Name: ' + project.projectName, 10, 40);
		options.doc.text('Phone: ' + project.phobe, 10, 60);
		options.doc.text('Address: ' + project.address, 10, 80);
		options.doc.text('City: ' + project.city, 10, 100);
		options.doc.text('State: ' + project.state, 10, 120);
		options.doc.text('Owner: ' + project.owner, 10, 140);

		options.doc.autoTable(columns, rows, {
			startY: 160,
			margin: { horizontal: 10 },
			columnStyles: { text: { columnWidth: 200 } }
		});


		options.doc.save('Project Location.pdf');
	},

	LocationPageReport: function (location) {

		var dateTime = moment().format('LLLL');
		var doc = new jsPDF('p', 'pt');
		var columns = ["Trade Name", "Locations", "Material Name", "Description"];

		var rows = [];

		var trades = location.trades;

		for (var i = 0; i < trades.length; i++) {

			rows.push([trades[i].tradeName, location.locationName, '', '']);
			var materials = trades[i].materials;

			for (var j = 0; j < materials.length; j++) {
				rows.push(['', '', materials[j].materialName, '___']);
			}
		}

		// Logo for Xactli
		var imgData = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAkACQAAD/4QBYRXhpZgAATU0AKgAAAAgABAExAAIAAAARAAAAPlEQAAEAAAABAQAAAFERAAQAAAABAAAAAFESAAQAAAABAAAAAAAAAABBZG9iZSBJbWFnZVJlYWR5AAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCABIALYDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9/KKKKACiiigCnrulNrWmSWy3l3YmUY862ZVlX/dJBA+uK8r8e/sXeHfiDbsLzWvFzTMD+9m1VrrJ9xKGGPZcDHAxXsFFefmOU4PH0/ZYympx7NX/AKfnudmDzDE4SftMNNxfdHwH8XPhn4+/Yx8QWt1peu3v9k3D4tby0Zo4XYc+XLFkqGxztbcpGSCcMF+mv2RP2n0/aB8MzW+oLDb+ItLUG4ROFuYzwJVHbnhgMgEg8BgB0/7THga3+IPwL8SWM0Yd4rKS7tztyyTRAyJj0yV2nHO1iO9fFv7C/iSTw1+0vokSyMsOoedZzANgOrROVB9R5ixnHqB6V+FVI4jg3i3C4PCVJPBYt8qpyk5KMm1H3bttWcou+7TcXeyZ+uU3R4n4cxGJxEEsVhlfnSScopX1ta90pK3RpNWu0fobRRRX9Cn4uFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUVi/EDxdN4G8Lz6pDpOo60LXDSW1iqtPs7sqsRux6Dn0Brwlv+Cmng5Gw2heJwfTy7f/AOO187nPFmU5TUjSzKuqbkrrmvqutnazt17aX3R7WV8O5lmUHPA0nUUXZ2tp6q9/68j6Qor5xg/4KZ+C5JlVtF8TRqxwWMcBx/5Fr37wr4r07xv4ftdU0m8hvtPvE3xTRnhh6EdQQeCDgggggEVWS8VZRm0pQy3ERqOOrSeqXe29vMWacPZllqjLHUZQUtm1o/mZ/wAWb5dL+Ffia5kxst9KupWyccCFj1/CvhX9hbw+2uftO6K+1mj0/wA+6kKjoBC6gn23sn4kV9Nft/fEVPBfwGuNPV1F54gnS1jXdhhGpEkjY7j5VQ/9dBXD/wDBNH4XPpuh6x4uuo2Rr8jT7MsCNyKQ0rD1UsI1z2MbCvy/i+n/AGtx1lmXU9fqydWb7appP5wiv+3kff8ADVR5dwlj8bPT27VOPno07fKUv/AWfVFYvxF8WS+BPA2qa1DYTao2l27XJtYnCvKq8tgkHkLk9OcYrapskazRsrKrKwwykZBHoa/asRCc6UoUpcsmmk7J2dtHZ6O29nofldGUI1IyqLmSautrrqrrVXOa+DvxUsPjR8PbHxFpyvFBe7g0LkF4HVirK2O4I/EEHvWJ8OP2gLb4ofFTxN4d03TbhrTwwfKuNSMgMUk27b5aqB6h+c87D7V4H4a+Ikn7H/iD4seE4fMuLSzg/tfRgmXW3aUxxqHxnGPOhyTjPlH+8K9k/Yr+GyfD74E6bcSMs+oeIh/at3OG3mQygFBu5zhNvfqWPevzfIeKcfmeKwuX3UalNTeJ0W8G6aitNOeactNorfU+6zjh/B4DD4jGWcqdRwVDV7TSm2+/LG0dftPyPWqK8X8U+G/jpd6zqFzpfiLwba2SXEhsrQ2zFpIQx8sOzIcMVxnDY96vfs9ftJH4j/CfWdZ8RW8Ol3/hWWWHVRF/qx5a7y6jJPTIxk8qccYr6ahxVh5YtYLEUqlGTUnFzilGSh8VmpPZa62dtTwa3DtaOG+tUKkKqTimoNtxcvhumlu9NL66HrVNmnS2haSRljjjUszMcKoHUk+lfOfgn4k/F/8AaRtbjXvCs/h7wn4aMzxWQvYTNcXIU4LH5WHXIJG0ZGBnBNZv7Zdz8SNH/Zxs11bUNBSJn8nXJLB5Vku2aVhGkalABFsCFiSCxyMAZB8nE8fUY5dWzOhhqsqcIOcZOKjGaVrWbfMk73u4r3U2r6J+lh+DaksdSwFavTjOUlGUVJuUX5q1m+llJ+9ZO259AfETx9H4F+GereJIYV1CLTbF71I1l8sXChdwAbBxn1waxvA/xlk8b/ANfG0OjzedJYXF6mmRTGaSRot+I1YKCWYpgfL1boa8wv8ATviFZfsxeLm8X33hy70x/DR/s9NOWQTR/uv+Wm5FH3cdM81qfAv/AISRf2JvD7+FLjTbXWord5I5L9WMIQXEhcEKCclc4464rnp8TYyvmPIozhB4adTkcY8/OpqKaV3fTZOVnpdG08hw1LA8zlGU1XjDnUpcvK4NtN9Nd3a66Ho/wT+JN78VvAcOsahoN54buJJXjNlcszOoU4DZZEOD/u11teU/sc/F3WfjX8JJNZ1x7eS9XUJbcGGLy12KqEcf8CNerV9Zw3jljcroYtTc+eKfNJKLlpu4x0TfZaI+dzzBvC5hWw7gocsmuVNyS8k3q7d3qFFFFe0eSFFFFABRUUN7FcXE0McitJbkCRR/ASMjP4c1LQAV80/tf/sURePFuPE3hO3SHWsGS8sYxtW+9XQdBJ6jo/Xhs7/paivB4i4bwGeYKWBzCHNF6p9YvpKL6Nfc1o002j1slzzGZVili8HK0lv2a7NdU/8Agqz1PyTu7SWwuZIZo2jljYqysMFSOCCK9O/Zo/ai1b9n3X9q777QrtgbuxZ8Angb0P8AC4AAz0IGD2K/Un7WP7Gdl8Y7S41zQI4bHxRGC7qMJFqWOzdlk44boejcYZfhm58KalaeJG0eSxuk1NJvs5tTE3miTO3btxndnjGM54r+Oc/4azvgnNoV6Env+7qRWkv7rWurWkoO6fS61P6eyfPsp4ry6VGvFbe/BvWPmn27SVrdbM9r+Nnja8/bS/aH03SfDqyNpsYW0svNUqsa/flncZ47k45KxoMbuv0D4Z8QLZftKeHfhT4TfZ4f+HOhHWPEkqqkgae4zFY2shGCkr/6TdNwM7Iz0bjyXxN4y8O/8Eqv2ZLzxp4mW2vvH3iCM22maWJB5k8pAYQAj/lmh2vNIOBhVBY+Xu7D/gk7a6b4g/Znk8cf8JJaeLfGHxD1KXW/FOoRN80F6+ALNlwDGII9iBPujJZPkZa/prw94bxeGjWzvOP98xT5pL+SP2YK92rK11footvlu/wPjPPMNXdLK8t/3bDq0f70vtTfe72+b0vY+nqyfHnjOz+HfgzU9c1Bttnpdu1xJjq2Bwo9ycAe5Fa1YnxC+HWj/FTwzJo+vWrXumzOryQieSEOVOVyY2UkA4OM4yB6V+gY76x9Xn9Ut7Sz5ea6jzW0vZN2vvZM+PwnsPbw+s35Lrmtva+trtK9ttTw79kv4Sn4lfD3xb4r8VQ+defEh5kYMPmjtckDb6ZbJGOyIR2qT9j3x3f+CNA8XfD3VN1xrXgR55bRTn/Sbfkjb327iCP9mVAK990LSrPQNHtdP0+OOCz0+JbaGJD8sSIAqr+AAHNYg+EnhtvicfFy2AXxIsP2d7pLiVdybduGjDbG+X+8pPA9Bj4fCcG1sAsDVwM17WkpRqyd/wB5Gp703s7yVS04p2W6bVz6zE8UUsY8XTxcX7Oo1Kmlb3JQ0gt1Zcl4ya12dmfOv7PHwvs/2sfCuoeKvGXirXL7VJLqWM2dtf8AkQ6cgA24TnaOcjoMdick5v7Ovg+41v8AZ5+M2i6Z51xcNdTxW4LbpJ9qHAz3ZtuPcmva/FX7E3w38X65PqFxoHk3F0xeX7NdSwxsxOSdittGfYCuw+F3wc8OfBnSZrHw3p39n29y4kmHnSStKwGMkuxP5cV85lnh7jViaLxkKa5IVY1KsZzlUq+0i48zUoKzV76zdr6aHuZhxphXQqrCym+aVOUKbhFQp8klKyak7rS3wrzPOv2G/iVourfs+aNpqX1nDqGjiS3urZ5QskZ81mDYJzhgwOemcjtVX/goDdxal+zHdzW8kdxD9ut/njYMvDkHkccHj610njX9jr4c/ETXZtUvNBjW8uHYzSWlzJAsr55LKjBd2c5IGSc5zXUaV8FvC+hfDJvCEelxN4bKMr2k0jyKQzbySzEtnd82c8HkYwK96nkOdVckqZBilSUPYunGpGUrtpKMXKDglHT4rSeuyaPHqZxlVPNoZzh/aOXtVOUHGNkm+aSU1LXXa8Vpu0cl8S9Vtdc/ZB1n7HcQXWfC7SYicMdotwSeO1Uv2R9Wtb39kvRYYbiGSaGwuVkjVwWQiWXOR1HUfmK6b4ZfsyeC/hDfahcaHpTW8mpQG2nEtzLOrxE5KbXYjB468+/WjwB+zF4K+F/ia+1bQ9JaxutQt3tZQt1K0fluVZlCliACVB46Y4xW+HyfOP7QoZjVjTT9lKjOKnJ2XMpKUXyLmemsWo2vuzKtmmWfUquBpym17RVINxirvlacZLndt9Gm79kef/8ABN7/AJN6m/7C8/8A6BFXv1c/8N/hdoXwi8PNpPh2x/s/T2macxedJN87AAnMjMf4RxnHFdBX0HCuU1ssyjD5fXac6cFFtXauu10nb5I8biLMaePzOvjaKajOTavvZ97Nr8Qooor6A8UKKakiyZ2srbTg4PQ0UAfPn/BOj4rN8a/A3xG8RNfG+W8+IWsJCc58mCMxRwJ7YhWL8896+ha/HX/gm9+2vb/8E+/2ofH3w58fSSW/hXVNbmtLm8IZhpN9BK8XnFRyY3A2uQCRtjPQHP6/eH/EWn+LdFttS0q+s9T068TzLe6tJlmhnX+8jqSrD3BoAuUVS1/U7jStPaS1sLjUrjokMTIm444yzEBR7/oa8G+PfxBvNK8PyXHxM8eeF/hN4XkXL2VtfibUr2P7rJ5rBc55wIkbrghuccGOxk6EbUqUqk3slZffJtRS9Xfsm9DrwuFjVd6k1CK3b/RK7b+Vu7RpfHz9qmay1Y+D/h9A2veLromFpLdfOisD/Ef7rOvfPypgluhWvG/G/wAQfAP/AATO8Ht40+JOof8ACSfE7Wkkm0/SIZvNuZHbOSCc7cnO+4fgfMq7jnzPnv4v/wDBYfwv8J9Mk8J/s5+EpJdQvStsfEuqWxknuHJAHkwMC8hJwVEm1QTgRYrrP2GP+CS3iT4w+OF+LH7Rs2oapqV5KLu38P6lIZLi5bOVe9z91B2txjjAbaAYz81g+GauIx0c2zuSqVYfw4Rv7Ol5xuk5z71JJP8AljFI9vE55To4V5flcXCnL45P46nk7aRj2gm/NyPgP9r/APaN8dftR/F2TxV47+1W91fQJJp1k0Tw29lZP88SwK3/ACzIO4PzvJLEknNWv2NP20/GH7E3xQj8QeGbj7RY3JWPVdInci11WEH7rD+F1ySkgGVJPVSyt+0H7eX/AATz8I/tw/DuOzvI4dF8UaTCU0bWoYQXtR1EMijHmQE/wZ+UklSDnP5h3P8AwQv/AGgYLiRF0fw7MqMVEia1FtcDuM4OD7gH2r7Q+ZP1z/ZU/aw8H/th/Cy28U+Eb7zY2xHe2UpC3WmT4yYplzwfQjKsOQSKyP27vD/izxN+zdqlr4Ph1a7vPtdnJf2Wl3H2e/1HTluIzd29vJxtkeHeBggnkA84P52/spf8E5P2sv2O/irbeKvCWn+H45FxHfWMutxG11ODOTFKoPI9CPmU8gg1+mnib4qeLfC/wg0zxAfhzq2q6/N5R1Lw7pupWstzYAg+YUlkeOOfYQAApUtuHA5AAPMv2Sh8C7o+Irj4W6La+GfE1nYm21nSprWbT9VslPzAT28uG+8ow+CCejHNfK3gLwv8OfCH7Cnwz8TeB7rS7L4/XkemLpS6RqBOqapeNcxiWKeCN8yRGLeZBIu0KMntn6c8M+EvF/7Q/wC1RofxI1DwDqHw70fwj4f1DSoRq89udV12W68sBGjgkdY4ItjMC75LPkDBJHTfsD/sy2HwG/Zy8Ew6n4O0HQfHNto8Vvq9xDZ2/wBseUA7hJPGCXPPXcaAPCPHmj/BfW/24PjR/wALY0W61i6hn0ZdNMelalfCGM6ZCXGbRGC5bBw2Ce3evRfHngTSviJ8d/AvwJ0ltQ0L4Z6P4Vl8Xapp1lcTWx1aI3SwWtm8mRKsW8yyOm4Ftqg9OLUmqfEb4F/tU/FjWdL+EviLxronjSfSp7G+07VdOt0At7COGQFZ50fO8MPu9vQir3jrwl4+v/H/AIN+N3hvwXND4qtNKuPD3iDwbqGo20d1d6c9x5ieVcqzQCaORBKAW2sshUlWFAHqHwi/Zb+H/wABNXur7wb4X0/w9c30P2e4NoXVZk3bvmXcVJ3c7sZ968J/bK0fR9P/AGmdL1z4seHNc8SfB1PDotbWS2t5r3TtD1Q3DGae8t4cvh4TEqysrKu0jgkke2fBf43+JPivr11DqXww8YeCNOtbcv8AbNcnsh5025QIkjhmkcjaS284X5cdTVX4ufH7xT8M/Fzafp/wl8beMLGWJWt9R0e4sDC8h+8jrNPG8YHHzEEHJx0oA8c/aK8Q+AfhZ8FPgTq3gu4t4/h/b/EOwu7aTSPNvomheG+d9ix73f5i2UUEggjAxgO/aF/an8I/GHxz8GdK8N32ty3sfxF064lW40LUNPQxCC6U/PPDGh5dflzk9cYBxS8E/sm+NfDngfwZNPolnZ3978Yj4+1LSLK6jaDw3ZSxTqYVckLJsyhbyxgtI20EDNezftU/DfWviJqPwrk0axa9Xw748sNY1AiVE+z2kcF0jyfMRuw0iDC5bnpwaAPmXWtE+Ceufth/HGT4saJeaxqEeuafHp7ppOp3yxQ/2TZll3WqMq/Mc4bB59K2v2pPAHwn8IftYfDPw3400OSTwHpngXUY7DToLK9vFglW8sxGdlury8IZBubgZwTkiu30nVviR8A/2j/i9qGn/CHxJ4y0fxlrFlqFhf6fqunW8ZSPTraBgVmnV8+ZG45UdO+ad8RtS+JFn+0Z4D+J2n/CPxFq8a+ENQ0fUNIh1XTo7rTLia7tpUDs84RhthJyhYfMPegCt4y/Y6+Bfi39lfWtd8OeCrVdPtdL1LUtMeaO8tZIZxCVaTy5isindbpw64+QEDByeN039lW38J/8E9PD2u/DDw5ew+KvE3h/QbjxEuj3skOpa7Yt9nmvoYXZsLLIhkwV2k5IB5wfoSPxT4s+OPwF8b2uqfD/AFrwXq11pt3Y2NjqF/Z3El6z27BWVoJXVQWbb85HPtUHg3UfFH7Pv7I/w/tovBOreKtc0PQ9M07U9I027tlurfy7VEmZTJIschRlxtV/mzwSOaAOa/YuT4FXmtatP8LtFs/DXiazt1tta0qa0m0/VLNCwYC4t5cN95Rh8EHsxzRXC+Mvgf4w/bd+Num+Kr7wnqvwl0fw9olzpsV1qctu+savLNPbybGit5HCQRiFiC75LSZC4JNFAHlv/BWT/gknq37QHjC4+Jnwzht5/El1Eq61oryrD/aRRQqzwM2FEu0KrIxAYKCDuyH/ADn0fXvjZ+yHrU1nZ3HxG+H90GIkt1+1WKyc85ThXHvgjvRRQB1+mftVftP/ABhjfTdL8WfFrXDMu1odMa6aRgf+uI3V6b8Ev+CLXx0/aI1lNT8aOvg2xnIMt7rtwbrUJF6/LArF8j0laPvRRQB+kX7HX/BML4X/ALGyQ3+k6a2veKlXD67qoWW4Q9/JXGyEdR8g3Y4LNX0VRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQB/9k=';
		doc.addImage(imgData, 20, 20, 80, 20);

		doc.setFontSize(15);
		doc.setTextColor(100);
		doc.text(location.locationName, 110, 36);

		doc.setFontSize(10);
		doc.text('Type: ' + location.type.name, 110, 55);

		//doc.text('Owner: ' + HeaderInformation.projectOwner, 350, 55);
		//doc.text('Address: ' + HeaderInformation.address, 20, 70);
		//doc.text('Phone: ' + HeaderInformation.phone, 350, 70);
		//doc.text('Location Name: ' + location.locationName, 10, 40);
		//doc.text('Type: ' + location.type.name, 10, 60);

		var pageContent = function (data) {

			// FOOTER
			var pageNumber = "Page " + data.pageCount;

			if (typeof doc.putTotalPages === 'function') {
				pageNumber = pageNumber + " of " + ReportsLibrary.totalPagesExp;
			}

			ReportsLibrary.Footer(doc, pageNumber, dateTime);
		};

		doc.autoTable(columns, rows, {
			addPageContent: pageContent,
			startY: 80,
			headerStyles: { fillColor: [194, 146, 0] },
			margin: { top: 30 }
		});

		if (typeof doc.putTotalPages === 'function') {
			doc.putTotalPages(ReportsLibrary.totalPagesExp);
		}

		doc.save('Location ' + location.locationName + ' Details.pdf');
	},

	MaterialsBySpecificTradeByLocation: function (project, tradeId) {

		var position = 0;
		var pageHeight = 295;
		var doc = new jsPDF('p', 'pt');

		var options = { doc: doc, canvas: '', position: 0, pageHeight: pageHeight, heightLeft: pageHeight };

		var columns = ["Trade Name", "Location Name", "Material Name", "Description"];

		var rows = [];

		//var trades = location.trades;

		for (var i = 0; i < project.locations.length; i++) {
			var location = project.locations[i];

			for (var j = 0; j < location.trades.length; j++) {

				var trade = location.trades[j];

				if (tradeId == 0 || trade.tradeId == tradeId) {

					rows.push([trade.tradeName, location.locationName, "", ""]);

					for (var k = 0; k < trade.materials.length; k++) {

						var material = trade.materials[k];
						rows.push(['', '', material.materialName, '___']);
					}
				}
			}
		}

		options.doc.text('Project Name: ' + project.projectName, 10, 40);

		options.doc.autoTable(columns, rows, {
			startY: 80,
			margin: { horizontal: 10 },
			columnStyles: { text: { columnWidth: 200 } }
		});

		options.doc.save('Materials by specific trades by Locations.pdf');
	},

	Footer: function (doc, pageNumber, dateTime) {

		doc.setFontSize(10);
		doc.text(20, 830, dateTime)
		doc.text(460, 830, pageNumber); //print number bottom right
	},

	MainReportHeader: function (HeaderInformation) {

		var doc = HeaderInformation.doc;

		// Logo for Xactli
		var imgData = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAkACQAAD/4QBYRXhpZgAATU0AKgAAAAgABAExAAIAAAARAAAAPlEQAAEAAAABAQAAAFERAAQAAAABAAAAAFESAAQAAAABAAAAAAAAAABBZG9iZSBJbWFnZVJlYWR5AAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCABIALYDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9/KKKKACiiigCnrulNrWmSWy3l3YmUY862ZVlX/dJBA+uK8r8e/sXeHfiDbsLzWvFzTMD+9m1VrrJ9xKGGPZcDHAxXsFFefmOU4PH0/ZYympx7NX/AKfnudmDzDE4SftMNNxfdHwH8XPhn4+/Yx8QWt1peu3v9k3D4tby0Zo4XYc+XLFkqGxztbcpGSCcMF+mv2RP2n0/aB8MzW+oLDb+ItLUG4ROFuYzwJVHbnhgMgEg8BgB0/7THga3+IPwL8SWM0Yd4rKS7tztyyTRAyJj0yV2nHO1iO9fFv7C/iSTw1+0vokSyMsOoedZzANgOrROVB9R5ixnHqB6V+FVI4jg3i3C4PCVJPBYt8qpyk5KMm1H3bttWcou+7TcXeyZ+uU3R4n4cxGJxEEsVhlfnSScopX1ta90pK3RpNWu0fobRRRX9Cn4uFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUVi/EDxdN4G8Lz6pDpOo60LXDSW1iqtPs7sqsRux6Dn0Brwlv+Cmng5Gw2heJwfTy7f/AOO187nPFmU5TUjSzKuqbkrrmvqutnazt17aX3R7WV8O5lmUHPA0nUUXZ2tp6q9/68j6Qor5xg/4KZ+C5JlVtF8TRqxwWMcBx/5Fr37wr4r07xv4ftdU0m8hvtPvE3xTRnhh6EdQQeCDgggggEVWS8VZRm0pQy3ERqOOrSeqXe29vMWacPZllqjLHUZQUtm1o/mZ/wAWb5dL+Ffia5kxst9KupWyccCFj1/CvhX9hbw+2uftO6K+1mj0/wA+6kKjoBC6gn23sn4kV9Nft/fEVPBfwGuNPV1F54gnS1jXdhhGpEkjY7j5VQ/9dBXD/wDBNH4XPpuh6x4uuo2Rr8jT7MsCNyKQ0rD1UsI1z2MbCvy/i+n/AGtx1lmXU9fqydWb7appP5wiv+3kff8ADVR5dwlj8bPT27VOPno07fKUv/AWfVFYvxF8WS+BPA2qa1DYTao2l27XJtYnCvKq8tgkHkLk9OcYrapskazRsrKrKwwykZBHoa/asRCc6UoUpcsmmk7J2dtHZ6O29nofldGUI1IyqLmSautrrqrrVXOa+DvxUsPjR8PbHxFpyvFBe7g0LkF4HVirK2O4I/EEHvWJ8OP2gLb4ofFTxN4d03TbhrTwwfKuNSMgMUk27b5aqB6h+c87D7V4H4a+Ikn7H/iD4seE4fMuLSzg/tfRgmXW3aUxxqHxnGPOhyTjPlH+8K9k/Yr+GyfD74E6bcSMs+oeIh/at3OG3mQygFBu5zhNvfqWPevzfIeKcfmeKwuX3UalNTeJ0W8G6aitNOeactNorfU+6zjh/B4DD4jGWcqdRwVDV7TSm2+/LG0dftPyPWqK8X8U+G/jpd6zqFzpfiLwba2SXEhsrQ2zFpIQx8sOzIcMVxnDY96vfs9ftJH4j/CfWdZ8RW8Ol3/hWWWHVRF/qx5a7y6jJPTIxk8qccYr6ahxVh5YtYLEUqlGTUnFzilGSh8VmpPZa62dtTwa3DtaOG+tUKkKqTimoNtxcvhumlu9NL66HrVNmnS2haSRljjjUszMcKoHUk+lfOfgn4k/F/8AaRtbjXvCs/h7wn4aMzxWQvYTNcXIU4LH5WHXIJG0ZGBnBNZv7Zdz8SNH/Zxs11bUNBSJn8nXJLB5Vku2aVhGkalABFsCFiSCxyMAZB8nE8fUY5dWzOhhqsqcIOcZOKjGaVrWbfMk73u4r3U2r6J+lh+DaksdSwFavTjOUlGUVJuUX5q1m+llJ+9ZO259AfETx9H4F+GereJIYV1CLTbF71I1l8sXChdwAbBxn1waxvA/xlk8b/ANfG0OjzedJYXF6mmRTGaSRot+I1YKCWYpgfL1boa8wv8ATviFZfsxeLm8X33hy70x/DR/s9NOWQTR/uv+Wm5FH3cdM81qfAv/AISRf2JvD7+FLjTbXWord5I5L9WMIQXEhcEKCclc4464rnp8TYyvmPIozhB4adTkcY8/OpqKaV3fTZOVnpdG08hw1LA8zlGU1XjDnUpcvK4NtN9Nd3a66Ho/wT+JN78VvAcOsahoN54buJJXjNlcszOoU4DZZEOD/u11teU/sc/F3WfjX8JJNZ1x7eS9XUJbcGGLy12KqEcf8CNerV9Zw3jljcroYtTc+eKfNJKLlpu4x0TfZaI+dzzBvC5hWw7gocsmuVNyS8k3q7d3qFFFFe0eSFFFFABRUUN7FcXE0McitJbkCRR/ASMjP4c1LQAV80/tf/sURePFuPE3hO3SHWsGS8sYxtW+9XQdBJ6jo/Xhs7/paivB4i4bwGeYKWBzCHNF6p9YvpKL6Nfc1o002j1slzzGZVili8HK0lv2a7NdU/8Agqz1PyTu7SWwuZIZo2jljYqysMFSOCCK9O/Zo/ai1b9n3X9q777QrtgbuxZ8Angb0P8AC4AAz0IGD2K/Un7WP7Gdl8Y7S41zQI4bHxRGC7qMJFqWOzdlk44boejcYZfhm58KalaeJG0eSxuk1NJvs5tTE3miTO3btxndnjGM54r+Oc/4azvgnNoV6Env+7qRWkv7rWurWkoO6fS61P6eyfPsp4ry6VGvFbe/BvWPmn27SVrdbM9r+Nnja8/bS/aH03SfDqyNpsYW0svNUqsa/flncZ47k45KxoMbuv0D4Z8QLZftKeHfhT4TfZ4f+HOhHWPEkqqkgae4zFY2shGCkr/6TdNwM7Iz0bjyXxN4y8O/8Eqv2ZLzxp4mW2vvH3iCM22maWJB5k8pAYQAj/lmh2vNIOBhVBY+Xu7D/gk7a6b4g/Znk8cf8JJaeLfGHxD1KXW/FOoRN80F6+ALNlwDGII9iBPujJZPkZa/prw94bxeGjWzvOP98xT5pL+SP2YK92rK11footvlu/wPjPPMNXdLK8t/3bDq0f70vtTfe72+b0vY+nqyfHnjOz+HfgzU9c1Bttnpdu1xJjq2Bwo9ycAe5Fa1YnxC+HWj/FTwzJo+vWrXumzOryQieSEOVOVyY2UkA4OM4yB6V+gY76x9Xn9Ut7Sz5ea6jzW0vZN2vvZM+PwnsPbw+s35Lrmtva+trtK9ttTw79kv4Sn4lfD3xb4r8VQ+defEh5kYMPmjtckDb6ZbJGOyIR2qT9j3x3f+CNA8XfD3VN1xrXgR55bRTn/Sbfkjb327iCP9mVAK990LSrPQNHtdP0+OOCz0+JbaGJD8sSIAqr+AAHNYg+EnhtvicfFy2AXxIsP2d7pLiVdybduGjDbG+X+8pPA9Bj4fCcG1sAsDVwM17WkpRqyd/wB5Gp703s7yVS04p2W6bVz6zE8UUsY8XTxcX7Oo1Kmlb3JQ0gt1Zcl4ya12dmfOv7PHwvs/2sfCuoeKvGXirXL7VJLqWM2dtf8AkQ6cgA24TnaOcjoMdick5v7Ovg+41v8AZ5+M2i6Z51xcNdTxW4LbpJ9qHAz3ZtuPcmva/FX7E3w38X65PqFxoHk3F0xeX7NdSwxsxOSdittGfYCuw+F3wc8OfBnSZrHw3p39n29y4kmHnSStKwGMkuxP5cV85lnh7jViaLxkKa5IVY1KsZzlUq+0i48zUoKzV76zdr6aHuZhxphXQqrCym+aVOUKbhFQp8klKyak7rS3wrzPOv2G/iVourfs+aNpqX1nDqGjiS3urZ5QskZ81mDYJzhgwOemcjtVX/goDdxal+zHdzW8kdxD9ut/njYMvDkHkccHj610njX9jr4c/ETXZtUvNBjW8uHYzSWlzJAsr55LKjBd2c5IGSc5zXUaV8FvC+hfDJvCEelxN4bKMr2k0jyKQzbySzEtnd82c8HkYwK96nkOdVckqZBilSUPYunGpGUrtpKMXKDglHT4rSeuyaPHqZxlVPNoZzh/aOXtVOUHGNkm+aSU1LXXa8Vpu0cl8S9Vtdc/ZB1n7HcQXWfC7SYicMdotwSeO1Uv2R9Wtb39kvRYYbiGSaGwuVkjVwWQiWXOR1HUfmK6b4ZfsyeC/hDfahcaHpTW8mpQG2nEtzLOrxE5KbXYjB468+/WjwB+zF4K+F/ia+1bQ9JaxutQt3tZQt1K0fluVZlCliACVB46Y4xW+HyfOP7QoZjVjTT9lKjOKnJ2XMpKUXyLmemsWo2vuzKtmmWfUquBpym17RVINxirvlacZLndt9Gm79kef/8ABN7/AJN6m/7C8/8A6BFXv1c/8N/hdoXwi8PNpPh2x/s/T2macxedJN87AAnMjMf4RxnHFdBX0HCuU1ssyjD5fXac6cFFtXauu10nb5I8biLMaePzOvjaKajOTavvZ97Nr8Qooor6A8UKKakiyZ2srbTg4PQ0UAfPn/BOj4rN8a/A3xG8RNfG+W8+IWsJCc58mCMxRwJ7YhWL8896+ha/HX/gm9+2vb/8E+/2ofH3w58fSSW/hXVNbmtLm8IZhpN9BK8XnFRyY3A2uQCRtjPQHP6/eH/EWn+LdFttS0q+s9T068TzLe6tJlmhnX+8jqSrD3BoAuUVS1/U7jStPaS1sLjUrjokMTIm444yzEBR7/oa8G+PfxBvNK8PyXHxM8eeF/hN4XkXL2VtfibUr2P7rJ5rBc55wIkbrghuccGOxk6EbUqUqk3slZffJtRS9Xfsm9DrwuFjVd6k1CK3b/RK7b+Vu7RpfHz9qmay1Y+D/h9A2veLromFpLdfOisD/Ef7rOvfPypgluhWvG/G/wAQfAP/AATO8Ht40+JOof8ACSfE7Wkkm0/SIZvNuZHbOSCc7cnO+4fgfMq7jnzPnv4v/wDBYfwv8J9Mk8J/s5+EpJdQvStsfEuqWxknuHJAHkwMC8hJwVEm1QTgRYrrP2GP+CS3iT4w+OF+LH7Rs2oapqV5KLu38P6lIZLi5bOVe9z91B2txjjAbaAYz81g+GauIx0c2zuSqVYfw4Rv7Ol5xuk5z71JJP8AljFI9vE55To4V5flcXCnL45P46nk7aRj2gm/NyPgP9r/APaN8dftR/F2TxV47+1W91fQJJp1k0Tw29lZP88SwK3/ACzIO4PzvJLEknNWv2NP20/GH7E3xQj8QeGbj7RY3JWPVdInci11WEH7rD+F1ySkgGVJPVSyt+0H7eX/AATz8I/tw/DuOzvI4dF8UaTCU0bWoYQXtR1EMijHmQE/wZ+UklSDnP5h3P8AwQv/AGgYLiRF0fw7MqMVEia1FtcDuM4OD7gH2r7Q+ZP1z/ZU/aw8H/th/Cy28U+Eb7zY2xHe2UpC3WmT4yYplzwfQjKsOQSKyP27vD/izxN+zdqlr4Ph1a7vPtdnJf2Wl3H2e/1HTluIzd29vJxtkeHeBggnkA84P52/spf8E5P2sv2O/irbeKvCWn+H45FxHfWMutxG11ODOTFKoPI9CPmU8gg1+mnib4qeLfC/wg0zxAfhzq2q6/N5R1Lw7pupWstzYAg+YUlkeOOfYQAApUtuHA5AAPMv2Sh8C7o+Irj4W6La+GfE1nYm21nSprWbT9VslPzAT28uG+8ow+CCejHNfK3gLwv8OfCH7Cnwz8TeB7rS7L4/XkemLpS6RqBOqapeNcxiWKeCN8yRGLeZBIu0KMntn6c8M+EvF/7Q/wC1RofxI1DwDqHw70fwj4f1DSoRq89udV12W68sBGjgkdY4ItjMC75LPkDBJHTfsD/sy2HwG/Zy8Ew6n4O0HQfHNto8Vvq9xDZ2/wBseUA7hJPGCXPPXcaAPCPHmj/BfW/24PjR/wALY0W61i6hn0ZdNMelalfCGM6ZCXGbRGC5bBw2Ce3evRfHngTSviJ8d/AvwJ0ltQ0L4Z6P4Vl8Xapp1lcTWx1aI3SwWtm8mRKsW8yyOm4Ftqg9OLUmqfEb4F/tU/FjWdL+EviLxronjSfSp7G+07VdOt0At7COGQFZ50fO8MPu9vQir3jrwl4+v/H/AIN+N3hvwXND4qtNKuPD3iDwbqGo20d1d6c9x5ieVcqzQCaORBKAW2sshUlWFAHqHwi/Zb+H/wABNXur7wb4X0/w9c30P2e4NoXVZk3bvmXcVJ3c7sZ968J/bK0fR9P/AGmdL1z4seHNc8SfB1PDotbWS2t5r3TtD1Q3DGae8t4cvh4TEqysrKu0jgkke2fBf43+JPivr11DqXww8YeCNOtbcv8AbNcnsh5025QIkjhmkcjaS284X5cdTVX4ufH7xT8M/Fzafp/wl8beMLGWJWt9R0e4sDC8h+8jrNPG8YHHzEEHJx0oA8c/aK8Q+AfhZ8FPgTq3gu4t4/h/b/EOwu7aTSPNvomheG+d9ix73f5i2UUEggjAxgO/aF/an8I/GHxz8GdK8N32ty3sfxF064lW40LUNPQxCC6U/PPDGh5dflzk9cYBxS8E/sm+NfDngfwZNPolnZ3978Yj4+1LSLK6jaDw3ZSxTqYVckLJsyhbyxgtI20EDNezftU/DfWviJqPwrk0axa9Xw748sNY1AiVE+z2kcF0jyfMRuw0iDC5bnpwaAPmXWtE+Ceufth/HGT4saJeaxqEeuafHp7ppOp3yxQ/2TZll3WqMq/Mc4bB59K2v2pPAHwn8IftYfDPw3400OSTwHpngXUY7DToLK9vFglW8sxGdlury8IZBubgZwTkiu30nVviR8A/2j/i9qGn/CHxJ4y0fxlrFlqFhf6fqunW8ZSPTraBgVmnV8+ZG45UdO+ad8RtS+JFn+0Z4D+J2n/CPxFq8a+ENQ0fUNIh1XTo7rTLia7tpUDs84RhthJyhYfMPegCt4y/Y6+Bfi39lfWtd8OeCrVdPtdL1LUtMeaO8tZIZxCVaTy5isindbpw64+QEDByeN039lW38J/8E9PD2u/DDw5ew+KvE3h/QbjxEuj3skOpa7Yt9nmvoYXZsLLIhkwV2k5IB5wfoSPxT4s+OPwF8b2uqfD/AFrwXq11pt3Y2NjqF/Z3El6z27BWVoJXVQWbb85HPtUHg3UfFH7Pv7I/w/tovBOreKtc0PQ9M07U9I027tlurfy7VEmZTJIschRlxtV/mzwSOaAOa/YuT4FXmtatP8LtFs/DXiazt1tta0qa0m0/VLNCwYC4t5cN95Rh8EHsxzRXC+Mvgf4w/bd+Num+Kr7wnqvwl0fw9olzpsV1qctu+savLNPbybGit5HCQRiFiC75LSZC4JNFAHlv/BWT/gknq37QHjC4+Jnwzht5/El1Eq61oryrD/aRRQqzwM2FEu0KrIxAYKCDuyH/ADn0fXvjZ+yHrU1nZ3HxG+H90GIkt1+1WKyc85ThXHvgjvRRQB1+mftVftP/ABhjfTdL8WfFrXDMu1odMa6aRgf+uI3V6b8Ev+CLXx0/aI1lNT8aOvg2xnIMt7rtwbrUJF6/LArF8j0laPvRRQB+kX7HX/BML4X/ALGyQ3+k6a2veKlXD67qoWW4Q9/JXGyEdR8g3Y4LNX0VRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQB/9k=';
		doc.addImage(imgData, 20, 20, 80, 20);

		doc.setFontSize(15);
		doc.setTextColor(100);
		doc.text(HeaderInformation.ReportTitle, 110, 36);

		doc.setFontSize(10);
		doc.text('Project: ' + HeaderInformation.projectId, 20, 55);

		doc.text('Owner: ' + HeaderInformation.projectOwner, 350, 55);

		doc.text('Address: ' + HeaderInformation.address, 20, 70);

		doc.text('Phone: ' + HeaderInformation.phone, 350, 70);
	}
}