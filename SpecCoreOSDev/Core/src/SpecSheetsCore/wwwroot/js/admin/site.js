﻿// Write your Javascript code.


function ConvertPDF() {

    var position = 0;
    var pageHeight = 295;
    var doc = new jsPDF('p', 'mm');

    var options = { doc: doc, canvas: '', position: 0, pageHeight: pageHeight, heightLeft: pageHeight };

    options = AddHeadingInPDF('Summary Report', options);
    options = AddHtmlInPDF($('#filtersLabel').get(0), options, 20);

    options.canvas = CreateCanvasFromSVG("#MostUsedHashtags");
    options = AddImageInPDF(options);

    options.canvas = CreateCanvasFromSVG("#AnnualVolume");
    options = AddImageInPDF(options);

    options = AddHtmlInPDF($('#AnnualVolumeTable').get(0), options, 150);

    options.canvas = CreateCanvasFromSVG("#SentimentByPlatform");
    options = AddImageInPDF(options);

    options = AddHtmlInPDF($('#SentimentByPlatformTable').get(0), options, 180);


    options.canvas = CreateCanvasFromSVG("#TotalVolumeOverTime");
    options = AddImageInPDF(options);

    options = AddHtmlInPDF($('#TotalVolumeOverTimeTable').get(0), options, 180);


    options.canvas = CreateCanvasFromSVG("#PositiveAndNegativeVolumeOverTime");
    options = AddImageInPDF(options);

    options.canvas = CreateCanvasFromSVG("#VolumeByPlatform");
    options = AddImageInPDF(options);

    options.canvas = CreateCanvasFromSVG("#BrandConversation");
    options = AddImageInPDF(options);

    options = AddHtmlInPDF($('#BrandConversationTable').get(0), options, 180);


    //var doc = new jsPDF();

    //doc.addImage(canvas, 0, 0, canvas.width, canvas.height);

    options.doc.save('Summary Report.pdf');
}
// Write your Javascript code.
function AddHeadingInPDF(text, options) {

    var imgHeight = 20;

    if (options.heightLeft < imgHeight) {
        options.doc.addPage();
        options.position = 0;
        options.heightLeft = options.pageHeight;
    }

    options.doc.text(10, 10, text);

    options.heightLeft = options.heightLeft - imgHeight;
    options.position += imgHeight;

    return options;
}

function AddImageInPDF(options) {

    var canvas = options.canvas;
    var imgData = canvas.toDataURL('image/png');

    var imgWidth = 210;
    var imgHeight = canvas.height * imgWidth / canvas.width;

    if (options.heightLeft < imgHeight) {
        options.doc.addPage();
        options.position = 0;
        options.heightLeft = options.pageHeight;
    }

    options.doc.addImage(imgData, 'PNG', 0, options.position, imgWidth, imgHeight);
    options.heightLeft = options.heightLeft - imgHeight;
    options.position += imgHeight;

    return options;
}

function AddHtmlInPDF(element, options, imgHeight) {

    if (options.heightLeft < imgHeight) {
        options.doc.addPage();
        options.position = 0;
        options.heightLeft = options.pageHeight;
    }

    options.doc.fromHTML(element, 15, options.position, {}); //, 'elementHandlers': elementHandler

    options.heightLeft = options.heightLeft - imgHeight;
    options.position += imgHeight;

    return options;
}

function ProjectReport(project) {
    var position = 0;
    var pageHeight = 295;
    var doc = new jsPDF('p', 'pt');

    var options = { doc: doc, canvas: '', position: 0, pageHeight: pageHeight, heightLeft: pageHeight };

    var columns = ["Location Name", "Trade Name"];

    var rows = [];

    for (var i = 0; i < project.locations.length; i++) {

        var location = project.locations[i];
        rows.push([location.locationName, ""]);

        for (var j = 0; j < location.trades.length; j++) {
            var trade = location.trades[j];
            rows.push(["", trade.tradeName]);
        }
    }

    options.doc.text(project.projectName, 10, 40);
    options.doc.autoTable(columns, rows, {
        startY: 55,
        margin: { horizontal: 10 },
        columnStyles: { text: { columnWidth: 200 } }
    });


    options.doc.save('All Trades by All Locations.pdf');
}

function ProjectPageReport(project) {
    var position = 0;
    var pageHeight = 295;
    var doc = new jsPDF('p', 'pt');

    var options = { doc: doc, canvas: '', position: 0, pageHeight: pageHeight, heightLeft: pageHeight };

    var columns = ["Location", "Type"];

    var rows = [];

    for (var i = 0; i < project.locations.length; i++) {

        var location = project.locations[i];
        rows.push([location.locationName, location.type.name]);
    }

    options.doc.text('Project Name: ' + project.projectName, 10, 40);
    options.doc.text('Phone: ' + project.phobe, 10, 60);
    options.doc.text('Address: ' + project.address, 10, 80);
    options.doc.text('City: ' + project.city, 10, 100);
    options.doc.text('State: ' + project.state, 10, 120);
    options.doc.text('Owner: ' + project.owner, 10, 140);

    options.doc.autoTable(columns, rows, {
        startY: 160,
        margin: { horizontal: 10 },
        columnStyles: { text: { columnWidth: 200 } }
    });


    options.doc.save('Project Location.pdf');
}

function LocationPageReport(location) {
    var position = 0;
    var pageHeight = 295;
    var doc = new jsPDF('p', 'pt');

    var options = { doc: doc, canvas: '', position: 0, pageHeight: pageHeight, heightLeft: pageHeight };

    var columns = ["Trade Name", "Locations", "Material Name", "Description"];

    var rows = [];

    var trades = location.trades;

    for (var i = 0; i < trades.length; i++) {

        rows.push([trades[i].tradeName, location.locationName, '', '']);
        var materials = trades[i].materials;

        for (var j = 0; j < materials.length; j++) {
            rows.push(['', '', materials[j].materialName, '___']);
        }
    }

    options.doc.text('Location Name: ' + location.locationName, 10, 40);
    options.doc.text('Type: ' + location.type.typeName, 10, 60);

    options.doc.autoTable(columns, rows, {
        startY: 80,
        margin: { horizontal: 10 },
        columnStyles: { text: { columnWidth: 200 } }
    });


    options.doc.save('Location Details.pdf');

}

function MaterialsBySpecificTradeByLocation(project, tradeId) {
    var position = 0;
    var pageHeight = 295;
    var doc = new jsPDF('p', 'pt');

    var options = { doc: doc, canvas: '', position: 0, pageHeight: pageHeight, heightLeft: pageHeight };

    var columns = ["Trade Name", "Location Name", "Material Name", "Description"];

    var rows = [];

    //var trades = location.trades;

    for (var i = 0; i < project.locations.length; i++) {
        var location = project.locations[i];

        for (var j = 0; j < location.trades.length; j++) {

            var trade = location.trades[j];

            if (tradeId == 0 || trade.tradeId == tradeId) {

                rows.push([trade.tradeName, location.locationName, "", ""]);

                for (var k = 0; k < trade.materials.length; k++) {

                    var material = trade.materials[k];
                    rows.push(['', '', material.materialName, '___']);
                }
            }
        }
    }

    options.doc.text('Project Name: ' + project.projectName, 10, 40);

    options.doc.autoTable(columns, rows, {
        startY: 80,
        margin: { horizontal: 10 },
        columnStyles: { text: { columnWidth: 200 } }
    });


    options.doc.save('Materials by specific trades by Locations.pdf');

}