﻿using Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpecSheetsCore
{
    public class ConfigurationManager
    {
        public static void Settings(IConfigurationRoot Configuration)
        {
            AppConfiguration.ConnectionString = Configuration[ConfigurationKeys.ConnectionString];
        }
    }

    public class ConfigurationKeys
    {
        public static string ConnectionString = "ConnectionStrings:DefaultConnection";
    }
}
