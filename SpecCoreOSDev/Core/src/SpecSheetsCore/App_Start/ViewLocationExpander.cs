﻿using Microsoft.AspNetCore.Mvc.Razor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpecSheetsCore
{
    public class ViewLocationExpander : IViewLocationExpander
    {
        public void PopulateValues(ViewLocationExpanderContext context)
        {
            // nothing here
        }

        public IEnumerable<string> ExpandViewLocations(ViewLocationExpanderContext context, IEnumerable<string> viewLocations)
        {
            var area = context.ActionContext.ActionDescriptor.RouteValues.FirstOrDefault(rc => rc.Key == "area");
            var additionalLocations = new LinkedList<string>();

            if (area.Key != null && area.Value != null)
            {
                additionalLocations.AddLast($"/Views/{area.Value}" + "{1}/{0}.cshtml");
            }

            return viewLocations.Concat(additionalLocations);
        }
    }
}
