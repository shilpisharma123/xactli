﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpecSheetsCore
{
	public static class HttpRequestExtensions
	{
		public static string GetRawUrl(this HttpRequest request)
		{
			var httpContext = request.HttpContext;
			return $"{httpContext.Request.Scheme}://{httpContext.Request.Host}{httpContext.Request.Path}{httpContext.Request.QueryString}";
		}

		public static string GetRawUrlWithoutQueryString(this HttpRequest request)
		{
			var httpContext = request.HttpContext;
			return $"{httpContext.Request.Scheme}://{httpContext.Request.Host}";
		}
	}
}
