USE [SpecSheetsDB]
GO
/****** Object:  Table [dbo].[tblContact]    Script Date: 8/11/2016 8:22:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblContact](
	[ContactId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[Email] [nvarchar](150) NULL,
	[Phone] [nvarchar](50) NULL,
	[Notes] [ntext] NULL
)

GO
/****** Object:  Table [dbo].[tblCustomer]    Script Date: 8/11/2016 8:22:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCustomer](
	[CustomerId] [int] IDENTITY(1,1) NOT NULL,
	[AspNetUsersId] [nvarchar](450) NOT NULL
)

GO
/****** Object:  Table [dbo].[tblLocationTradeMaterial]    Script Date: 8/11/2016 8:22:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblLocationTradeMaterial](
	[LocationTradeMaterialId] [int] IDENTITY(1,1) NOT NULL,
	[LocationTypeId] [nchar](10) NOT NULL,
	[TradeId] [int] NOT NULL,
	[MaterialId] [int] NOT NULL,
	[LowBudget] [money] NULL,
	[HighBudget] [money] NULL
)

GO
/****** Object:  Table [dbo].[tblLocationType]    Script Date: 8/11/2016 8:22:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblLocationType](
	[LocationTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NULL,
	[Category] [nvarchar](150) NULL
)

GO
/****** Object:  Table [dbo].[tblMaterial]    Script Date: 8/11/2016 8:22:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMaterial](
	[MaterialId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL
)

GO
/****** Object:  Table [dbo].[tblProject]    Script Date: 8/11/2016 8:22:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblProject](
	[ProjectId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NULL,
	[Address] [nvarchar](200) NULL,
	[City] [nvarchar](100) NULL,
	[State] [nvarchar](2) NULL,
	[Zip] [nvarchar](15) NULL,
	[Owner] [nvarchar](150) NULL,
	[Phone] [nvarchar](150) NULL,
	[Email] [nvarchar](200) NULL,
	[AspNetUsersId] [nvarchar](450) NOT NULL
)

GO
/****** Object:  Table [dbo].[tblProjectAspNetUsers]    Script Date: 8/11/2016 8:22:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblProjectAspNetUsers](
	[ProjectAspNetUsersId] [int] IDENTITY(1,1) NOT NULL,
	[ProjectId] [int] NOT NULL,
	[AspNetUsersId] [nvarchar](450) NOT NULL
)

GO
/****** Object:  Table [dbo].[tblProjectInvitedContact]    Script Date: 8/11/2016 8:22:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblProjectInvitedContact](
	[ProjectInvitedContactId] [int] IDENTITY(1,1) NOT NULL,
	[ProjectId] [int] NOT NULL,
	[ContactId] [int] NOT NULL,
	[Status] [int] NOT NULL
)

GO
/****** Object:  Table [dbo].[tblProjectLocation]    Script Date: 8/11/2016 8:22:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblProjectLocation](
	[ProjectLocationId] [int] IDENTITY(1,1) NOT NULL,
	[LocationTypeID] [int] NOT NULL,
	[Name] [nvarchar](150) NOT NULL
)

GO
/****** Object:  Table [dbo].[tblProjectLocationTradeMaterial]    Script Date: 8/11/2016 8:22:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblProjectLocationTradeMaterial](
	[ProjectLocationTradeMaterialId] [int] IDENTITY(1,1) NOT NULL,
	[ProjectLocationID] [int] NOT NULL,
	[LocationTradeMaterialID] [int] NOT NULL,
	[LowBudget] [money] NULL,
	[HightBudget] [money] NULL,
	[Estimate] [money] NULL,
	[Actual] [money] NULL,
	[ContactId] [int] NULL
)

GO
/****** Object:  Table [dbo].[tblTrade]    Script Date: 8/11/2016 8:22:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTrade](
	[TradeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL
)

GO
ALTER TABLE [dbo].[tblProjectInvitedContact] ADD  CONSTRAINT [DF_tblProjectInvitedContact_Status]  DEFAULT ((0)) FOR [Status]
GO
