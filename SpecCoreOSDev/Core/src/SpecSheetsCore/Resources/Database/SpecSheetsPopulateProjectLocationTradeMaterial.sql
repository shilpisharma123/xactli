-- =============================================================================================
-- Create Stored Procedure Template for Azure SQL Database and Azure SQL Data Warehouse Database
-- =============================================================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE PopulateProjectLocationTradeMaterial_sp
	@CustomerId int,
	@ProjectLocationId int
	
AS
BEGIN
	-- some logic here to check permission to update this project by CustomerID
	
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[tblProjectLocationTradeMaterial]
           ([ProjectLocationId]
           ,[LocationTradeMaterialId]
           ,[LowBudget]
           ,[HightBudget]
           )
     SELECT
	   @ProjectLocationID
       ,LocationTradeMaterialId
      ,[LowBudget]
      ,[HighBudget]
  FROM [dbo].[tblLocationTradeMaterial]
  where LocationTypeId = (Select LocationTypeId from tblProjectLocation where ProjectLocationId = @ProjectLocationId)
END
GO
