﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpecSheetsCore.Models.AdminViewModels
{
    public class TradeViewModel
    {
        public Trade trade { get; set; }

        public List<Trade> trades { get; set; }
    }
}
