﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpecSheetsCore.Models.AdminViewModels
{
    public class TagViewModel
    {
        public Tag tag { get; set; }

        public List<Tag> tags { get; set; }
    }
}
