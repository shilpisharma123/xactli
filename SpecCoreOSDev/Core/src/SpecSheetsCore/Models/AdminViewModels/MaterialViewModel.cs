﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpecSheetsCore.Models.AdminViewModels
{
    public class MaterialViewModel
    {
        public List<Material> materials { get; set; }

        public Material material { get; set; }
    }
}
