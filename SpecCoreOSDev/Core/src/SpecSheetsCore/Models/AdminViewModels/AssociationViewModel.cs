﻿using Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpecSheetsCore.Models.AdminViewModels
{
    public class AssociationViewModel
    {
        public IEnumerable<SelectListItem> locations { get; set; }

        public IEnumerable<SelectListItem> trades { get; set; }

        public IEnumerable<SelectListItem> materials { get; set; }

        public LocationTradeMaterialAssociation association { get; set; }

        public List<AssociationValues> associations { get; set; }

        public AssociationValues editAssociation { get; set; }
    }
}
