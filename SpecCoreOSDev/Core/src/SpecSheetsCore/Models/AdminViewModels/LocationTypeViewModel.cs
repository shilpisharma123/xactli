﻿using Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpecSheetsCore.Models.AdminViewModels
{
    public class LocationTypeViewModel
    {
        public List<LocationType> locationTemplates { get; set; }

        public LocationType locationType { get; set; }
    }
}
