﻿using Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpecSheetsCore.Models.LoggedInViewModels
{
    public class LocationViewModel
    {
        public LocationViewModel()
        {
            trade = new Trade();
            projectTradeMaterialImage = new ProjectTradeMaterialImage();
            tag = new Tag();
        }

        public bool IsOwner { get; set; }

        public Location location { get; set; }

        public int SelectedTradeId { get; set; }

        public List<LocationTrade> locationTrades { get; set; }

        public IEnumerable<SelectListItem> LocationTypes { get; set; }

        public Trade trade { get; set; }

        public Material material { get; set; }

        public IEnumerable<SelectListItem> Trades { get; set; }

        public IEnumerable<SelectListItem> Materials { get; set; }

        public ProjectTradeMaterialImage projectTradeMaterialImage { get; set; }

        //Added to get all images to bind
        public List<ProjectTradeMaterialImage> projectImages { get; set; }
        public Tag tag { get; set; }

        public IEnumerable<SelectListItem> TagTypes { get; set; }

        //Newly added for multiple image upload
        public int ProjectId { get; set; }

        //added by shilpi to add document to application (pdf /excel) all types
        public List<ProjectDocument> ProjectDocument { get; set; }
        //added by shilpi to get list of all trade based on project Id
        public List<Trade> TradeList { get; set; }

	
    }
}

