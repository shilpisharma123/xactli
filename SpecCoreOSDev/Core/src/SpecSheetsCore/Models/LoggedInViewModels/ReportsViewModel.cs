﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpecSheetsCore.Models.LoggedInViewModels
{
    public class ReportsViewModel
    {
		public List<SelectListItem> Projects { get; set; }
	}
}
