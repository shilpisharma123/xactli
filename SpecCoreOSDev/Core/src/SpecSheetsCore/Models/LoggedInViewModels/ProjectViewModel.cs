﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpecSheetsCore.Models.LoggedInViewModels
{
    public class ProjectViewModel
    {
        public ProjectDetails project { get; set; }

        public List<LocationTrade> locationTrades { get; set; }
    }
}
