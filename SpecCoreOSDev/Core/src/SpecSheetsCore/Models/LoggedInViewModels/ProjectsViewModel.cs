﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpecSheetsCore.Models.LoggedInViewModels
{
    public class ProjectsViewModel
    {
        public ProjectsViewModel()
        {
            projects = new List<Project>();
        }
		public bool IsInvitedUser { get; set; }
		public List<Project> projects { get; set; }
    }
}
