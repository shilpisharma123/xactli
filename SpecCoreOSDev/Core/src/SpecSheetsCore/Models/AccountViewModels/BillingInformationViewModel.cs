﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace SpecSheetsCore.Models.AccountViewModels
{
    public class BillingInformationViewModel
    {
		[Display(Name = "Name On Card")]
        public string NameOnCard { get; set; }

        [Display(Name = "Billing Address")]
		public string BillingAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }

		[Display(Name = "Phone Number")]
		public string PhoneNumber { get; set; }

		[Display(Name = "Credit Card No")]
		public string CreditCardNo { get; set; }

		[Display(Name = "Billing Data")]
		public DateTime BillingDate { get; set; }

    }
}
