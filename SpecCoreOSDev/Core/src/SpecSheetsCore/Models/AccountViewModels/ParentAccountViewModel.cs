﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpecSheetsCore.Models.AccountViewModels
{
    public class ParentAccountViewModel
    {
		public ParentAccountViewModel()
		{
			InvitedUsers = new List<InvitedUser>();
		}


        public AccountInformationViewModel AccountInfoViewModel { get; set; }
        public BillingInformationViewModel BillingInformationViewModel { get; set; }

		public List<InvitedUser> InvitedUsers { get; set; }
    }
}
