﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpecSheetsCore.Models.ProjectViewModels
{
    public class InvitedUserViewModel
    {
		public InvitedUserViewModel()
		{
			
		}

		public int ProjectId { get; set; }

		public string ProjectName { get; set; }
		public List<InvitedUser> InvitedUsers { get; set; }
    }
}
