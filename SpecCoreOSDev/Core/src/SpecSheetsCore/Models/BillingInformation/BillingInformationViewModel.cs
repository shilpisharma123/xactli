﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SpecSheetsCore.Models.ManageViewModels
{
    public class BillingInformationViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string BillingAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string PhoneNumber { get; set; }
        public bool AddressCheck { get; set; }
        public string CreditCardNo { get; set; }
        public string BillingDate { get; set; }
    }
}
