using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SpecSheetsCore.Models.ManageViewModels;
using System.Data.SqlClient;
using Repository;
using System.Data;
using Models;

namespace SpecSheetsCore.Controllers
{
    public class PaymentController : Controller
    {

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult BillingInformation()
        {
            return View();
        }

        public IActionResult PaymentInformation()
        {
            return View();
        }

        [HttpPost]
        public IActionResult SaveBillingInfo(BillingInformation billinginfo)
        {

            int result = new AccountRepository().SaveBillingInfo(billinginfo);
            return View();

            }
        }
    
}