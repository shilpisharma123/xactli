﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Repository;
using SpecSheetsCore.Models;

namespace SpecSheetsCore.Controllers
{
	public class HomeController : Controller
    {
        private AppSettings _settings;
        public HomeController(IOptions<AppSettings> settings)
        {
            _settings = settings.Value;
          //_settings.StringSetting == "My Value";
        }

        public IActionResult Index()
        {
            
            ViewData["environment"] = _settings.ApplicationTitle;
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Why Spec Sheets?";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Xactli - Spec Sheets.";

            return View();
        }

        public IActionResult ProductDetials()
        {
            return View();
        }

        public IActionResult PaymentInformation()
        {
            return View();    
        }

        public IActionResult Error()
        {
            return View();
        }

		public IActionResult ProjectExport(int id)
		{
			var projectDetails = new ReportsRepository().ReportGetNormalizedProject(id);

			return new JsonResult(projectDetails);
		}
    }
}
