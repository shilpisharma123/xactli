﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Repository;
using Models;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SpecSheetsCore.Controllers
{
    [Route("api/[controller]")]
    public class ProjectsWebAPIController : Controller
    {
        // GET: api/values
        [HttpGet]
        public IEnumerable<Project> Get()
        {
            var list = new ProjectRepository().GetProjects();
            return list;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Project Get(int id)
        {
            var project = new ProjectRepository().GetProject(id);
            return project;
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
            if (!ModelState.IsValid)
            {
                //return BadRequest(ModelState);
            }

            //if (model.Id != setting.Id)
            //{
            //    return BadRequest();
            //}

        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
