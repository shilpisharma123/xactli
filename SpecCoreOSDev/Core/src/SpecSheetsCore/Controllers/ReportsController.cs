﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Repository;
using SpecSheetsCore.Models.LoggedInViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Http;
using SpecSheetsCore.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SpecSheetsCore.Controllers
{
	[Authorize]
	public class ReportsController : Controller
	{
		private readonly UserManager<ApplicationUser> _userManager;

		public ReportsController(UserManager<ApplicationUser> userManager)
		{
			_userManager = userManager;
		}

		// GET: /<controller>/
		public async Task<IActionResult> Index()
		{
			// Generate the token and send it
			var user = await GetCurrentUserAsync();
			if (user == null)
			{
				return View("Error");
			}

			ReportsViewModel reports = new ReportsViewModel();

			reports.Projects = new List<SelectListItem>();
			reports.Projects.Add(new SelectListItem() { Text = "Select Project", Value = "0" });

			reports.Projects.AddRange(new ProjectRepository().GetProjectsByUserAccess(user.Id).Select(x => new SelectListItem() { Text = x.ProjectName, Value = x.ProjectId.ToString() }).ToList());

			return View(reports);
		}

		public JsonResult GetLocations(int ProjectId)
		{
			List<SelectListItem> ProjectLocations = new List<SelectListItem>();
			ProjectLocations.Add(new SelectListItem() { Text = "ALL", Value = "0" });
			ProjectLocations.AddRange(new LocationRepository().GetLocations(ProjectId).Select(x => new SelectListItem() { Text = x.LocationName, Value = x.LocationId.ToString() }).ToList());

			return Json(new SelectList(ProjectLocations, "Value", "Text"));
		}

		public JsonResult GetTrades(int ProjectId)
		{
			List<SelectListItem> ProjectLocations = new List<SelectListItem>();
			ProjectLocations.Add(new SelectListItem() { Text = "ALL", Value = "0" });
			ProjectLocations.AddRange(new TradeRepository().GetTradesByProjectId(ProjectId).Select(x => new SelectListItem() { Text = x.TradeName, Value = x.TradeId.ToString() }).ToList());

			return Json(new SelectList(ProjectLocations, "Value", "Text"));
		}

		public JsonResult GetUniqueTrades(int ProjectId)
		{
			List<SelectListItem> ProjectLocations = new List<SelectListItem>();
			ProjectLocations.Add(new SelectListItem() { Text = "ALL", Value = "0" });
			ProjectLocations.AddRange(new TradeRepository().GetUniqueTradesByProjectId(ProjectId).Select(trade => new SelectListItem() { Text = trade, Value = "1" }).ToList());

			return Json(new SelectList(ProjectLocations, "Value", "Text"));
		}

		public IActionResult ProjectDetails(int Id)
		{
			var projectDetails = new ProjectRepository().GetProjectDetailsWithTrades(Id);

			return new JsonResult(projectDetails);
		}

		public IActionResult ProjectDetailPage(int Id)
		{
			var projectDetails = new ProjectRepository().GetProjectDetailsWithTrades(Id);

			return new JsonResult(projectDetails);
		}

		public IActionResult LocationPageReport(int Id)
		{
			var locationDetails = new LocationRepository().GetLocation(Id, "");
			return new JsonResult(locationDetails);
		}

		public IActionResult LocationTradeReport(int Id, int TradeId)
		{
			var locationDetails = new LocationRepository().GetProjectLocationWithTrades(Id, TradeId);
			return new JsonResult(locationDetails);
		}

		public IActionResult LocationByProjectTrades(int Id, int TradeId) // Id = ProjectId, TradeId
		{
			var projectDetails = new ProjectRepository().GetProjectDetailsWithTrades(Id);
			return new JsonResult(projectDetails);
		}

		public IActionResult ReportTradesByLocation(int Id, int LocationId = 0)
		{
			var projectDetails = new ReportsRepository().ReportTradesByLocations(Id, LocationId);
			return new JsonResult(projectDetails);
		}

		//public IActionResult ReportLocationsByTrades(int Id, int TradeId = 0, string TradeName= "")
		//{
		//	var projectDetails = new ReportsRepository().ReportLocationsByTrades(Id, TradeId);
		//	return new JsonResult(projectDetails);
		//}

		public IActionResult ReportLocationsByTrades(int Id, string TradeName = "ALL")
		{
			int ProjectId = Id;
			var projectDetails = new ReportsRepository().ReportLocationsByTrades(ProjectId, TradeName);
			return new JsonResult(projectDetails);
		}

		private Task<ApplicationUser> GetCurrentUserAsync()
		{
			return _userManager.GetUserAsync(HttpContext.User);
		}
	}
}
