﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Models;
using Repository;
using SpecSheetsCore.Models;
using SpecSheetsCore.Models.AdminViewModels;
using SpecSheetsCore.Models.LoggedInViewModels;
using SpecSheetsCore.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using System.Web;
using Microsoft.Office.Interop.Word;
using System.Text.RegularExpressions;


// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SpecSheetsCore.Controllers
{
    [Authorize]
    public class ProjectsController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly ISmsSender _smsSender;
        private readonly ILogger _logger;
		private readonly IHostingEnvironment _hostingEnvironment;

		public ProjectsController(
        UserManager<ApplicationUser> userManager,
        SignInManager<ApplicationUser> signInManager,
        IEmailSender emailSender,
        ISmsSender smsSender,
        ILoggerFactory loggerFactory,
		IHostingEnvironment hostingEnvironment)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _smsSender = smsSender;
            _logger = loggerFactory.CreateLogger<ProjectsController>();
			_hostingEnvironment = hostingEnvironment;
		}

        // GET: /<controller>/
        public async Task<IActionResult> Index()
        {
            // Generate the token and send it
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return View("Error");
            }

            ProjectsViewModel model = new ProjectsViewModel();

            model.IsInvitedUser = new InviteUserRepository().CheckIsUserInvited(user.Id);
            model.projects = new ProjectRepository().GetProjects(user.Id);
            model.projects.ForEach(x => x.IsOwner = (x.AspNetUsersId == user.Id));

            return View(model);
        }

        //Nabeel
        //[Authorize(Roles = "Retail")]
        public async Task<IActionResult> Add()
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return View("Error");
            }

            ProjectViewModel model = new ProjectViewModel();
            model.project = new ProjectDetails();

            return View("AddProject", model);
        }

        //Nabeel
        [HttpPost]
        //[Authorize(Roles = "Retail")]
        public async Task<IActionResult> Add(ProjectViewModel model)
        {
            // Generate the token and send it
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return View("Error");
            }

            model.project.AspNetUsersId = user.Id;

            new ProjectRepository().SaveProject(model.project);

            if (model.project.ProjectId > 0)
            {
                return RedirectToAction("Details", new { Id = model.project.ProjectId });
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        public async Task<IActionResult> Details(int Id)
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return View("Error");
            }

            ProjectViewModel model = new ProjectViewModel();
            model.project = new ProjectRepository().GetProjectDetailsWithPermissions(Id, user.Id);

            model.project.IsOwner = (model.project.AspNetUsersId == user.Id);

            var locationTrades = new LocationRepository().GetTradesByProjectId(Id);
            model.locationTrades = locationTrades;

            locationTrades.Insert(0, new LocationTrade() { TradeId = 0, TradeName = "ALL" });
            ViewBag.Trades = new SelectList(locationTrades, "TradeId", "TradeName");

            return View("ProjectDetails", model);
        }

        public IActionResult AddProjectLocationTrade(LocationViewModel model) // Location Id
        {
            // Trade, LocationTypeId, ProjectLocationId
            int ProjectLocationTradeId = new ProjectRepository().SaveProjectTrade(model.trade, model.location.LocationId);

            var projectlocationTrade = new
            {
                LocationId = model.location.LocationId,
                LocationName = model.location.LocationName,
                ProjectLocationTradeId = ProjectLocationTradeId,
                ProjectLocationTradeName = model.trade.Name
            };

            return base.Json(new { Message = true, result = projectlocationTrade });
        }

		[HttpPost]
        public async Task<IActionResult> UpdateMultipleImagesNotesTags()
        {
            var files = Request.Form.Files;
            var projectId = Request.Form["projectId"][0];
            var locationId = Request.Form["locationId"][0];
            var tradeId = Request.Form["tradeId"][0];
            var materialId = Request.Form["materialId"][0];
            var arrSelectedImgIds = Request.Form["arrSelectedImgIds"][0];
            var notesText = Request.Form["notesText"][0];
            string[] ids = arrSelectedImgIds.Split(',');

            List<ProjectTradeMaterialImage> lstMultipleSelectedImages = new List<ProjectTradeMaterialImage>();

            foreach (var item in ids)
            {
                ProjectTradeMaterialImage model = new ProjectTradeMaterialImage();
                model.Id = int.Parse(item);
                lstMultipleSelectedImages.Add(model);
            }
            var user1 = await GetCurrentUserAsync();
            var Sequence = GetImagesOrderSequence(int.Parse(projectId), int.Parse(tradeId), int.Parse(materialId), user1.Id);
            NoteHistory modelNote = new NoteHistory();

            ProjectTags pTag = new ProjectTags();

            foreach (var multipleSelectedImages in lstMultipleSelectedImages)
            {
                using (var stream = new MemoryStream())
                {
                    var user = await GetCurrentUserAsync();
                    multipleSelectedImages.Id = multipleSelectedImages.Id;
                    multipleSelectedImages.ProjectId = int.Parse(projectId);
                    multipleSelectedImages.LocationId = int.Parse(locationId);
                    multipleSelectedImages.TradeId = int.Parse(tradeId);
                    multipleSelectedImages.MaterialId = int.Parse(materialId);
                    multipleSelectedImages.AspNetUsersId = user.Id;
                    if (Sequence != null)
                    {
                        foreach (var ord in Sequence.ToList())
                        {
                            multipleSelectedImages.ImageOrderSequence = Convert.ToInt32(ord.ImageOrderSequence) + 1;
                        }
                    }
                    new ProjectRepository().UpdateMultipleSelectedImgTags(multipleSelectedImages.Id, multipleSelectedImages.ProjectId, multipleSelectedImages.LocationId, multipleSelectedImages.TradeId, multipleSelectedImages.MaterialId, multipleSelectedImages.AspNetUsersId, multipleSelectedImages.ImageOrderSequence);

                    //Inserting notes
                    modelNote.Notes = notesText;
                    modelNote.DateTimeStamp = DateTime.Now;
                    modelNote.AspNetUsersId = user.Id;
                    new ProjectRepository().SaveNoteHistory(Convert.ToInt32(multipleSelectedImages.Id), modelNote.Notes, modelNote.DateTimeStamp, modelNote.AspNetUsersId);


                    //save tags KS 13/12/2018
                    pTag.LocationId = int.Parse(locationId);
                    pTag.TradeId = int.Parse(tradeId);
                    pTag.MaterialId = int.Parse(materialId);
                    pTag.ProjectLocationTradeMaterialImageId = Convert.ToInt32(multipleSelectedImages.Id);
                    pTag.ProjectId = int.Parse(projectId);
                    pTag.AspNetUsersId = user.Id;
                    new ProjectRepository().SaveProjectTags(pTag);
                }
            }
            LocationViewModel locationViewModel = new LocationViewModel();
            List<ProjectTradeMaterialImage> lstImages = new List<ProjectTradeMaterialImage>();
            var userId = await GetCurrentUserAsync();
            var userLogged = userId.Id;
            var data = new ProjectRepository().GetProjectMultipleImagesWithoutTrade(int.Parse(projectId), userLogged.ToString());
            locationViewModel.ProjectId = int.Parse(projectId);
            locationViewModel.projectImages = data;
            return Json(locationViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> AddMultipleImages()
        {
            var files = Request.Form.Files;
            var projectId = Request.Form["projectId"][0];


            foreach (var formFile in files)
            {
                ProjectTradeMaterialImage model = new ProjectTradeMaterialImage();
                if (formFile.Length > 0)
                {
                    using (var stream = new MemoryStream())
                    {
                        var user = await GetCurrentUserAsync();
                        await formFile.CopyToAsync(stream);
                        model.ProjectId = int.Parse(projectId);
                        model.LocationId = 0;
                        model.TradeId = 0;
                        model.MaterialId = 0;
                        model.Image = stream.ToArray();
                        model.AspNetUsersId = user.Id;

                        object MaterialImageId = new ProjectRepository().SaveProjectTradeMaterialImage(model.Image, model.ProjectId, model.LocationId, model.TradeId, model.MaterialId, model.AspNetUsersId, model.ImageOrderSequence);

                    }
                }
            }
            LocationViewModel locationViewModel = new LocationViewModel();
            List<ProjectTradeMaterialImage> lstImages = new List<ProjectTradeMaterialImage>();
            var userId = await GetCurrentUserAsync();
            var userLogged = userId.Id;
            var data = new ProjectRepository().GetProjectMultipleImagesWithoutTrade(int.Parse(projectId), userLogged.ToString());
            locationViewModel.ProjectId = int.Parse(projectId);
            locationViewModel.projectImages = data;
            return Json(locationViewModel);
        }


        //ProjectController
        //Modified by :Jeet Bhandari
        //Modified date:02-Nov-2018
        [HttpPost]
        public async Task<IActionResult> AddImages(int projectId, int tradeId, int materialId, List<IFormFile> Image, int locationId,string note1)
        {
            if (Image.Count == 0)
            {
                TempData["message"] = "Please Upload Image";
            }
            else if (note1 == "" || note1==null)
            {
                TempData["message"] = "Please Add Note";
            }
           
            else if (Image.Count > 0 || note1 != null)
            {
                if (Image[0].ContentType.ToLower() == ("image/jpg") || Image[0].ContentType.ToLower() == ("image/jpeg") || Image[0].ContentType.ToLower() == ("image/png"))
                {
                    foreach (var item in Image)
                    {
                        ProjectTradeMaterialImage model = new ProjectTradeMaterialImage();

                        ProjectTags pTag = new ProjectTags();

                        NoteHistory modelNote = new NoteHistory();
                        if (item.Length > 0)
                        {
                            using (var stream = new MemoryStream())
                            {

                                var user = await GetCurrentUserAsync();
                                var Sequence = GetImagesOrderSequence(projectId, tradeId, materialId, user.Id);
                                await item.CopyToAsync(stream);
                                model.ProjectId = projectId;
                                model.LocationId = locationId;
                                model.TradeId = tradeId;
                                model.MaterialId = materialId;
                                model.Image = stream.ToArray();
                                modelNote.Notes = note1;
                                modelNote.DateTimeStamp = DateTime.Now;
                                model.AspNetUsersId = user.Id;
                                modelNote.AspNetUsersId = user.Id;
                                if (Sequence != null)
                                {
                                    foreach (var ord in Sequence.ToList())
                                    {
                                        model.ImageOrderSequence = Convert.ToInt32(ord.ImageOrderSequence) + 1;
                                    }
                                }

                                object MaterialImageId = new ProjectRepository().SaveProjectTradeMaterialImage(model.Image, model.ProjectId, model.LocationId, model.TradeId, model.MaterialId, model.AspNetUsersId, model.ImageOrderSequence);
                                new ProjectRepository().SaveNoteHistory(Convert.ToInt32(MaterialImageId), modelNote.Notes, modelNote.DateTimeStamp, model.AspNetUsersId);
                                //save tags KS 11/12/2018
                                pTag.LocationId = locationId;
                                pTag.TradeId = tradeId;
                                pTag.MaterialId = materialId;
                                pTag.ProjectLocationTradeMaterialImageId = Convert.ToInt32(MaterialImageId);
                                pTag.ProjectId = projectId;
                                pTag.AspNetUsersId = user.Id;
                                new ProjectRepository().SaveProjectTags(pTag);



                                
                            }
                        }
                    }
                    TempData["message"] = "Image Uploaded Successfully";
                    TempData["materialId"] = materialId;
                }
                else
                {
                    TempData["message"] = "Please select correct file format of image";
                }
            }
            return RedirectToAction("Details", "Locations", new { id = locationId });
        }
        public List<ProjectTradeMaterialImage> GetImagesOrderSequence(int projectId, int tradeId, int materialId, string AspNetUsersId)
        {
            return new ProjectRepository().GetProjectTradeMaterialImageSequence(projectId, tradeId, materialId, AspNetUsersId);
        }


        public ActionResult GetImagesNotes(int projectId,int locationId, int tradeId, int materialId)
        {
            return Json(new ProjectRepository().GetAllProjectTradeMaterialImage(projectId,locationId, tradeId, materialId));
        }

        public IActionResult NotesById(int Id)
        {
            return Json(new ProjectRepository().GetNotes(Id));
        }
        //To get the values of the location,material,trade by their ids to display : Jeet 4/12/2018
        public IActionResult GetDetailsByID(int locationId, int tradeId, int materialId)
        {
            return Json(new ProjectRepository().GetDetailsByID(locationId, tradeId, materialId));
        }

        //To update the final selection value at tblLocationTradeMaterialImages --Jeet 5/12/2018
        [HttpPost]
        public async Task<IActionResult> UpdateFinalSelection(int Id, bool FinalSelection )
        {

            ProjectTradeMaterialImage model = new ProjectTradeMaterialImage();
            using (var stream = new MemoryStream())
            {
                var user = await GetCurrentUserAsync();
                model.Id = Id;
                new ProjectRepository().UpdateFinalSelectionDetails(Id, FinalSelection,user.Id);
                //add note when final selection is done KS 11/12/2018
                if (FinalSelection == true)
                {
                    new ProjectRepository().SaveNoteHistory(Id, "This image is selected as Final selection.", DateTime.Now, user.Id);
                }
                else if(FinalSelection==false)
                {
                    new ProjectRepository().SaveNoteHistory(Id, "This image is removed from Final selection.", DateTime.Now, user.Id);
                }


            }
            return Json(true);
        }

		//To Save the Data in Project tags --KS 6/12/2018
        //Save the data now according to single values and not in array--KS :11/12/2018 
		[HttpPost]
		public async Task<IActionResult> SaveProjectTags(int LocationId, int TradeId, int MaterialId, int projectId, string imageId)
		{

			ProjectTags model = new ProjectTags();
			using (var stream = new MemoryStream())
			{
                var user = await GetCurrentUserAsync();
                model.LocationId = LocationId;
				model.TradeId = TradeId;
				model.MaterialId = MaterialId;
                model.ProjectId = projectId;
                model.ProjectLocationTradeMaterialImageId = int.Parse(imageId);
                model.AspNetUsersId = user.Id;
                new ProjectRepository().SaveProjectTags(model);
				


			}
			return Json(true);
		}

        //To Delete the Data in Project tags according to the loc,trade,mat,proj,imag--KS 12/12/2018
        [HttpPost]
        public async Task<IActionResult> DeleteProjectTags(int LocationId, int TradeId, int MaterialId, int projectId, string imageId)
        {

            ProjectTags model = new ProjectTags();
            using (var stream = new MemoryStream())
            {
                var user = await GetCurrentUserAsync();
                model.LocationId = LocationId;
                model.TradeId = TradeId;
                model.MaterialId = MaterialId;
                model.ProjectId = projectId;
                model.ProjectLocationTradeMaterialImageId = int.Parse(imageId);
                model.AspNetUsersId = user.Id;
                new ProjectRepository().DeleteProjectTags(model);



            }
            return Json(true);
        }
        //To get all the tags associated with image id  KS: 11/12/2018
        public IActionResult GetTagsByImageId(int ProjectId,int Id)
        {
            return Json(new ProjectRepository().GetProjectTagsByImageId(ProjectId,Id));
        }

        public IActionResult NotesHistoryId(int Id)
        {
            return Json(new ProjectRepository().GetNotesHistory(Id));
        }

        [HttpPost]
        public IActionResult UpdateNotes(int Id, string notes, string aspNetUsersId, int locationId)
        {
            NoteHistory model = new NoteHistory();
            using (var stream = new MemoryStream())
            {
                model.Notes = notes;
                model.DateTimeStamp = DateTime.Now;
                model.AspNetUsersId = aspNetUsersId;
                new ProjectRepository().SaveNoteHistory(Id, model.Notes, model.DateTimeStamp, model.AspNetUsersId);
            }
            return Json(true);
        }
        public List<ProjectTradeMaterialImage> GetProjectTradeMaterialById(int id)
        {
            return new ProjectRepository().GetProjectTradeMaterialImageById(id);
        }

        public ActionResult GetImages(int projectId, int tradeId, int materialId)
        {
            return Json(new ProjectRepository().GetProjectTradeMaterialImages(projectId, tradeId, materialId));
        }

        public ActionResult DeleteImage(int imageId)
        {
            return Json(new ProjectRepository().DeleteMaterialImage(imageId));
        }

        public IActionResult AddMaterial(LocationViewModel model) // Location Id
        {
            int ProjectLocationTradeId = model.trade.TradeId;
            int ProjectLocationTradeMaterialId = new ProjectRepository().SaveProjectMaterial(model.material, ProjectLocationTradeId);

            var projectlocationTradeMaterial = new
            {
                ProjectLocationTradeId = ProjectLocationTradeId,
                ProjectLocationTradeMaterialId = ProjectLocationTradeMaterialId,
                ProjectLocationTradeMaterialName = model.material.Name + " - " + model.material.PlanReference,
                Cost = model.material.Cost,
                Qty = model.material.Quantity,
                Estimate = model.material.Quantity * model.material.Cost,
                PlanReference = model.material.PlanReference,
                Manufacturer = model.material.Manufacturer,
                Model = model.material.Model,
                Finish = model.material.Finish,
                LeadTime = model.material.LeadTime,
                Description = model.material.Description,
                IsNew = model.material.IsNew
            };

            return base.Json(new { Message = true, result = projectlocationTradeMaterial });
        }

        public async Task<IActionResult> DeleteProject(int projectId)
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return View("Error");
            }

            new ProjectRepository().DeleteProject(projectId, user.Id);

            return RedirectToAction("Index");
        }

        public IActionResult DeleteProjectTrade(int Id, int LocationId)
        {
            new ProjectRepository().DeleteProjectTrade(Id);

            return base.Json(new { Message = true, IsDeleted = true });
        }

        public ActionResult EditProjectTradeMaterial(int Id)
        {
            MaterialViewModel model = new MaterialViewModel();

            model.material = new ProjectRepository().GetMaterialBy(Id);

            var result = Newtonsoft.Json.JsonConvert.SerializeObject(model.material);

            return Content(result, "application/json");
        }

        public IActionResult DeleteProjectMaterial(int Id)
        {
            // Id = ProjectLocationMaterialId
            new ProjectRepository().DeleteProjectMaterial(Id);

            return base.Json(new { Message = true, IsDeleted = true });
        }

        public bool UpdateEstimateDetails(int ProjectLocationMaterialId, string cost, string qty, string estimate)
        {
            double Cost = 0;
            double Qty = 0;
            double Estimate = 0;

            double.TryParse(cost, out Cost);
            double.TryParse(qty, out Qty);
            double.TryParse(estimate, out Estimate);

            new ProjectRepository().UpdateEstimateDetails(ProjectLocationMaterialId, Cost, Qty, Estimate);
            return true;
        }

        private Task<ApplicationUser> GetCurrentUserAsync()
        {
            return _userManager.GetUserAsync(HttpContext.User);
        }

        public async Task<IActionResult> UploadTagImages(int ProjectId)
        {
            LocationViewModel locationViewModel = new LocationViewModel();
            List<ProjectTradeMaterialImage> lstImages = new List<ProjectTradeMaterialImage>();
            var userId = await GetCurrentUserAsync();
            var userLogged = userId.Id;
            var data = new ProjectRepository().GetProjectMultipleImagesWithoutTrade(ProjectId, userLogged.ToString());
            locationViewModel.ProjectId = ProjectId;
            locationViewModel.projectImages = data;
            return View(locationViewModel);
        }

		[HttpPost]
		public async Task<IActionResult> GetCheckedImageSelected(List<int> Ids, int ProjectId)
		{
			LocationViewModel locationViewModel = new LocationViewModel();
			List<ProjectTradeMaterialImage> lstImages = new List<ProjectTradeMaterialImage>();
			var userId = await GetCurrentUserAsync();
			var userLogged = userId.Id;
			var data = new ProjectRepository().GetSelectedCheckedImagesModelPopupForTag(Ids, ProjectId, userLogged.ToString());
			locationViewModel.ProjectId = ProjectId;
			locationViewModel.projectImages = data;
			return Json(locationViewModel);
		}
		//Kadambini Sahoo //18/12/2018
	   //Get Note History by Projecctdocumentid
		public IActionResult NotesHistoryByDocId(int projectDocumentId)
		{
			return Json(new ProjectRepository().NotesHistoryByDocId(projectDocumentId));
		}
		// added by shilpi to allow upload of document
		[HttpPost]
        public async Task<IActionResult> AddDocument(IFormFile file)
        {
            var files = Request.Form.Files;
            var projectId = Request.Form["projectId"][0];
            foreach (var formFile in files)
            {
                ProjectDocument projObj = new ProjectDocument();
                if (formFile.Length > 0)
                {
					string randomName = DateTime.Now.Ticks.ToString();

						object documentFormat = 8;

						string webRootPath = _hostingEnvironment.WebRootPath;
						string contentRootPath = _hostingEnvironment.ContentRootPath;
						string path = webRootPath + "\\" + "projectDocument";
						string fileSavePath = path + "\\" + randomName + formFile.FileName;
						object htmlFilePath = fileSavePath + ".htm";

						//If Directory not present, create it.
						if (!Directory.Exists(path))
						{
							Directory.CreateDirectory(path);
						}
						if (formFile == null || formFile.Length == 0)
							return Content("file not selected");

						var fileName = Path.GetFileName(fileSavePath);
						formFile.CopyTo(new FileStream(fileSavePath, FileMode.Create));
						using (var stream = new MemoryStream())
						{
							var user = await GetCurrentUserAsync();
							await formFile.CopyToAsync(stream);
							projObj.ProjectId = int.Parse(projectId);
							projObj.TradeId = 0;
							projObj.FileType = formFile.ContentType;
							projObj.Document = stream.ToArray();
							projObj.AspNetUsersId = user.Id;
							projObj.DocumentName = randomName + formFile.FileName;
							object MaterialImageId = new ProjectRepository().AddProjectDocument(projObj.Document, projObj.ProjectId, projObj.TradeId, projObj.AspNetUsersId, projObj.FileType, projObj.DocumentName);
						}

                }
            }
            LocationViewModel locationViewModel = new LocationViewModel();
            List<ProjectDocument> lstImages = new List<ProjectDocument>();
            var userId = await GetCurrentUserAsync();
            var userLogged = userId.Id;
            var data = new ProjectRepository().GetAllProjectDocuments(int.Parse(projectId), userLogged.ToString());
            locationViewModel.ProjectId = int.Parse(projectId);
            locationViewModel.ProjectDocument = data;
            return Json(locationViewModel);
        }
        [HttpPost]
        public async Task<IActionResult> UpdateProjectDocumentDetails()
        {
            var files = Request.Form.Files;
            var projectId = Request.Form["projectId"][0];
            var tradeId = Request.Form["tradeId"][0];
            var notesText = Request.Form["notesText"][0];
            var projectDocumentId = Request.Form["projectDocumentId"][0];
            var documentType = Request.Form["documentType"][0];

            //Update Project Document table
            ProjectDocument model = new ProjectDocument();
            model.ProjectDocumentId = int.Parse(projectDocumentId);
            model.ProjectId = int.Parse(projectId);
            model.TradeId = int.Parse(tradeId);
            model.ProjectDocumentType = documentType;
            new ProjectRepository().UpdateProjectDocument(model.ProjectDocumentId, model.ProjectId, model.TradeId, model.ProjectDocumentType);
            //Insert Notes for project documents in History Table
            NoteHistory modelNote = new NoteHistory();
            modelNote.Notes = notesText;
            modelNote.DateTimeStamp = DateTime.Now;
            var user = await GetCurrentUserAsync();
            modelNote.AspNetUsersId = user.Id;
            modelNote.ProjectDocumentId = int.Parse(projectDocumentId);
            new ProjectRepository().SaveDocumentNoteHistory(Convert.ToInt32(projectDocumentId), modelNote.Notes, modelNote.DateTimeStamp, modelNote.AspNetUsersId);
            //todo
            LocationViewModel locationViewModel = new LocationViewModel();
            List<ProjectTradeMaterialImage> lstImages = new List<ProjectTradeMaterialImage>();
            var userId = await GetCurrentUserAsync();
            var userLogged = userId.Id;
            var data = new ProjectRepository().GetProjectMultipleImagesWithoutTrade(int.Parse(projectId), userLogged.ToString());
            locationViewModel.ProjectId = int.Parse(projectId);
            locationViewModel.projectImages = data;
            return Json(locationViewModel);
        }

        //Added by shilpi to add Page for Upload prof docs
        public async Task<IActionResult> UploadProjectDocuments(int ProjectId)
        {
            LocationViewModel locationViewModel = new LocationViewModel();
            List<ProjectDocument> lstImages = new List<ProjectDocument>();
            var userId = await GetCurrentUserAsync();
            var userLogged = userId.Id;
            var data = new ProjectRepository().GetAllProjectDocuments(ProjectId, userLogged.ToString());
            locationViewModel.ProjectId = ProjectId;
            locationViewModel.ProjectDocument = data;

            return View(locationViewModel);
        }
		//Code change by shilpi to download project document

		public async Task<FileResult> DownloadProjectDocument(string DocumentName)
		{
			//if (DocumentName == null)
			//	return Content("filename not present");

			var path = Path.Combine(
						   Directory.GetCurrentDirectory(),
						   "wwwroot", "projectDocument",DocumentName);

			var memory = new MemoryStream();
			using (var stream = new FileStream(path, FileMode.Open))
			{
				await stream.CopyToAsync(memory);
			}
			memory.Position = 0;
		
			return File(memory, GetContentType(path), Path.GetFileName(path));
		}
		private string GetContentType(string path)
		{
			var types = GetMimeTypes();
			var ext = Path.GetExtension(path).ToLowerInvariant();
			return types[ext];
		}
		private Dictionary<string, string> GetMimeTypes()
		{
			return new Dictionary<string, string>
			{
				{".txt", "text/plain"},
				{".pdf", "application/pdf"},
				{".doc", "application/vnd.ms-word"},
				{".docx", "application/vnd.ms-word"},
				{".xls", "application/vnd.ms-excel"},
				{".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
				{".png", "image/png"},
				{".jpg", "image/jpeg"},
				{".jpeg", "image/jpeg"},
				{".gif", "image/gif"},
				{".csv", "text/csv"},
				{".ppt", "application/vnd.ms-powerpoint"}
				
			};
		}
		//Code change by Shilpi for opening popup of document
		[HttpPost]
        public async Task<IActionResult> GetProjectDocumentDetails(int ProjectDocumentId, int ProjectId)
        {
            LocationViewModel locationViewModel = new LocationViewModel();
            List<ProjectDocument> lstImages = new List<ProjectDocument>();
            var userId = await GetCurrentUserAsync();
            var userLogged = userId.Id;
            var data = new ProjectRepository().GetSelectedDocumentModelPopupDetails(ProjectDocumentId, ProjectId, userLogged.ToString());
            locationViewModel.ProjectId = ProjectId;
            locationViewModel.ProjectDocument = data;
			foreach (var item in locationViewModel.ProjectDocument)
			{
				if (item.FileType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" || item.FileType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
				{
					string webRootPath = _hostingEnvironment.WebRootPath;
					string contentRootPath = _hostingEnvironment.ContentRootPath;
					string path = webRootPath + "\\" + "projectDocument";
					string fileSavePath = path + "\\" + item.DocumentName;
					//Read the saved Html File.
					string wordHTML = System.IO.File.ReadAllText(fileSavePath.ToString());
					//Loop and replace the Image Path.
					foreach (Match match in Regex.Matches(wordHTML, "<v:imagedata.+?src=[\"'](.+?)[\"'].*?>", RegexOptions.IgnoreCase))
					{
						wordHTML = Regex.Replace(wordHTML, match.Groups[1].Value, "projectDocument/" + match.Groups[1].Value);
					}
					item.DocumentDetailsString = wordHTML;
				}
			}
			var tradeList = new ProjectRepository().GetAllTradeList(ProjectId);
            locationViewModel.TradeList = tradeList;
            return Json(locationViewModel);
        }
    }
}


