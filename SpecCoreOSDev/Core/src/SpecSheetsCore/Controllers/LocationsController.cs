﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Repository;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;
using Models;
using SpecSheetsCore.Models.LoggedInViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using SpecSheetsCore.Models;
using SpecSheetsCore.Services;
using Microsoft.Extensions.Logging;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SpecSheetsCore.Controllers
{
	[Authorize]
	public class LocationsController : Controller
	{
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly SignInManager<ApplicationUser> _signInManager;
		private readonly IEmailSender _emailSender;
		private readonly ISmsSender _smsSender;
		private readonly ILogger _logger;

		public LocationsController(
			UserManager<ApplicationUser> userManager,
			SignInManager<ApplicationUser> signInManager,
			IEmailSender emailSender,
			ISmsSender smsSender,
			ILoggerFactory loggerFactory)
		{
			_userManager = userManager;
			_signInManager = signInManager;
			_emailSender = emailSender;
			_smsSender = smsSender;
			_logger = loggerFactory.CreateLogger<AccountController>();
		}

		// GET: /<controller>/
		public IActionResult Index()
		{
			return View();
		}

		//Nabeel
		public IActionResult Add(int Id) // Project Id
		{
			List<LocationType> locationTypes = new LocationRepository().GetLocationTypes();

			Location location = new Location();
			location.ProjectId = Id;

			LocationViewModel model = new LocationViewModel();
			model.location = location;
			model.LocationTypes = new SelectList(locationTypes, "LocationTypeId", "Name", location.TypeId);

			return View("Add", model);
		}

		//Nabeel
		[HttpPost]
		public IActionResult Add(LocationViewModel model)
		{
			int result = new LocationRepository().AddLocation(model.location);

			if (model.location.LocationId > 0)
			{
				return RedirectToAction("Details", "Locations", new { id = model.location.LocationId });
			}
			else
			{
				return RedirectToAction("Details", "Projects", new { id = model.location.ProjectId });
			}
		}

		

		[HttpPost]
		public async Task<IActionResult> DeleteProjectLocation(int projectLocationId, int projectId)
		{
			var user = await GetCurrentUserAsync();
			if (user == null)
			{
				return View("Error");
			}

			new ProjectRepository().DeleteProjectLocation(projectLocationId, user.Id);

			return RedirectToAction("Details", "Projects", new { id = projectId});
		}

		private Task<ApplicationUser> GetCurrentUserAsync()
		{
			return _userManager.GetUserAsync(HttpContext.User);
		}

        public async Task<IActionResult> Details(int Id, int TradeId = 0, int MaterialId = 0) // Location Id
        {
            LocationViewModel model = new LocationViewModel();

            LocationRepository repository = new LocationRepository();

            var user = await GetCurrentUserAsync();
            model.location = repository.GetLocation(Id, user.Id);
            model.LocationTypes = new SelectList(repository.GetLocationTypes(), "LocationTypeId", "Name", model.location.TypeId);

            model.IsOwner = model.location.IsOwner;

            var allTrades = new TradeRepository().GetTrades();
            allTrades.Insert(0, new Trade() { TradeId = -1, Name = "Other" });
            allTrades.Insert(0, new Trade() { TradeId = 0, Name = "Select" });
            model.Trades = new SelectList(allTrades, "TradeId", "Name");

            var allMaterials = new MaterialRepository().GetMaterials();
            allMaterials.Insert(0, new Material() { MaterialId = -1, Name = "Other" });
            allMaterials.Insert(0, new Material() { MaterialId = 0, Name = "Select" });
            model.Materials = new SelectList(allMaterials, "MaterialId", "Name");

            model.SelectedTradeId = TradeId;
            //var projectTradeMaterialImages = GetImages(Id, TradeId, MaterialId);            
            //model.projectImages = projectTradeMaterialImages;
            return View(model);
        }
        //Get the Images according to the project to bind
        public List<ProjectTradeMaterialImage> GetImages(int projectId,int locationId, int tradeId, int materialId)
        {
            return new ProjectRepository().GetAllProjectTradeMaterialImage(projectId, locationId, tradeId, materialId);
        }
        public IActionResult GetAllLocationTypes()
        {            
            return Json(new LocationRepository().GetLocationTypes());           
        }
        public IActionResult GetAllTrades()
        {
            return Json(new TradeRepository().GetTrades());
        }
        public IActionResult GetLocationByProjectId(int Id)
        {
            return Json(new LocationRepository().GetLocations(Id));
        }
        public IActionResult GetTradesByLocationId(int Id)
        {
            return Json(new LocationRepository().GetTradesByLocationId(Id));
        }
        public IActionResult GetMaterialsByTradeId(int Id)
        {
            return Json(new LocationRepository().GetMaterialsByTradeId(Id));
        }
        public IActionResult GetAllMaterials()
        {
            return Json(new MaterialRepository().GetMaterials());
        }
    }
}
