﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SpecSheetsCore.Models.AdminViewModels;
using Repository;
using Microsoft.AspNetCore.Mvc.Rendering;
using Models;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SpecSheetsCore.Controllers.Admin
{
    [Authorize]
    [Area("Admin")]
    public class AssociationController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            AssociationViewModel model = new AssociationViewModel();
            model.associations = new LocationTypeRepository().GetAssociation();
            
            return View(model);
        }

        public IActionResult Add()
        {
            AssociationViewModel model = new AssociationViewModel();
            model.association = new LocationTradeMaterialAssociation();

			var locationList = new LocationTypeRepository().GetLocationTypes().OrderBy(x => x.Name);
			var tradeList = new TradeRepository().GetTrades().OrderBy(x => x.Name);
			var materialList = new MaterialRepository().GetMaterials().OrderBy(x => x.Name);

			model.locations = new SelectList(locationList, "LocationTypeId", "Name");
            model.trades = new SelectList(tradeList, "TradeId", "Name");
            model.materials = new SelectList(materialList, "MaterialId", "Name");

            return View(model);
        }

        [HttpPost]
        public IActionResult Save(AssociationViewModel model)
        {
            new LocationTypeRepository().SaveAssociation(model.association);
            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id)
        {
            new LocationTypeRepository().DeleteAssociation(id);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            AssociationViewModel model = new AssociationViewModel();
            model.editAssociation = new LocationTypeRepository().GetAssociation(id);

            return View(model);
        }

        [HttpPost]
        public IActionResult Update(AssociationViewModel model)
        {
            var association = model.editAssociation;
            new LocationTypeRepository().UpdateAssociation(association.AssociationId, association.DefaultQuantity);

            return RedirectToAction("Index");
        }
    }
}
