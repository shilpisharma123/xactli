﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SpecSheetsCore.Models.AdminViewModels;
using Repository;
using Models;
using Microsoft.AspNetCore.Mvc.Rendering;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SpecSheetsCore.Controllers.Admin
{
    [Authorize]
    [Area("Admin")]
    public class MaterialController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            MaterialViewModel model = new MaterialViewModel();
            model.materials = new MaterialRepository().GetMaterials();

            return View(model);
        }

        //public IActionResult Add()
        //{
        //    MaterialViewModel model = new MaterialViewModel();
        //    model.material = new Material();

        //    return View(model);
        //}

        [HttpPost]
        public IActionResult Add(MaterialViewModel model)
        {
            new MaterialRepository().SaveMaterial(model.material);
            return RedirectToAction("Index");
        }


        //public IActionResult Edit(int Id)
        //{
        //    MaterialViewModel model = new MaterialViewModel();
        //    model.material = new MaterialRepository().GetMaterial(Id);

        //    return View("Add", model);
        //}
    }
}
