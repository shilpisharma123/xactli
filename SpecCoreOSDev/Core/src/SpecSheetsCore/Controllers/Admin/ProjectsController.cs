﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SpecSheetsCore.Models.AdminViewModels;
using Repository;
using SpecSheetsCore.Models.LoggedInViewModels;
using Models;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SpecSheetsCore.Controllers.Admin
{
    [Authorize]
    [Area("Admin")]
    public class ProjectsController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            ProjectsViewModel model = new ProjectsViewModel();
            model.projects = new ProjectRepository().GetProjects();

            return View(model);
        }

        public IActionResult Add()
        {
            ProjectViewModel model = new ProjectViewModel();
            model.project = new ProjectDetails();

            return View(model);
        }

        [HttpPost]
        public IActionResult Add(ProjectViewModel model)
        {
            return View();
        }


        public IActionResult Edit(int Id)
        {
            ProjectViewModel model = new ProjectViewModel();
            model.project = new ProjectRepository().GetProjectDetails(Id);

            return View(model);
        }

        public IActionResult Details(int Id)
        {
            ProjectViewModel model = new ProjectViewModel();
            model.project = new ProjectRepository().GetProjectDetails(Id);

            model.locationTrades = new LocationRepository().GetTradesByProjectId(Id);

            return View("ProjectDetails", model);
        }
    }
}
