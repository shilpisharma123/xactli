using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SpecSheetsCore.Models.AdminViewModels;
using Repository;
using Models;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SpecSheetsCore.Controllers.Admin
{
    [Authorize]
    [Area("Admin")]
    public class TagController : Controller
    {
        public IActionResult Index()
        {
            TagViewModel model = new TagViewModel();
            model.tags = new TagRepository().GetTags();
            return View(model);
        }
        [HttpPost]
        public IActionResult Add(TagViewModel model)
        {
            new TagRepository().SaveTag(model.tag);
            return RedirectToAction("Index");
        }
        
        public IActionResult Delete(int id)
        {
            new TagRepository().DeleteTag(id);
            return RedirectToAction("Index");
        }
        //public IActionResult Edit(int id)
        //{
        //    TagViewModel model = new TagViewModel();
        //    model.editTags = new TagRepository().GetTags(id);

        //    return View(model);
        //}
    }
}