﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SpecSheetsCore.Models.AdminViewModels;
using Repository;
using Models;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SpecSheetsCore.Controllers.Admin
{
    [Authorize]
    [Area("Admin")]
    public class TradeController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            TradeViewModel model = new TradeViewModel();
            model.trades = new TradeRepository().GetTrades();

            return View(model);
        }

        //public IActionResult Add()
        //{
        //    TradeViewModel model = new TradeViewModel();
        //    model.trade = new Trade();

        //    return View(model);
        //}

        [HttpPost]
        public IActionResult Add(TradeViewModel model)
        {
            new TradeRepository().SaveTrade(model.trade);
            return RedirectToAction("Index");
        }


        //public IActionResult Edit(int Id)
        //{
        //    TradeViewModel model = new TradeViewModel();
        //    model.trade = new TradeRepository().GetTrade(Id);

        //    return View("Add", model);
        //}

        public IActionResult Delete(int id)
        {
            new LocationTypeRepository().DeleteLocationType(id);
            return RedirectToAction("Index");
        }
    }
}
