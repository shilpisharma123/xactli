﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SpecSheetsCore.Models.AdminViewModels;
using Repository;
using Models;
using Microsoft.AspNetCore.Mvc.Rendering;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SpecSheetsCore.Controllers.Admin
{
    [Authorize]
    [Area("Admin")]
    public class LocationTypeController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            LocationTypeViewModel model = new LocationTypeViewModel();
            model.locationTemplates = new LocationTypeRepository().GetLocationTypes();

            return View(model);
        }

        [HttpPost]
        public IActionResult Add(LocationTypeViewModel model)
        {
            new LocationTypeRepository().SaveLocation(model.locationType);
            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id)
        {
            new LocationTypeRepository().DeleteLocationType(id);
            return RedirectToAction("Index");
        }
    }
}
