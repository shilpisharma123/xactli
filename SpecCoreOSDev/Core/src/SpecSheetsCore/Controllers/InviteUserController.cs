using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Repository;
using Models;
using SpecSheetsCore.Models.ProjectViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using SpecSheetsCore.Services;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using SpecSheetsCore.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;

namespace SpecSheetsCore.Controllers
{
    [Authorize]
    public class InviteUserController : Controller
	{
		private readonly IHostingEnvironment _env;
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly SignInManager<ApplicationUser> _signInManager;
		private readonly IEmailSender _emailSender;
		private readonly ISmsSender _smsSender;
		private readonly ILogger _logger;

		public InviteUserController(
		UserManager<ApplicationUser> userManager,
		SignInManager<ApplicationUser> signInManager,
		IEmailSender emailSender,
		ISmsSender smsSender,
		ILoggerFactory loggerFactory,
		IHostingEnvironment env)
		{
			_userManager = userManager;
			_signInManager = signInManager;
			_emailSender = emailSender;
			_smsSender = smsSender;
			_logger = loggerFactory.CreateLogger<ProjectsController>();
			_env = env;
		}

		/// <summary>
		/// Get Presentation of Invited Users of Project
		/// </summary>
		/// <param name="id">ProjectId</param>
		/// <returns></returns>
		public IActionResult Index(int ProjectId)
		{
			var project = new ProjectRepository().GetProject(ProjectId);

			var locationTrades = new LocationRepository().GetTradesByProjectId(ProjectId);
			locationTrades.Insert(0, new LocationTrade() { TradeId = 0, TradeName = "ALL" });
			ViewBag.Trades = new SelectList(locationTrades, "TradeId", "TradeName");

			InvitedUserViewModel model = new InvitedUserViewModel();

			model.InvitedUsers = new InviteUserRepository().GetInvitedUsersByProjectId(ProjectId);
			model.ProjectId = ProjectId;
			model.ProjectName = project.ProjectName;

			return View(model);
		}

		public async Task<IActionResult> InviteProjectUser(string UserName, string Email, int ProjectId, string Trades, int PermissionId, string formName)
		{
			var user = await GetCurrentUserAsync();
			if (user == null)
			{
				return View("Error");
			}

			Guid InviteCode = Guid.NewGuid();
			// Invite Sent By
			string emailBody = CreateInviteEmailBody(UserName, InviteCode.ToString());

			await _emailSender.SendEmailAsync(UserName, Email, "Invitation to Xactli from " + user.UserName, emailBody);
			new InviteUserRepository().InviteTradeUser(InviteCode, UserName, Email, ProjectId, Trades, PermissionId);

			if (formName == "ProjectDetails")
			{
				return RedirectToAction("Details", "Projects", new { id = ProjectId });
			}

			return RedirectToAction("Index", "InviteUser", new { ProjectId = ProjectId });
		}

		private string CreateInviteEmailBody(string Username, string InviteCode)
		{
			StringBuilder builder = new StringBuilder();
			string url = String.Format("{0}/Account/Invite?InviteCode={1}", Request.GetRawUrlWithoutQueryString(), InviteCode);

			builder.Append("Please click on following link to accept invite: ");
			builder.Append("<a href='").Append(url).Append("'>Click here</a>");

			return builder.ToString();
		}

		public bool ResendInvite(int InviteId)
		{
			var InvitedUser = new InviteUserRepository().GetInvitedUserById(InviteId);

			_emailSender.SendEmailAsync(InvitedUser.Email, "Please click on following: " + InvitedUser.InviteCode, InvitedUser.InviteCode);

			return true;
		}

		public IActionResult DeleteInvitedUser(int invitedUserId, int projectId)
		{
			var IsDeleted = new InviteUserRepository().DeleteInvitedUser(invitedUserId);
			return RedirectToAction("Index", new { ProjectId = projectId });
		}

		public Task<ApplicationUser> GetCurrentUserAsync()
		{
			return _userManager.GetUserAsync(HttpContext.User);
		}
	}
}