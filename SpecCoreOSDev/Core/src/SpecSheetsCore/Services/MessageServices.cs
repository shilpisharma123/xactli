﻿using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Hosting;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpecSheetsCore.Services
{
	// This class is used by the application to send Email and SMS
	// when you turn on two-factor authentication in ASP.NET Identity.
	// For more details see this link http://go.microsoft.com/fwlink/?LinkID=532713
	public class AuthMessageSender : IEmailSender, ISmsSender
	{
		private IEmailTemplateMaker _templateMaker;

		public AuthMessageSender(IEmailTemplateMaker templateMaker)
		{
			_templateMaker = templateMaker;
		}

		public async Task SendEmailAsync(string userName, string email, string subject, string messageBody)
		{
			try
			{
				var template = _templateMaker.GetDefaultTemplate(messageBody, userName);
				await SendEmailEngineAsync(email, subject, template);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task SendEmailAsync(string email, string subject, string messageBody)
		{
			try
			{
				var template = _templateMaker.GetDefaultTemplate(messageBody);
				await SendEmailEngineAsync(email, subject, template);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		private async Task SendEmailEngineAsync(string email, string subject, string messageBody)
		{
			try
			{
				var message = new MimeMessage();
				message.From.Add(new MailboxAddress("test@centerstrings.com"));
				message.To.Add(new MailboxAddress(email));
				message.Subject = subject;

				message.Body = new TextPart("html") { Text = messageBody };

				using (var client = new SmtpClient())
				{
					// For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
					client.ServerCertificateValidationCallback = (s, c, h, e) => true;

					await client.ConnectAsync("smtpout.secureserver.net", 80, false).ConfigureAwait(false);

					// Note: since we don't have an OAuth2 token, disable
					// the XOAUTH2 authentication mechanism.
					client.AuthenticationMechanisms.Remove("XOAUTH2");

					// Note: only needed if the SMTP server requires authentication
					await client.AuthenticateAsync("test@centerstrings.com", "test11").ConfigureAwait(false);

					await client.SendAsync(message).ConfigureAwait(false);
					await client.DisconnectAsync(true).ConfigureAwait(false);
				}
			}
			catch (Exception ex)
			{
			}
		}

		//public bool SendEmail(string email, string subject, string messageBody)
		//{
		//	try
		//	{
		//		var message = new MimeMessage();
		//		message.From.Add(new MailboxAddress("test@centerstrings.com"));
		//		message.To.Add(new MailboxAddress(email));
		//		message.Subject = subject;

		//		var template = _templateMaker.GetDefaultTemplate(messageBody);

		//		message.Body = new TextPart("html") { Text = template };

		//		using (var client = new SmtpClient())
		//		{
		//			// For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
		//			client.ServerCertificateValidationCallback = (s, c, h, e) => true;

		//			client.Connect("smtpout.secureserver.net", 80, false);

		//			// Note: since we don't have an OAuth2 token, disable
		//			// the XOAUTH2 authentication mechanism.
		//			client.AuthenticationMechanisms.Remove("XOAUTH2");

		//			// Note: only needed if the SMTP server requires authentication
		//			client.Authenticate("test@centerstrings.com", "test11");

		//			client.SendAsync(message);
		//			client.Disconnect(true);
		//		}

		//		return true;
		//	}
		//	catch (Exception)
		//	{
		//		return false;
		//	}
		//}


		public Task SendSmsAsync(string number, string message)
		{
			// Plug in your SMS service here to send a text message.
			return Task.FromResult(0);
		}
	}
}
