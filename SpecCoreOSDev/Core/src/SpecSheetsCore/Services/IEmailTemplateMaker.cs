﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpecSheetsCore.Services
{
    public interface IEmailTemplateMaker
    {
		string GetDefaultTemplate(string body, string username = "");

	}
}
