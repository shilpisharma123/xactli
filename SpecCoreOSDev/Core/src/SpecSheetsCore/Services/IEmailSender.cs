﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpecSheetsCore.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);

		Task SendEmailAsync(string userName, string email, string subject, string message);
	}
}
