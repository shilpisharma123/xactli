﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpecSheetsCore.Services
{
	public class EmailTemplateMaker : IEmailTemplateMaker
	{
		private IHostingEnvironment _env;
		private IHttpContextAccessor _httpContextAccessor;

		public EmailTemplateMaker(IHostingEnvironment env, IHttpContextAccessor httpContextAccessor)
		{
			_env = env;
			_httpContextAccessor = httpContextAccessor;
		}


		public string GetDefaultTemplate(string body, string username = "")
		{
			try
			{
				string path = $"{_env.ContentRootPath}/Views/_EmailTemplates/_default.html";
				string contentPath = $"{_httpContextAccessor.HttpContext.Request.Scheme}://{_httpContextAccessor.HttpContext.Request.Host}";

				string template = System.IO.File.ReadAllText(path);
				template = template.Replace("{{Body}}", body);

				template = template.Replace("{{User}}", username != string.Empty ? $"Hi {username}," : string.Empty);
				template = template.Replace("{{WebRootPath}}", contentPath);

				return template;
			}
			catch (Exception)
			{
				throw;
			}
		}
	}


}
