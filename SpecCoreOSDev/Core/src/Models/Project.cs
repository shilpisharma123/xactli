﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;

namespace Models
{
    public class Project
    {
        public int ProjectId { get; set; }

        public string ProjectName { get; set; }

		public string OwnerName { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }

        public string Phone { get; set; }

        public bool IsOwner { get; set; }

		public string Email { get; set; }

        public string AspNetUsersId { get; set; }

		public int InvitedProjectId { get; set; }

		public int ProjectLocationTradeId { get; set; }

		public int PermissionLevel { get; set; }
	}
}
