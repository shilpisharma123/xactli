﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    //KS Added ProjectTags Model - 6/12/2018
    public class ProjectTags
    {
        public int TagId { get; set; }
        public int ProjectId { get; set; }//added project id 11/12/2018
        public int LocationId { get; set; }
        public int TradeId { get; set; }
        public int MaterialId { get; set; }

        public string LocationName { get; set; }
        public string TradeName { get; set; }
        public string MaterialName { get; set; }

        public int ProjectLocationTradeMaterialImageId { get; set; }

        public bool Status { get; set; }

        public DateTime DateTimeStamp { get; set; }
        public string AspNetUsersId { get; set; }
        public string UserName { get; set; }
        public string DateString { get; set; }
    }
}
