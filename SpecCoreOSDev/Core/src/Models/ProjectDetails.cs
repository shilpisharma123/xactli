﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Models
{
    public class ProjectDetails : Project
    {
        public ProjectDetails()
        {
            Locations = new List<Location>();
			Trades = new List<LocationTrade>();
        }

        public List<Location> Locations { get; set; }

		public List<LocationTrade> Trades { get; set; }
    }
}
