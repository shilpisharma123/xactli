﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Models
{
    public class InvitedUser
    {
		public int InvitedUserId { get; set; }

		public int ProjectId { get; set; }

		public string ProjectName { get; set; }

		public int ProjectLocationTradeId { get; set; }

		public string TradeName { get; set; }

		public int InviteStatusId { get; set; }

		public int ContactId { get; set; }

		public string ContactName { get; set; }

		public string Email { get; set; }

		public int PermissionLevelId { get; set; }

		public string AspNetUsersId { get; set; }

		public int PackageType { get; set; }

		public string PermissionLevel
		{
			get
			{
				switch (PermissionLevelId)
				{
					case 1:
						return "Read-only";

					case 2:
						return "Update";

					case 3:
						return "Update Trade Only";
				}

				return string.Empty;
			}
		}

		public string InviteStatus
		{
			get
			{
				switch (InviteStatusId)
				{
					case 1:
						return "Pending";

					case 2:
						return "Accepted";
				}

				return string.Empty;
			}
		}

		public DateTime DateInvited { get; set; }

		public DateTime DateRecieved { get; set; }

		public string InviteCode { get; set; }
	}

	public enum Packages{
		HomeOwner = 1,
		Pro = 2,
		concierge = 3
	}
}
