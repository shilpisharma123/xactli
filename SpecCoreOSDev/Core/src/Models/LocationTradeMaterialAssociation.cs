﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Models
{
    public class LocationTradeMaterialAssociation
    {
        public int AssociationId { get; set; }
        public int LocationId { get; set; }

        public int TradeId { get; set; }

        public int MaterialId { get; set; }

        public int DefaultQuantity { get; set; }
    }
}
