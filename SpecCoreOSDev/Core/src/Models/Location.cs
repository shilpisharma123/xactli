﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Models
{
    public class Location
    {
        public Location()
        {
            Trades = new List<LocationTrade>();
			Materials = new List<TradeMaterial>();
        }

        public int LocationId { get; set; }
        public string LocationName { get; set; }

        public int TypeId { get; set; }

        public LocationType Type { get; set; }

        public List<LocationTrade> Trades { get; set; }

		public List<TradeMaterial> Materials { get; set; }

		public int ProjectId { get; set; }

		public int InvitedProjectId { get; set; }

		public int ProjectLocationTradeId { get; set; }

		public int PermissionLevel { get; set; }

		public bool IsOwner { get; set; }
	}

    public class LocationTrade
    {
        public LocationTrade()
        {
            Materials = new List<TradeMaterial>();
			Locations = new List<Location>();

		}

        public int TradeId { get; set; }
        public int LocationTypeId { get; set; }
        public string TradeName { get; set; }

		public int PermissionLevel { get; set; }

		public List<TradeMaterial> Materials { get; set; }

        public List<Location> Locations { get; set; }
    }

    public class TradeMaterial
    {
        public int MaterialId { get; set; }

        public string MaterialName { get; set; }

		public string Model { get; set; }

		public double LowBudget { get; set; }

        public double HighBudget { get; set; }

        public double Actual { get; set; }

        public double Estimate { get; set; }

		public string Manufacturer { get; set; }

		public double Cost { get; set; }

		public int DefaultQuantity { get; set; }

        public string UnitOfMeasure { get; set; }

        public int LocationTradeId { get; set; }

		public string Description { get; set; }

		public int ProjectLocationTradeMaterialId { get; set; }

		public string Finish { get; set; }

		public string LeadTime { get; set; }

		public string PlanReference { get; set; }

        public List<ProjectTradeMaterialImage> ProjectTradeMaterialImages { get; set; }

    }

    public class LocationAssociationTrade
    {
        public int Id { get; set; }
        public int LocationId { get; set; }
        public int LocationTradeId { get; set; }
    }
}
