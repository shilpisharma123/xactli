﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Models
{
   public class Tag
    {
        public int TagId { get; set; }
        public string TagName { get; set; }
        public int LocationId { get; set; }
        public int TradeId { get; set; }
        public int MaterialId { get; set; }

        public string LocationName { get; set; }
        public string TradeName { get; set; }
        public string MaterialName { get; set; }

        public bool Status { get; set; }
    }
}
