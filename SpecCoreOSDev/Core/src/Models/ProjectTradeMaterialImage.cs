﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Models
{
    //Modified by :Jeet Bhandari
    //Modified date:02-Nov-2018
    public class ProjectTradeMaterialImage
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int TradeId { get; set; }
        public int MaterialId { get; set; }
        public byte[] Image { get; set; }
        public string Notes { get; set; }
        public DateTime DateTimeStamp { get; set; }
        public string AspNetUsersId { get; set; }
        public string UserName { get; set; }//we want to display  the user who has entered the notes
        public string DateString { get; set; }
        public int ImageOrderSequence { get; set; }
        public bool FinalSelection { get; set; }//add final selection -Kadambini 5/12/2018
        public bool IsFinal { get; set; }//if any image has been finalised for the material -Kadambini 5/12/2018
        public string LocationName { get; set; }//get loc -Kadambini 5/12/2018
        public string TradeName { get; set; }//get trade -Kadambini 5/12/2018
        public string MaterialName { get; set; }//get material-Kadambini 5/12/2018

        public int LocationId { get; set; }
    }
}
