﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class ProjectLocationTradeMaterialDetails
    {
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public int TradeId { get; set; }
        public string TradeName { get; set; }
        public int MaterialId { get; set; }
        public string MaterialName { get; set; }

    }
}
