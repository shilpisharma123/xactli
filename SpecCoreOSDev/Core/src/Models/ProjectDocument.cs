using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class ProjectDocument
	{
		public int ProjectDocumentId { get; set; }
		public int ProjectId { get; set; }
		public int TradeId { get; set; }
		public string ProjectDocumentType { get; set; }
		public DateTime DateTimeStamp { get; set; }
		public string AspNetUsersId { get; set; }
		public byte[] Document { get; set; }
		public string FileType { get; set; }
		public string DocumentName { get; set; }

		public string DocumentDetailsString { get; set; }		



	}
}
