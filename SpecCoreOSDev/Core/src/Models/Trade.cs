﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Models
{
    public class Trade
    {
        public int TradeId { get; set; }
        public string Name { get; set; }
    }
}
