﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Models
{
    public class AppConfiguration
    {
        public static string ConnectionString { get; set; }

		public static SmtpSettings SMTPSettings { get; set; }
    }

	public class SmtpSettings
	{
		public string MailFrom { get; set; }

		public string MailServer { get; set; }

		public string MailServerPort { get; set; }

		public string MailUser { get; set; }

		public string MailPassword { get; set; }
	}
}
