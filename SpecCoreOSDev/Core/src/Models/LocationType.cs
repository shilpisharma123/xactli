﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Models
{
    public class LocationType
    {
		public LocationType() { }

		public LocationType(int LocationTypeId, string LocationTypeName, string LocationTypeCategory) {
			this.LocationTypeId = LocationTypeId;
			this.Name = LocationTypeName;
			this.Category = LocationTypeCategory;
		}

		public int LocationTypeId { get; set; }
        public string Name { get; set; }

        public string Category { get; set; }
    }
}
