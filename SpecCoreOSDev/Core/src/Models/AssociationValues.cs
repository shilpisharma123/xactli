﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Models
{
    public class AssociationValues
    {
        public int AssociationId { get; set; }

        public int LocationId { get; set; }
        public int TradeId { get; set; }
        public int MaterialId { get; set; }


        public string LocationName { get; set; }
        public string TradeName { get; set; }
        public string MaterialName { get; set; }

        public int DefaultQuantity { get; set; }
    }
}
