﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Models
{
    public class Material
    {
        public int ProjectLocationTradeMaterialId { get; set; }
        public int MaterialId { get; set; }
        public string Name { get; set; }

		public string Model { get; set; }

		public int Quantity { get; set; }
        public decimal Estimate { get; set; }
        public decimal Actual { get; set; }
        public decimal LowBudget { get; set; }
        public decimal HighBudget { get; set; }
        public string UnitMeasure { get; set; }
        public string Manufacturer { get; set;}
        public string Finish { get; set; }
        public decimal Cost { get; set; }
        public string LeadTime { get; set; }
        public string PlanReference { get; set; }
        public string Description { get; set; }
        public string HaveNeedBy { get; set; }
		public bool IsNew { get; set; }
        public List<ProjectTradeMaterialImage> Images { get; set; }
    }
}
