﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class NoteHistory
    {
        public int NoteId { get; set; }
        public string ProjectLocationTradeMaterialImagesId { get; set; }
        public string NoteName { get; set; }
        public string Notes { get; set; }
        public DateTime DateTimeStamp { get; set; }
        public string AspNetUsersId { get; set; }
        public string UserName { get; set; }//we want to display  the user who has entered the notes
        public string DateString { get; set; }
        public int ProjectDocumentId { get; set; }
    }
}
