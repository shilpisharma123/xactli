﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Models
{
    public class AccountInformation
    {
		public AccountInformation() { }

		public int ContactId { get; set; }

		public string Name { get; set; }

		public string CompanyName { get; set; }

		public string Address { get; set; }

		public string Notes { get; set; }

		public string City { get; set; }

		public string State { get; set; }

		public string Zip { get; set; }

		public string Phone { get; set; }

		public string AspNetUsersId { get; set; }

		public string Email { get; set; }
    }
}
