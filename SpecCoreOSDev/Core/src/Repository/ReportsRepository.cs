﻿using Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;

namespace Repository
{
    public class ReportsRepository
    {
        private string ConnectionString = AppConfiguration.ConnectionString;
		
		public ProjectDetails ReportTradesByLocations(int ProjectId, int LocationId = 0)
        {
            try
            {
                ProjectDetails project = new ProjectDetails();

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter("ProjectId", ProjectId);

                    using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Project-GetProjectById", param))
                    {
                        while (reader.Read())
                        {
                            project.ProjectId = Common.CheckIntegerNull(reader["ProjectId"]);
                            project.ProjectName = Common.CheckStringNull(reader["ProjectName"]);
							project.OwnerName = Common.CheckStringNull(reader["OwnerName"]);
							project.Address = Common.CheckStringNull(reader["Address"]);
                            project.City = Common.CheckStringNull(reader["City"]);
                            project.Email = Common.CheckStringNull(reader["Email"]);
                            project.Phone = Common.CheckStringNull(reader["Phone"]);
                            project.State = Common.CheckStringNull(reader["State"]);
                            project.Zip = Common.CheckStringNull(reader["Zip"]);
                        }
                    }
                }

                //project.Locations = new LocationRepository().GetLocationWithTradesAndMaterials(project.ProjectId);

				try
				{
					List<Location> locations = new List<Location>();

					using (SqlConnection connection = new SqlConnection(ConnectionString))
					{
						SqlParameter[] param = new SqlParameter[2];
						param[0] = new SqlParameter("ProjectId", ProjectId);
						param[1] = new SqlParameter("LocationId", LocationId);

						using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Reports-TradesByLocations", param))
						{
							Location location = new Location();
							while (reader.Read())
							{
								LocationId = Common.CheckIntegerNull(reader["ProjectLocationId"]);

								if (!locations.Any(x => x.LocationId == LocationId))
								{
									location = new Location();

									location.LocationId = Common.CheckIntegerNull(reader["ProjectLocationId"]);
									location.TypeId = Common.CheckIntegerNull(reader["LocationTypeID"]);
									location.LocationName = Common.CheckStringNull(reader["ProjectLocationName"]);

									location.Type = new LocationType()
									{
										LocationTypeId = location.TypeId,
										Name = Common.CheckStringNull(reader["LocationTypeName"])
									};

									locations.Add(location);
								}

								int TradeId = Common.CheckIntegerNull(reader["TradeId"]);
								int MaterialId = Common.CheckIntegerNull(reader["MaterialId"]);

								if (!location.Trades.Any(x => x.TradeId == TradeId))
								{
									location.Trades.Add(new LocationTrade()
									{
										TradeId = TradeId,
										TradeName = Common.CheckStringNull(reader["TradeName"])
									});

									location.Trades = location.Trades.OrderBy(x => x.TradeName).ToList();
								}

								var trade = location.Trades.Single(x => x.TradeId == TradeId);
								if (!trade.Materials.Any(x => x.MaterialId == MaterialId))
								{
									trade.Materials.Add(new TradeMaterial()
									{
										MaterialId = MaterialId,
										MaterialName = Common.CheckStringNull(reader["MaterialName"]),
										DefaultQuantity = Common.CheckIntegerNull(reader["Quantity"]),
										Estimate = Common.CheckDoubleNull(reader["Estimate"]),
										Cost = Common.CheckDoubleNull(reader["Cost"]),
										Manufacturer = Common.CheckStringNull(reader["Manufacturer"]),
										Model = Common.CheckStringNull(reader["Model"]),
										Description = Common.CheckStringNull(reader["Description"])
									});

									trade.Materials = trade.Materials.OrderBy(x => x.MaterialName).ToList();
								}
							}
						}
					}

					project.Locations = locations.OrderBy(x=> x.LocationName).OrderBy(x=> x.LocationName).ToList();
				}
				catch (Exception ex)
				{
					throw ex;
				}
				
				return project;
            }
            catch (Exception ex)
            {
                throw new Exception("Problem Getting ProjectDetails", ex);
            }
        }

		public object ReportLocationsByTrades(int ProjectId, string TradeName)
		{
			try
			{
				ProjectDetails project = new ProjectDetails();

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					SqlParameter[] param = new SqlParameter[1];
					param[0] = new SqlParameter("ProjectId", ProjectId);

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Project-GetProjectById", param))
					{
						while (reader.Read())
						{
							project.ProjectId = Common.CheckIntegerNull(reader["ProjectId"]);
							project.ProjectName = Common.CheckStringNull(reader["ProjectName"]);
							project.OwnerName = Common.CheckStringNull(reader["OwnerName"]);
							project.Address = Common.CheckStringNull(reader["Address"]);
							project.City = Common.CheckStringNull(reader["City"]);
							project.Email = Common.CheckStringNull(reader["Email"]);
							project.Phone = Common.CheckStringNull(reader["Phone"]);
							project.State = Common.CheckStringNull(reader["State"]);
							project.Zip = Common.CheckStringNull(reader["Zip"]);
						}
					}
				}

				//project.Locations = new LocationRepository().GetLocationWithTradesAndMaterials(project.ProjectId);

				try
				{
					List<LocationTrade> trades = new List<LocationTrade>();

					using (SqlConnection connection = new SqlConnection(ConnectionString))
					{
						SqlParameter[] param = new SqlParameter[2];
						param[0] = new SqlParameter("ProjectId", ProjectId);
						param[1] = new SqlParameter("TradeName", TradeName);

						using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Reports-LocationsByTradeName", param))
						{
							LocationTrade trade = null;
							Location location = null;

							while (reader.Read())
							{
								int tradeId = Common.CheckIntegerNull(reader["TradeId"]);
								string tradeName = Common.CheckStringNull(reader["TradeName"]);
								int MaterialId = Common.CheckIntegerNull(reader["MaterialId"]);
								int LocationId = Common.CheckIntegerNull(reader["ProjectLocationId"]);

								if (!trades.Any(x => x.TradeId == tradeId && x.TradeName == tradeName))
								{
									trade = new LocationTrade();
									trade.TradeId = tradeId;
									trade.TradeName = tradeName;

									trades.Add(trade);
								}
								else
								{
									trade = trades.Single(x => x.TradeId == tradeId && x.TradeName == tradeName);
								}

								if (!trade.Locations.Any(x => x.LocationId == LocationId))
								{
									location = new Location();

									location.LocationId = Common.CheckIntegerNull(reader["ProjectLocationId"]);
									location.TypeId = Common.CheckIntegerNull(reader["LocationTypeID"]);
									location.LocationName = Common.CheckStringNull(reader["ProjectLocationName"]);

									location.Type = new LocationType()
									{
										LocationTypeId = location.TypeId,
										Name = Common.CheckStringNull(reader["LocationTypeName"])
									};

									trade.Locations.Add(location);
									trade.Locations = trade.Locations.OrderBy(x => x.LocationName).ToList();
								}
								else
								{
									location = trade.Locations.Single(x => x.LocationId == LocationId);
								}

								if (!location.Materials.Any(x => x.MaterialId == MaterialId))
								{
									location.Materials.Add(new TradeMaterial()
									{
										MaterialId = MaterialId,
										MaterialName = Common.CheckStringNull(reader["MaterialName"]),
										DefaultQuantity = Common.CheckIntegerNull(reader["Quantity"]),
										Estimate = Common.CheckDoubleNull(reader["Estimate"]),
										Cost = Common.CheckDoubleNull(reader["Cost"]),
										Manufacturer = Common.CheckStringNull(reader["Manufacturer"]),
										Model = Common.CheckStringNull(reader["Model"]),
										Description = Common.CheckStringNull(reader["Description"])
									});

									location.Materials = location.Materials.OrderBy(x => x.MaterialName).ToList();
								}
							}
						}
					}

					project.Trades = trades.OrderBy(x => x.TradeName).ToList();
				}
				catch (Exception ex)
				{
					throw ex;
				}

				return project;
			}
			catch (Exception ex)
			{
				throw new Exception("Problem in ReportLocationsByTrades", ex);
			}
		}

		public ProjectDetails ReportLocationsByTrades(int ProjectId, int TradeId = 0)
		{
			try
			{
				ProjectDetails project = new ProjectDetails();

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					SqlParameter[] param = new SqlParameter[1];
					param[0] = new SqlParameter("ProjectId", ProjectId);

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Project-GetProjectById", param))
					{
						while (reader.Read())
						{
							project.ProjectId = Common.CheckIntegerNull(reader["ProjectId"]);
							project.ProjectName = Common.CheckStringNull(reader["ProjectName"]);
							project.OwnerName = Common.CheckStringNull(reader["OwnerName"]);
							project.Address = Common.CheckStringNull(reader["Address"]);
							project.City = Common.CheckStringNull(reader["City"]);
							project.Email = Common.CheckStringNull(reader["Email"]);
							project.Phone = Common.CheckStringNull(reader["Phone"]);
							project.State = Common.CheckStringNull(reader["State"]);
							project.Zip = Common.CheckStringNull(reader["Zip"]);
						}
					}
				}

				//project.Locations = new LocationRepository().GetLocationWithTradesAndMaterials(project.ProjectId);

				try
				{
					List<LocationTrade> trades = new List<LocationTrade>();

					using (SqlConnection connection = new SqlConnection(ConnectionString))
					{
						SqlParameter[] param = new SqlParameter[3];
						param[0] = new SqlParameter("ProjectId", ProjectId);
						param[1] = new SqlParameter("TradeId", TradeId);

						using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Reports-LocationsByTrades", param))
						{
							LocationTrade trade = null;
							Location location = null;

							while (reader.Read())
							{
								int tradeId = Common.CheckIntegerNull(reader["TradeId"]);
								string tradeName = Common.CheckStringNull(reader["TradeName"]);
								int MaterialId = Common.CheckIntegerNull(reader["MaterialId"]);
								int LocationId = Common.CheckIntegerNull(reader["ProjectLocationId"]);

								if (!trades.Any(x => x.TradeId == tradeId && x.TradeName == tradeName))
								{
									trade = new LocationTrade();
									trade.TradeId = tradeId;
									trade.TradeName = tradeName;

									trades.Add(trade);
								}
								else
								{
									trade = trades.Single(x => x.TradeId == tradeId && x.TradeName == tradeName);
								}

								if (!trade.Locations.Any(x => x.LocationId == LocationId))
								{
									location = new Location();

									location.LocationId = Common.CheckIntegerNull(reader["ProjectLocationId"]);
									location.TypeId = Common.CheckIntegerNull(reader["LocationTypeID"]);
									location.LocationName = Common.CheckStringNull(reader["ProjectLocationName"]);

									location.Type = new LocationType()
									{
										LocationTypeId = location.TypeId,
										Name = Common.CheckStringNull(reader["LocationTypeName"])
									};

									trade.Locations.Add(location);
									trade.Locations = trade.Locations.OrderBy(x => x.LocationName).ToList();
								}
								else
								{
									location = trade.Locations.Single(x => x.LocationId == LocationId);
								}

								if (!location.Materials.Any(x => x.MaterialId == MaterialId))
								{
									location.Materials.Add(new TradeMaterial()
									{
										MaterialId = MaterialId,
										MaterialName = Common.CheckStringNull(reader["MaterialName"]),
										DefaultQuantity = Common.CheckIntegerNull(reader["Quantity"]),
										Estimate = Common.CheckDoubleNull(reader["Estimate"]),
										Cost = Common.CheckDoubleNull(reader["Cost"]),
										Manufacturer = Common.CheckStringNull(reader["Manufacturer"]),
										Model = Common.CheckStringNull(reader["Model"]),
										Description = Common.CheckStringNull(reader["Description"])
									});

									location.Materials = location.Materials.OrderBy(x => x.MaterialName).ToList();
								}
							}
						}
					}

					project.Trades = trades.OrderBy(x=> x.TradeName).ToList();
				}
				catch (Exception ex)
				{
					throw ex;
				}

				return project;
			}
			catch (Exception ex)
			{
				throw new Exception("Problem Getting ProjectDetails", ex);
			}
		}

		public List<object> ReportGetNormalizedProject(int ProjectId)
		{
			try
			{
				List<object> product = new List<object>();

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();
					param.Add(new SqlParameter("ProjectId", ProjectId));

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Reports-GetProjectDeNormalized", param))
					{
						int fieldsCount = reader.FieldCount;

						while (reader.Read())
						{
							ExpandoObject expando = new ExpandoObject();
							var expandoDict = expando as IDictionary<string, object>;

							for (int i = 0; i < fieldsCount; i++)
							{
								expandoDict[reader.GetName(i).ToString()] = reader[i];
							}

							product.Add(expandoDict);
						}
					}
				}

				return product;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
	}
}
