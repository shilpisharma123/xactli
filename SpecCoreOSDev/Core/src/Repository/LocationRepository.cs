﻿using Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Repository
{
	public class LocationRepository
	{
		private string ConnectionString = AppConfiguration.ConnectionString;

		public List<Location> GetLocations(int ProjectId)
		{
			try
			{
				var locationTypes = GetLocationTypes();
				List<Location> locations = new List<Location>();

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					SqlParameter[] param = new SqlParameter[1];
					param[0] = new SqlParameter("ProjectId", ProjectId);

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Location-GetLocationsByProjectId", param.ToArray()))
					{
						Location location = null;
						while (reader.Read())
						{
							location = new Location();
							location.LocationId = Common.CheckIntegerNull(reader["ProjectLocationId"]);
							location.TypeId = Common.CheckIntegerNull(reader["LocationTypeID"]);
							location.LocationName = Common.CheckStringNull(reader["Name"]);
							location.ProjectId = Common.CheckIntegerNull(reader["ProjectId"]);

							location.Type = locationTypes.SingleOrDefault(x => x.LocationTypeId == location.TypeId);

							locations.Add(location);
						}
					}
				}

				return locations;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public List<LocationTrade> GetTradesByProjectId(int ProjectId)
		{
			try
			{
				List<LocationTrade> trades = new List<LocationTrade>();

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					SqlParameter[] param = new SqlParameter[1];
					param[0] = new SqlParameter("ProjectId", ProjectId);

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "LocationTrade-GetTradesByProjectId", param))
					{
						LocationTrade trade = null;
						while (reader.Read())
						{
							trade = new LocationTrade();

							trade.TradeId = Common.CheckIntegerNull(reader["TradeId"]);
							trade.TradeName = Common.CheckStringNull(reader["TradeName"]);

							trades.Add(trade);
						}
					}
				}

				return trades;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

        public List<Location> GetLocationByProjectId(int LocationId, string userId)
        {
            try
            {
                List<Location> locations = new List<Location>();

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlParameter[] param = new SqlParameter[2];
                    param[0] = new SqlParameter("LocationId", LocationId);
                    param[1] = new SqlParameter("AspNetUsersId", userId);
                    using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Location-GetLocationByProjectId", param))
                    {
                        Location location = null;
                        while (reader.Read())
                        {
                            location = new Location();

                            location.LocationId = Common.CheckIntegerNull(reader["ProjectLocationId"]);
                            location.LocationName = Common.CheckStringNull(reader["ProjectLocationName"]);

                            locations.Add(location);
                        }
                    }
                }

                return locations;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Location GetLocation(int LocationId, string AspNetUsersId)
		{
			try
			{
				Location location = null;

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();
					param.Add(new SqlParameter("LocationId", LocationId));
					param.Add(new SqlParameter("AspNetUsersId", AspNetUsersId));

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Location-GetLocationbyId", param))
					{
						location = new Location();

						while (reader.Read())
						{
							if (location.LocationId == 0)
							{
								location.LocationId = Common.CheckIntegerNull(reader["ProjectLocationId"]);
								location.TypeId = Common.CheckIntegerNull(reader["LocationTypeID"]);
								location.LocationName = Common.CheckStringNull(reader["ProjectLocationName"]);
								location.ProjectId = Common.CheckIntegerNull(reader["ProjectId"]);

								location.InvitedProjectId = Common.CheckIntegerNull(reader["InvitedProjectId"]);
								location.ProjectLocationTradeId = Common.CheckIntegerNull(reader["ProjectLocationTradeId"]);
								location.PermissionLevel = Common.CheckIntegerNull(reader["PermissionLevel"]);

								location.IsOwner = Common.CheckBooleanNull(reader["IsOwner"]);

								location.Type = new LocationType()
								{
									LocationTypeId = location.TypeId,
									Name = Common.CheckStringNull(reader["LocationTypeName"])
								};
							}

							int TradeId = Common.CheckIntegerNull(reader["TradeId"]);

							if (TradeId > 0)
							{
								if (!location.Trades.Any(x => x.TradeId == TradeId))
								{
									location.Trades.Add(new LocationTrade()
									{
										TradeId = TradeId,
										TradeName = Common.CheckStringNull(reader["TradeName"]),
										PermissionLevel = Common.CheckIntegerNull(reader["PermissionLevel"]),
									});
								}

								int MaterialId = Common.CheckIntegerNull(reader["MaterialId"]);
								if (MaterialId > 0)
								{
									var trade = location.Trades.Single(x => x.TradeId == TradeId);
									if (!trade.Materials.Any(x => x.MaterialId == MaterialId))
									{
										trade.Materials.Add(new TradeMaterial()
										{
											MaterialId = MaterialId,
											MaterialName = Common.CheckStringNull(reader["MaterialName"]),
											DefaultQuantity = Common.CheckIntegerNull(reader["Quantity"]),
											Estimate = Common.CheckDoubleNull(reader["Estimate"]),
											Cost = Common.CheckDoubleNull(reader["Cost"]),
											Manufacturer = Common.CheckStringNull(reader["Manufacturer"]),
											Model = Common.CheckStringNull(reader["Model"]),
											Finish = Common.CheckStringNull(reader["Finish"]),

											LeadTime = Common.CheckStringNull(reader["LeadTime"]),
											PlanReference = Common.CheckStringNull(reader["PlanReference"]),
											Description = Common.CheckStringNull(reader["Description"]),
											ProjectLocationTradeMaterialId = Common.CheckIntegerNull(reader["ProjectLocationTradeMaterialId"])
										});

										trade.Materials = trade.Materials.OrderBy(x => x.MaterialName).ToList();
									}
								}
							}
						}

						location.Trades = location.Trades.OrderBy(x => x.TradeName).ToList();
					}
				}

				return location;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public Location GetProjectLocationWithTrades(int ProjectLocationId, int TradeId)
		{
			try
			{
				Location location = new Location();

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					SqlParameter[] param = new SqlParameter[2];
					param[0] = new SqlParameter("ProjectLocationId", ProjectLocationId);
					param[1] = new SqlParameter("TradeId", TradeId);

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "LocationTrade-GetProjectLocationWithTrades", param))
					{
						while (reader.Read())
						{
							if (location.TypeId == 0)
							{
								location.LocationName = Common.CheckStringNull(reader["LocationName"]);
								location.TypeId = Common.CheckIntegerNull(reader["LocationTypeID"]);

								location.Type = new LocationType()
								{
									LocationTypeId = location.TypeId,
									Name = Common.CheckStringNull(reader["LocationTypeName"])
								};

								location.Trades.Add(new LocationTrade()
								{
									TradeId = TradeId,
									TradeName = Common.CheckStringNull(reader["TradeName"])
								});
							}

							int MaterialId = Common.CheckIntegerNull(reader["MaterialId"]);
							if (MaterialId > 0)
							{
								var trade = location.Trades.Single(x => x.TradeId == TradeId);
								if (!trade.Materials.Any(x => x.MaterialId == MaterialId))
								{
									trade.Materials.Add(new TradeMaterial()
									{
										MaterialId = MaterialId,
										MaterialName = Common.CheckStringNull(reader["MaterialName"])
									});
								}
							}
						}
					}
				}

				return location;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}




		public List<Location> GetLocationWithTradesAndMaterials(int ProjectId)
		{
			try
			{
				List<Location> locations = new List<Location>();

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					SqlParameter[] param = new SqlParameter[1];
					param[0] = new SqlParameter("ProjectId", ProjectId);

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Location-GetLocationWithTradesAndMaterialsbyProjectId", param))
					{
						Location location = new Location();
						while (reader.Read())
						{
							int LocationId = Common.CheckIntegerNull(reader["ProjectLocationId"]);

							if (!locations.Any(x => x.LocationId == LocationId))
							{
								location = new Location();

								location.LocationId = Common.CheckIntegerNull(reader["ProjectLocationId"]);
								location.TypeId = Common.CheckIntegerNull(reader["LocationTypeID"]);
								location.LocationName = Common.CheckStringNull(reader["ProjectLocationName"]);

								location.Type = new LocationType()
								{
									LocationTypeId = location.TypeId,
									Name = Common.CheckStringNull(reader["LocationTypeName"])
								};

								locations.Add(location);
							}

							int TradeId = Common.CheckIntegerNull(reader["TradeId"]);
							int MaterialId = Common.CheckIntegerNull(reader["MaterialId"]);

							if (!location.Trades.Any(x => x.TradeId == TradeId))
							{
								location.Trades.Add(new LocationTrade()
								{
									TradeId = TradeId,
									TradeName = Common.CheckStringNull(reader["TradeName"])
								});
							}

							var trade = location.Trades.Single(x => x.TradeId == TradeId);
							if (!trade.Materials.Any(x => x.MaterialId == MaterialId))
							{
								trade.Materials.Add(new TradeMaterial()
								{
									MaterialId = MaterialId,
									MaterialName = Common.CheckStringNull(reader["MaterialName"]),
									DefaultQuantity = Common.CheckIntegerNull(reader["Quantity"]),
									Estimate = Common.CheckDoubleNull(reader["Estimate"]),
									Cost = Common.CheckDoubleNull(reader["Cost"]),
									Manufacturer = Common.CheckStringNull(reader["Manufacturer"]),
									Model = Common.CheckStringNull(reader["Model"]),
								    Finish = Common.CheckStringNull(reader["Finish"]),
								
								    LeadTime = Common.CheckStringNull(reader["LeadTime"]),
								    PlanReference = Common.CheckStringNull(reader["PlanReference"]),
								    Description = Common.CheckStringNull(reader["Description"])
								    //HaveNeedBy = Common.CheckStringNull(reader["HaveNeedBy"]),
							});
							}
						}
					}
				}

				return locations;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public int AddLocation(Location model)
		{
			try
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();

					param.Add(new SqlParameter("ProjectLocationId", model.LocationId));
					param.Add(new SqlParameter("Name", model.LocationName));
					param.Add(new SqlParameter("LocationTypeId", model.TypeId));
					param.Add(new SqlParameter("ProjectId", model.ProjectId));

					object locationId = SqlHelper.ExecuteScalar(connection, System.Data.CommandType.StoredProcedure, "Location-SaveLocation", param.ToArray());

					return Common.CheckIntegerNull(locationId);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public List<LocationType> GetLocationTypes()
		{
			try
			{
				List<LocationType> locationTypes = new List<LocationType>();

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Location-GetLocationTypes", null))
					{
						LocationType locationType = null;
						while (reader.Read())
						{
							locationType = new LocationType();

							locationType.LocationTypeId = Common.CheckIntegerNull(reader["LocationTypeId"]);
							locationType.Name = Common.CheckStringNull(reader["Name"]);
							locationType.Category = Common.CheckStringNull(reader["Category"]);

							locationTypes.Add(locationType);
						}
					}
				}

				return locationTypes;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public List<LocationTrade> GetLocationTypeTrades(int LocationId)
		{
			try
			{
				List<LocationTrade> trades = new List<LocationTrade>();

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					SqlParameter[] param = new SqlParameter[1];
					param[0] = new SqlParameter("LocationId", LocationId);

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "LocationTrade-GetTradesByLocationId", param.ToArray()))
					{
						LocationTrade trade = null;
						while (reader.Read())
						{
							trade = new LocationTrade();

							trade.TradeId = Common.CheckIntegerNull(reader["TradeId"]);
							trade.TradeName = Common.CheckStringNull(reader["TradeName"]);

							trades.Add(trade);
						}
					}
				}

				return trades;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public LocationTrade GetMaterialsBySpecificTradeByLocation(int projectId, int tradeId)
		{
			LocationTrade trade = null;

			//locationTypeTrades.SingleOrDefault(x => x.TradeId == tradeId);
			//trade.Locations = locations.Where(x => x.ProjectId == projectId).ToList();

			//foreach (var location in trade.Locations)
			//{
			//    var locationTrades = GetLocationTypeTrades(location.LocationId);

			//    if (locationTrades.Count() > 0)
			//    {
			//        location.Trades = locationTrades;
			//    }
			//}

			return trade;
		}
        public List<LocationTrade> GetTradesByLocationId(int LocationId)
        {
            try
            {
                List<LocationTrade> trades = new List<LocationTrade>();

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter("ProjectLocationId", LocationId);

                    using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "LocationTrade-GetTradesByProjectLocationIdForTag", param.ToArray()))
                    {
                        LocationTrade trade = null;
                        while (reader.Read())
                        {
                            trade = new LocationTrade();

                            trade.TradeId = Common.CheckIntegerNull(reader["ProjectLocationTradeID"]);
                            trade.TradeName = Common.CheckStringNull(reader["TradeName"]);

                            trades.Add(trade);
                        }
                    }
                }

                return trades;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<TradeMaterial> GetMaterialsByTradeId(int TradeId)
        {
            try
            {
                List<TradeMaterial> materials = new List<TradeMaterial>();

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter("TradeId", TradeId);

                    using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "LocationTrade-GetMaterialsByTradeId", param.ToArray()))
                    {
                        TradeMaterial material = null;
                        while (reader.Read())
                        {
                            material = new TradeMaterial();

                            material.MaterialId = Common.CheckIntegerNull(reader["ProjectLocationTradeMaterialId"]);
                            material.MaterialName = Common.CheckStringNull(reader["MaterialName"]);

                            materials.Add(material);
                        }
                    }
                }

                return materials;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
