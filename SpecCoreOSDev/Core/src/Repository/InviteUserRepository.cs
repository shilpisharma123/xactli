﻿using Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Repository
{
    public class InviteUserRepository
    {
		private string ConnectionString = AppConfiguration.ConnectionString;

		public int InviteTradeUser(Guid InviteCode, string UserName, string Email, int ProjectId, string Trade, int PermissionId)
		{
			try
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();

					param.Add(new SqlParameter("InviteCode", InviteCode));
					param.Add(new SqlParameter("UserName", UserName));
					param.Add(new SqlParameter("Email", Email));
					param.Add(new SqlParameter("ProjectId", ProjectId));
					param.Add(new SqlParameter("TradeId", Trade));
					param.Add(new SqlParameter("PermissionId", PermissionId));

					object tradeId = SqlHelper.ExecuteScalar(connection, System.Data.CommandType.StoredProcedure, "Project-InviteTradeUser", param.ToArray());

					return Common.CheckIntegerNull(tradeId);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public List<InvitedUser> GetInvitedUsersByProjectId(int ProjectId)
		{
			List<InvitedUser> InvitedUsers = new List<InvitedUser>();

			try
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();
					param.Add(new SqlParameter("ProjectId", ProjectId));

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Project-GetInvitedUsersByProjectId", param.ToArray()))
					{
						while (reader.Read())
						{
							InvitedUser user = new InvitedUser();

							user.InvitedUserId = Common.CheckIntegerNull(reader["ProjectInvitedContactId"]);
							user.ProjectId = Common.CheckIntegerNull(reader["ProjectId"]);
							user.ProjectLocationTradeId = Common.CheckIntegerNull(reader["ProjectLocationTradeId"]);
							user.TradeName = Common.CheckStringNull(reader["TradeName"]);
							user.ContactId = Common.CheckIntegerNull(reader["ContactId"]);
							user.ContactName = Common.CheckStringNull(reader["ContactName"]);
							user.Email = Common.CheckStringNull(reader["Email"]);
							user.InviteStatusId = Common.CheckIntegerNull(reader["InviteStatus"]);
							user.PermissionLevelId = Common.CheckIntegerNull(reader["PermissionLevel"]);
							user.DateInvited = Common.CheckDateTimeNull(reader["DateInvited"]);
							user.DateRecieved = Common.CheckDateTimeNull(reader["DateReceived"]);

							InvitedUsers.Add(user);
						}
					}
				}

				return InvitedUsers;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public InvitedUser GetInvitedUserByCode(Guid InviteCode)
		{
			InvitedUser user = null;

			try
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();
					param.Add(new SqlParameter("InviteCode", InviteCode));

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Project-GetInvitedUserByCode", param.ToArray()))
					{
						while (reader.Read())
						{
							user = new InvitedUser();

							user.InvitedUserId = Common.CheckIntegerNull(reader["ProjectInvitedContactId"]);
							user.ProjectId = Common.CheckIntegerNull(reader["ProjectId"]);
							user.ProjectLocationTradeId = Common.CheckIntegerNull(reader["ProjectLocationTradeId"]);
							user.TradeName = Common.CheckStringNull(reader["TradeName"]);
							user.ContactId = Common.CheckIntegerNull(reader["ContactId"]);
							user.ContactName = Common.CheckStringNull(reader["ContactName"]);
							user.Email = Common.CheckStringNull(reader["Email"]);
							user.InviteStatusId = Common.CheckIntegerNull(reader["InviteStatus"]);
							user.PermissionLevelId = Common.CheckIntegerNull(reader["PermissionLevel"]);
							user.DateInvited = Common.CheckDateTimeNull(reader["DateInvited"]);
							user.DateRecieved = Common.CheckDateTimeNull(reader["DateReceived"]);
							user.AspNetUsersId = Common.CheckStringNull(reader["AspNetUsersId"]);
						}
					}
				}

				return user;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public InvitedUser GetInvitedUserById(int InviteId)
		{
			InvitedUser user = new InvitedUser();

			try
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();
					param.Add(new SqlParameter("ProjectInvitedContactId", InviteId));

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Project-GetInvitedUserById", param.ToArray()))
					{
						while (reader.Read())
						{
							user.InvitedUserId = Common.CheckIntegerNull(reader["ProjectInvitedContactId"]);
							user.ProjectId = Common.CheckIntegerNull(reader["ProjectId"]);
							user.ProjectLocationTradeId = Common.CheckIntegerNull(reader["ProjectLocationTradeId"]);
							user.TradeName = Common.CheckStringNull(reader["TradeName"]);
							user.ContactId = Common.CheckIntegerNull(reader["ContactId"]);
							user.ContactName = Common.CheckStringNull(reader["ContactName"]);
							user.Email = Common.CheckStringNull(reader["Email"]);
							user.InviteStatusId = Common.CheckIntegerNull(reader["InviteStatus"]);
							user.PermissionLevelId = Common.CheckIntegerNull(reader["PermissionLevel"]);
							user.DateInvited = Common.CheckDateTimeNull(reader["DateInvited"]);
							user.DateRecieved = Common.CheckDateTimeNull(reader["DateReceived"]);
							user.AspNetUsersId = Common.CheckStringNull(reader["AspNetUsersId"]);
							user.InviteCode = Common.CheckStringNull(reader["InviteCode"]);
						}
					}
				}

				return user;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool DeleteInvitedUser(int InvitedUserId)
		{
			try
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();
					param.Add(new SqlParameter("InvitedUserId", InvitedUserId));

					SqlHelper.ExecuteNonQuery(connection, System.Data.CommandType.StoredProcedure, "Project-DeleteInvitedUser", param.ToArray());
				}

				return true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool CheckIsUserInvited(string AspNetUsersId)
		{
			try
			{
				bool IsInvitedUser = true;

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					SqlParameter[] param = new SqlParameter[1];
					param[0] = new SqlParameter("AspNetUsersId", AspNetUsersId);

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Account-CheckIfUserInvited", param))
					{
						while (reader.Read())
						{
							IsInvitedUser = Common.CheckBooleanNull(reader["IsInvitedUser"]);
						}
					}
				}

				return IsInvitedUser;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
	}
}
