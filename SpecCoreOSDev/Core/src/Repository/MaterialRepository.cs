﻿using Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Repository
{
    public class MaterialRepository
    {
        private string ConnectionString = AppConfiguration.ConnectionString;

        public List<Material> GetMaterials()
        {
            try
            {
                List<Material> materials = new List<Material>();

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Material-GetMaterials", null))
                    {
                        Material material = null;
                        while (reader.Read())
                        {
                            material = new Material();

                            material.MaterialId = Common.CheckIntegerNull(reader["MaterialId"]);
                            material.Name = Common.CheckStringNull(reader["Name"]);
                            material.UnitMeasure = Common.CheckStringNull(reader["UnitOfMeasure"]);
                            material.HighBudget = Common.CheckDecimalNull(reader["HighBudget"]);
                            material.LowBudget = Common.CheckDecimalNull(reader["LowBudget"]);

                            materials.Add(material);
                        }
                    }
                }

                return materials;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Material GetMaterial(int MaterialId)
        {
            try
            {
                Material material = null;

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter("MaterialId", MaterialId);

                    using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Material-GetMaterialById", param))
                    {
                        while (reader.Read())
                        {
                            material = new Material();
                            material.MaterialId = Common.CheckIntegerNull(reader["MaterialId"]);
                            material.Name = Common.CheckStringNull(reader["Name"]);
                            material.UnitMeasure = Common.CheckStringNull(reader["UnitOfMeasure"]);
                            material.HighBudget = Common.CheckDecimalNull(reader["HighBudget"]);
                            material.LowBudget = Common.CheckDecimalNull(reader["LowBudget"]);
                        }
                    }
                }

                return material;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SaveMaterial(Material model)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    List<SqlParameter> param = new List<SqlParameter>();

                    param.Add(new SqlParameter("MaterialId", model.MaterialId));
                    param.Add(new SqlParameter("Name", model.Name));
                    param.Add(new SqlParameter("UnitOfMeasure", model.UnitMeasure));
                    param.Add(new SqlParameter("HighBudget", model.HighBudget));
                    param.Add(new SqlParameter("LowBudget", model.LowBudget));

                    object materialId = SqlHelper.ExecuteScalar(connection, System.Data.CommandType.StoredProcedure, "Material-SaveMaterial", param.ToArray());

                    return Common.CheckIntegerNull(materialId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteMaterial(int MaterialId)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter("MaterialId", MaterialId);

                    SqlHelper.ExecuteNonQuery(connection, System.Data.CommandType.StoredProcedure, "Material-DeleteMaterial", param);
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
