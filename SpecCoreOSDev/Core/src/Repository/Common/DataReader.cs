﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Repository
{
    public class DataReader
    {
        #region Class Data
        private IDataReader _reader = null;
        private IDbConnection _connection = null;
        private List<string> _schema = null;

        #endregion

        #region Public Methods

        /// <summary>
        /// used for closing a reader
        /// </summary>
        public void Close()
        {
            if (_reader != null && !_reader.IsClosed)
                _reader.Close();

            if (_connection != null && _connection.State == ConnectionState.Open)
                _connection.Close();
        }

        /// <summary>
        /// used for reading a reder
        /// </summary>
        /// <returns>true if the reader is not empty and can be open</returns>
        public bool Read()
        {
            return _reader.Read();
        }

        /// <summary>
        /// return true if the reader succeded to move to the next result set
        /// </summary>
        /// <returns></returns>
        public bool NextResult()
        {
            bool hasNext = _reader.NextResult();

            if (_schema == null)
                _schema = new List<string>();
            else
                _schema.Clear();

            for (int i = 0; i < _reader.FieldCount; i++)
                _schema.Add(_reader.GetName(i));

            return hasNext;
        }

        #endregion

        #region Get Methods

        public int GetInt(string column)
        {
            int result = -1;

            if (!HasColumn(column) || _reader[column].ToString().Equals(""))
                return -1;

            int.TryParse(_reader[column].ToString(), out result);

            return result;
        }

        public string GetString(string column)
        {
            if (!HasColumn(column))
                return "";
            else
                return _reader[column].ToString();
        }

        public double GetDouble(string column)
        {
            double result = -1;

            if (!HasColumn(column))
                return -1;

            double.TryParse(_reader[column].ToString(), out result);

            return result;
        }

        public decimal GetDecimal(string column)
        {
            decimal result = -1;

            if (!HasColumn(column))
                return -1;

            decimal.TryParse(_reader[column].ToString(), out result);

            return result;
        }

        public bool GetBool(string column)
        {
            bool result = false;

            if (!HasColumn(column))
                return false;

            bool.TryParse(_reader[column].ToString(), out result);

            return result;
        }

        public DateTime GetDateTime(string column)
        {
            DateTime result = DateTime.Now;

            if (!HasColumn(column))
                return DateTime.MinValue;

            DateTime.TryParse(_reader[column].ToString(), out result);

            return result;
        }

        public byte GetByte(string column)
        {
            byte result = 0;

            if (!HasColumn(column) || _reader[column].ToString().Equals(""))
                return 0;

            byte.TryParse(_reader[column].ToString(), out result);

            return result;
        }
        #endregion

        #region Public Properties

        public IDataReader Reader
        {
            get { return _reader; }
            set
            {
                _reader = value;

                if (_schema == null)
                    _schema = new List<string>();
                else
                    _schema.Clear();

                for (int i = 0; i < _reader.FieldCount; i++)
                    _schema.Add(_reader.GetName(i));
            }
        }
        public IDbConnection Connection
        {
            get { return _connection; }
            set { _connection = value; }
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// returns true if the current schema containg the given column
        /// </summary>
        /// <param name="column">column name</param>
        /// <returns>true/false</returns>
        private bool HasColumn(string column)
        {
            return _schema.Contains(column);
        }
        #endregion
    }

}
