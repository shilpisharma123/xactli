﻿using Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Repository
{
	public class AccountRepository
	{
		private string ConnectionString = AppConfiguration.ConnectionString;

		public AccountInformation GetAccountInformation(string AspNetUsersId)
		{
			try
			{
				AccountInformation info = null;

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					SqlParameter[] param = new SqlParameter[1];
					param[0] = new SqlParameter("AspNetUsersId", AspNetUsersId);

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Account-GetAccountInformation", param))
					{
						while (reader.Read())
						{
							info = new AccountInformation();

							info.ContactId = Common.CheckIntegerNull(reader["ContactId"]);
							info.Name = Common.CheckStringNull(reader["Name"]);
							info.Email = Common.CheckStringNull(reader["Email"]);
							info.Phone = Common.CheckStringNull(reader["Phone"]);
							info.Notes = Common.CheckStringNull(reader["Notes"]);
							info.City = Common.CheckStringNull(reader["City"]);
							info.State = Common.CheckStringNull(reader["State"]);
							info.Zip = Common.CheckStringNull(reader["Zip"]);
							info.Address = Common.CheckStringNull(reader["StreetAddress"]);
							info.AspNetUsersId = Common.CheckStringNull(reader["AspNetUsersId"]);
						}
					}
				}

				return info;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public int UpdateBasicAccountInfo(AccountInformation model)
		{
			try
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();

					param.Add(new SqlParameter("Email", model.Email));
					param.Add(new SqlParameter("Name", model.Name));
					param.Add(new SqlParameter("Phone", model.Phone));
					param.Add(new SqlParameter("AspNetUsersId", model.AspNetUsersId));

					object contactId = SqlHelper.ExecuteScalar(connection, System.Data.CommandType.StoredProcedure, "Account-UpdateBasicAccountInfo", param);

					return Common.CheckIntegerNull(contactId);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public int SaveCompanyInfo(AccountInformation model)
		{
			try
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();

					param.Add(new SqlParameter("Name", model.Name));
					param.Add(new SqlParameter("CompanyName", model.CompanyName));
					param.Add(new SqlParameter("Phone", model.Phone));
					param.Add(new SqlParameter("City", model.City));
					param.Add(new SqlParameter("State", model.State));
					param.Add(new SqlParameter("Zip", model.Zip));
					param.Add(new SqlParameter("Address", model.Address));
					param.Add(new SqlParameter("AspNetUsersId", model.AspNetUsersId));

					object contactId = SqlHelper.ExecuteScalar(connection, System.Data.CommandType.StoredProcedure, "Account-SaveAccount", param);

					return Common.CheckIntegerNull(contactId);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public List<InvitedUser> GetAccountUsers(string AspnetUsersId)
		{
			try
			{
				List<InvitedUser> users = new List<InvitedUser>();

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();
					param.Add(new SqlParameter("AspnetUsersId", AspnetUsersId));

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Account-GetAccountUsers", param))
					{
						InvitedUser user = null;
						while (reader.Read())
						{
							user = new InvitedUser();	

							user.InvitedUserId = Common.CheckIntegerNull(reader["ProjectInvitedContactId"]);
							user.ProjectId = Common.CheckIntegerNull(reader["ProjectId"]);
							user.ProjectLocationTradeId = Common.CheckIntegerNull(reader["ProjectLocationTradeId"]);
							user.TradeName = Common.CheckStringNull(reader["TradeName"]);
							user.ContactId = Common.CheckIntegerNull(reader["ContactId"]);
							user.ContactName = Common.CheckStringNull(reader["ContactName"]);
							user.Email = Common.CheckStringNull(reader["Email"]);
							user.InviteStatusId = Common.CheckIntegerNull(reader["InviteStatus"]);
							user.PermissionLevelId = Common.CheckIntegerNull(reader["PermissionLevel"]);
							user.DateInvited = Common.CheckDateTimeNull(reader["DateInvited"]);
							user.DateRecieved = Common.CheckDateTimeNull(reader["DateReceived"]);
							user.ProjectName = Common.CheckStringNull(reader["ProjectName"]);

							users.Add(user);
						}
					}
				}

				return users;
			}
			catch (Exception)
			{
				throw;
			}
		}

		public int SaveSignupCompanyInfo(string AspNetUsersId, string Name, string Email, string CompanyName, string Phone, string City, string State, string Zip, string Address, int PackageId)
		{
			try
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();

					param.Add(new SqlParameter("AspNetUsersId", AspNetUsersId));
					param.Add(new SqlParameter("Name", Name));
					param.Add(new SqlParameter("Email", Email));
					param.Add(new SqlParameter("CompanyName", CompanyName));
					param.Add(new SqlParameter("Phone", Phone));
					param.Add(new SqlParameter("City", City));
					param.Add(new SqlParameter("State", State));
					param.Add(new SqlParameter("Zip", Zip));
					param.Add(new SqlParameter("Address", Address));
					param.Add(new SqlParameter("PackageId", PackageId));

					object contactId = SqlHelper.ExecuteScalar(connection, System.Data.CommandType.StoredProcedure, "Account-SaveNewUserSignUp", param);

					return Common.CheckIntegerNull(contactId);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public int SaveAspNetUsersProject(int ProjectId, string AspNetUsersId, int ProjectInviteContactId = 0)
		{
			try
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();

					param.Add(new SqlParameter("AspNetUsersId", AspNetUsersId));
					param.Add(new SqlParameter("ProjectId", ProjectId));
					param.Add(new SqlParameter("ProjectInviteContactId", ProjectInviteContactId));

					object contactId = SqlHelper.ExecuteScalar(connection, System.Data.CommandType.StoredProcedure, "Account-SaveAccountSignUp", param.ToArray());

					return Common.CheckIntegerNull(contactId);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public int SaveBillingInfo(BillingInformation model)
		{
			try
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();

					param.Add(new SqlParameter("@NameOnCard", model.NameOnCard));
					param.Add(new SqlParameter("@BillingAddress", model.BillingAddress));
					param.Add(new SqlParameter("@City", model.City));
					param.Add(new SqlParameter("@State", model.State));
					param.Add(new SqlParameter("@Zip", model.Zip));
					param.Add(new SqlParameter("@PhoneNumber", model.PhoneNumber));
					param.Add(new SqlParameter("@CreditCardNo", model.CreditCardNo));
					param.Add(new SqlParameter("@BillingDate", model.BillingDate));

					object accountInfoId = SqlHelper.ExecuteScalar(connection, System.Data.CommandType.StoredProcedure, "BillingInfo-SaveBillingInfo", param);

					return Common.CheckIntegerNull(accountInfoId);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
	}
}
