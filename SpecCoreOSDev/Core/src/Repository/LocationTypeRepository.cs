﻿using Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Repository
{
    public class LocationTypeRepository
    {
        private string ConnectionString = AppConfiguration.ConnectionString;

        public List<LocationType> GetLocationTypes()
        {
            try
            {                
                List<LocationType> locations = new List<LocationType>();

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {    
                    using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "LocationType-GetLocationsTypes", null))
                    {
                        LocationType location = null;
                        while (reader.Read())
                        {
                            location = new LocationType();

                            location.LocationTypeId = Common.CheckIntegerNull(reader["LocationTypeID"]);
                            location.Name = Common.CheckStringNull(reader["Name"]);
                            location.Category = Common.CheckStringNull(reader["Category"]);

                            locations.Add(location);
                        }
                    }
                }

                return locations;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public LocationType GetLocationTemplate(int LocationTemplateId)
        {
            try
            {
                LocationType location = null;

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter("LocationTypeId", LocationTemplateId);

                    using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "LocationType-GetLocationTypeById", param))
                    {
                        while (reader.Read())
                        {
                            location = new LocationType();

                            location.LocationTypeId = Common.CheckIntegerNull(reader["LocationTypeID"]);
                            location.Name = Common.CheckStringNull(reader["Name"]);
                            location.Category = Common.CheckStringNull(reader["Category"]);
                        }
                    }
                }

                return location;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SaveLocation(LocationType model)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    List<SqlParameter> param = new List<SqlParameter>();

                    param.Add(new SqlParameter("LocationTypeId", model.LocationTypeId));
                    param.Add(new SqlParameter("Name", model.Name));
                    param.Add(new SqlParameter("Category", model.Category));

                    object locationTemplateId = SqlHelper.ExecuteScalar(connection, System.Data.CommandType.StoredProcedure, "LocationType-SaveLocationType", param.ToArray());

                    return Common.CheckIntegerNull(locationTemplateId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }        
        }

        public bool DeleteLocationType(int LocationTypeId)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter("LocationTypeId", LocationTypeId);

                    SqlHelper.ExecuteNonQuery(connection, System.Data.CommandType.StoredProcedure, "LocationType-DeleteLocationType", param) ;
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SaveAssociation(LocationTradeMaterialAssociation model)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    List<SqlParameter> param = new List<SqlParameter>();

                    param.Add(new SqlParameter("LocationTypeId", model.LocationId));
                    param.Add(new SqlParameter("TradeId", model.TradeId));
                    param.Add(new SqlParameter("MaterialId", model.MaterialId));

                    param.Add(new SqlParameter("DefaultQuantity", model.DefaultQuantity));

                    object id = SqlHelper.ExecuteScalar(connection, System.Data.CommandType.StoredProcedure, "LocationTradeMaterial-Association", param.ToArray());

                    return Common.CheckIntegerNull(id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AssociationValues> GetAssociation()
        {
            try
            {
                List<AssociationValues> associationValues = new List<AssociationValues>();

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "LocationTradeMaterial-GetAssociation", null))
                    {
                        AssociationValues association = null;
                        while (reader.Read())
                        {
                            association = new AssociationValues();

                            association.AssociationId = Common.CheckIntegerNull(reader["LocationTradeMaterialId"]);

                            association.LocationName = Common.CheckStringNull(reader["LocationName"]);
                            association.TradeName = Common.CheckStringNull(reader["TradeName"]);
                            association.MaterialName = Common.CheckStringNull(reader["MaterialName"]);

                            association.DefaultQuantity = Common.CheckIntegerNull(reader["DefaultQuantity"]);

                            associationValues.Add(association);
                        }
                    }
                }

                return associationValues;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AssociationValues GetAssociation(int associationId)
        {
            try
            {
                AssociationValues association = null;

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter("AssociationId", associationId);

                    using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "LocationTradeMaterial-GetAssociationById", param))
                    {
                        while (reader.Read())
                        {
                            association = new AssociationValues();

                            association.AssociationId = Common.CheckIntegerNull(reader["LocationTradeMaterialId"]);

                            association.LocationName = Common.CheckStringNull(reader["LocationName"]);
                            association.TradeName = Common.CheckStringNull(reader["TradeName"]);
                            association.MaterialName = Common.CheckStringNull(reader["MaterialName"]);

                            association.DefaultQuantity = Common.CheckIntegerNull(reader["DefaultQuantity"]);
                        }
                    }
                }

                return association;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteAssociation(int id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter("LocationTradeMaterialId", id);

                    SqlHelper.ExecuteNonQuery(connection, System.Data.CommandType.StoredProcedure, "LocationTradeMaterial-DeleteAssociation", param);
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateAssociation(int associationId, int defaultQuantity)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    List<SqlParameter> param = new List<SqlParameter>();

                    param.Add(new SqlParameter("AssociationId", associationId));
                    param.Add(new SqlParameter("DefaultQuantity", defaultQuantity));

                    object id = SqlHelper.ExecuteScalar(connection, System.Data.CommandType.StoredProcedure, "LocationTradeMaterial-UpdateAssociation", param.ToArray());

                    return Common.CheckIntegerNull(id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
