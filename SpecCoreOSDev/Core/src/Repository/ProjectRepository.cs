﻿using Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Models;
namespace Repository
{
	public class ProjectRepository
	{
		private string ConnectionString = AppConfiguration.ConnectionString;

		public List<Project> GetProjects()
		{
			try
			{
				List<Project> projects = new List<Project>();

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Project-GetProjects", null))
					{
						Project project = null;

						while (reader.Read())
						{
							project = new Project();
							project.ProjectId = Common.CheckIntegerNull(reader["ProjectId"]);
							project.ProjectName = Common.CheckStringNull(reader["ProjectName"]);
							project.OwnerName = Common.CheckStringNull(reader["OwnerName"]);
							project.Address = Common.CheckStringNull(reader["Address"]);
							project.City = Common.CheckStringNull(reader["City"]);
							project.Email = Common.CheckStringNull(reader["Email"]);
							project.AspNetUsersId = Common.CheckStringNull(reader["AspNetUsersId"]);
							project.Phone = Common.CheckStringNull(reader["Phone"]);
							project.State = Common.CheckStringNull(reader["State"]);
							project.Zip = Common.CheckStringNull(reader["Zip"]);

							projects.Add(project);
						}
					}
				}

				return projects;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public List<Project> GetProjectsByUserAccess(string AspNetUsersId)
		{
			try
			{
				List<Project> projects = new List<Project>();

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();
					param.Add(new SqlParameter("AspNetUsersId", AspNetUsersId));

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Project-GetProjects_By_User_Access", param))
					{
						Project project = null;

						while (reader.Read())
						{
							project = new Project();
							project.ProjectId = Common.CheckIntegerNull(reader["ProjectId"]);
							project.ProjectName = Common.CheckStringNull(reader["ProjectName"]);
							project.OwnerName = Common.CheckStringNull(reader["OwnerName"]);
							project.Address = Common.CheckStringNull(reader["Address"]);
							project.City = Common.CheckStringNull(reader["City"]);
							project.Email = Common.CheckStringNull(reader["Email"]);
							project.AspNetUsersId = Common.CheckStringNull(reader["AspNetUsersId"]);
							project.Phone = Common.CheckStringNull(reader["Phone"]);
							project.State = Common.CheckStringNull(reader["State"]);
							project.Zip = Common.CheckStringNull(reader["Zip"]);

							projects.Add(project);
						}
					}
				}

				return projects;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public List<Project> GetProjects(string AspNetUsersId)
		{
			try
			{
				List<Project> projects = new List<Project>();

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();
					param.Add(new SqlParameter("AspNetUsersId", AspNetUsersId));

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Project-GetProjects_By_User", param.ToArray()))
					{
						Project project = null;
						while (reader.Read())
						{
							project = new Project();
							project.ProjectId = Common.CheckIntegerNull(reader["ProjectId"]);
							project.ProjectName = Common.CheckStringNull(reader["ProjectName"]);
							project.OwnerName = Common.CheckStringNull(reader["OwnerName"]);
							project.Address = Common.CheckStringNull(reader["Address"]);
							project.City = Common.CheckStringNull(reader["City"]);
							project.Email = Common.CheckStringNull(reader["Email"]);
							project.AspNetUsersId = Common.CheckStringNull(reader["AspNetUsersId"]);
							project.IsOwner = Common.CheckBooleanNull(reader["IsOwner"]);
							project.Phone = Common.CheckStringNull(reader["Phone"]);
							project.State = Common.CheckStringNull(reader["State"]);
							project.Zip = Common.CheckStringNull(reader["Zip"]);

							projects.Add(project);
						}
					}
				}

				return projects;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public ProjectDetails GetProjectDetailsWithPermissions(int ProjectId, string AspnetUsersId)
		{
			ProjectDetails project = new ProjectDetails();

			try
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();
					param.Add(new SqlParameter("ProjectId", ProjectId));
					param.Add(new SqlParameter("AspnetUsersId", AspnetUsersId));

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Project-GetProjectWithPermissions", param))
					{
						while (reader.Read())
						{
							project.ProjectId = Common.CheckIntegerNull(reader["ProjectId"]);
							project.ProjectName = Common.CheckStringNull(reader["ProjectName"]);
							project.OwnerName = Common.CheckStringNull(reader["OwnerName"]);
							project.Address = Common.CheckStringNull(reader["Address"]);
							project.City = Common.CheckStringNull(reader["City"]);
							project.Email = Common.CheckStringNull(reader["Email"]);
							project.Phone = Common.CheckStringNull(reader["Phone"]);
							project.State = Common.CheckStringNull(reader["State"]);
							project.Zip = Common.CheckStringNull(reader["Zip"]);
							project.AspNetUsersId = Common.CheckStringNull(reader["AspNetUsersId"]);

							project.InvitedProjectId = Common.CheckIntegerNull(reader["InvitedProjectId"]);
							project.ProjectLocationTradeId = Common.CheckIntegerNull(reader["ProjectLocationTradeId"]);
							project.PermissionLevel = Common.CheckIntegerNull(reader["PermissionLevel"]);
						}
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Problem Getting ProjectDetails", ex);
			}

			try
			{
				List<Location> locations = new List<Location>();

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();
					param.Add(new SqlParameter("ProjectId", ProjectId));
					param.Add(new SqlParameter("AspnetUsersId", AspnetUsersId));

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Project-GetProjectLocationsWithPermissions", param.ToArray()))
					{
						Location location = null;
						while (reader.Read())
						{
							location = new Location();
							location.LocationId = Common.CheckIntegerNull(reader["ProjectLocationId"]);
							location.TypeId = Common.CheckIntegerNull(reader["LocationTypeID"]);
							location.LocationName = Common.CheckStringNull(reader["ProjectLocationName"]);
							location.ProjectId = Common.CheckIntegerNull(reader["ProjectId"]);

							string LocationTypeName = Common.CheckStringNull(reader["LocationTypeName"]);
							string LocationTypeCategory = Common.CheckStringNull(reader["LocationTypeCategory"]);

							location.Type = new LocationType(location.TypeId, LocationTypeName, LocationTypeCategory);

							location.InvitedProjectId = Common.CheckIntegerNull(reader["InvitedProjectId"]);
							location.ProjectLocationTradeId = Common.CheckIntegerNull(reader["ProjectLocationTradeId"]);
							location.PermissionLevel = Common.CheckIntegerNull(reader["PermissionLevel"]);

							locations.Add(location);
						}
					}
				}

				project.Locations = locations;
			}
			catch (Exception ex)
			{
				throw ex;
			}

			return project;
		}

		public Project GetProject(int ProjectId)
		{
			try
			{
				Project project = new Project();

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					SqlParameter[] param = new SqlParameter[1];
					param[0] = new SqlParameter("ProjectId", ProjectId);

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Project-GetProjectbyId", param))
					{
						while (reader.Read())
						{
							project.ProjectId = Common.CheckIntegerNull(reader["ProjectId"]);
							project.ProjectName = Common.CheckStringNull(reader["ProjectName"]);
							project.OwnerName = Common.CheckStringNull(reader["OwnerName"]);
							project.Address = Common.CheckStringNull(reader["Address"]);
							project.City = Common.CheckStringNull(reader["City"]);
							project.Email = Common.CheckStringNull(reader["Email"]);
							project.Phone = Common.CheckStringNull(reader["Phone"]);
							project.State = Common.CheckStringNull(reader["State"]);
							project.Zip = Common.CheckStringNull(reader["Zip"]);
						}
					}
				}

				return project;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public Material GetMaterialBy(int Id)
		{
			try
			{
				Material material = new Material();

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					SqlParameter[] param = new SqlParameter[1];
					param[0] = new SqlParameter("Id", Id);

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Project-GetMaterialById", param))
					{
						while (reader.Read())
						{
							material.ProjectLocationTradeMaterialId = Common.CheckIntegerNull(reader["ProjectLocationTradeMaterialId"]);
							material.MaterialId = Common.CheckIntegerNull(reader["MaterialId"]);
							material.Name = Common.CheckStringNull(reader["MaterialName"]);
							material.Quantity = Common.CheckIntegerNull(reader["Quantity"]);
							material.UnitMeasure = Common.CheckStringNull(reader["UnitOfMeasure"]);
							material.Estimate = Common.CheckDecimalNull(reader["Estimate"]);
							material.Actual = Common.CheckDecimalNull(reader["Actual"]);
							material.LowBudget = Common.CheckDecimalNull(reader["LowBudget"]);
							material.HighBudget = Common.CheckDecimalNull(reader["HighBudget"]);
							material.Manufacturer = Common.CheckStringNull(reader["Manufacturer"]);
							material.Finish = Common.CheckStringNull(reader["Finish"]);
							material.Cost = Common.CheckDecimalNull(reader["Cost"]);
							material.LeadTime = Common.CheckStringNull(reader["LeadTime"]);
							material.PlanReference = Common.CheckStringNull(reader["PlanReference"]);
							material.Description = Common.CheckStringNull(reader["Description"]);
							material.HaveNeedBy = Common.CheckStringNull(reader["HaveNeedBy"]);
							material.Model = Common.CheckStringNull(reader["Model"]);
						}
					}
				}

				return material;
			}
			catch (Exception ex)
			{
				throw new Exception("Problem Getting Material Details", ex);
			}
		}

		public ProjectDetails GetProjectDetails(int ProjectId)
		{
			try
			{
				ProjectDetails project = new ProjectDetails();

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					SqlParameter[] param = new SqlParameter[1];
					param[0] = new SqlParameter("ProjectId", ProjectId);

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Project-GetProjectById", param))
					{
						while (reader.Read())
						{
							project.ProjectId = Common.CheckIntegerNull(reader["ProjectId"]);
							project.ProjectName = Common.CheckStringNull(reader["ProjectName"]);
							project.OwnerName = Common.CheckStringNull(reader["OwnerName"]);
							project.Address = Common.CheckStringNull(reader["Address"]);
							project.City = Common.CheckStringNull(reader["City"]);
							project.Email = Common.CheckStringNull(reader["Email"]);
							project.Phone = Common.CheckStringNull(reader["Phone"]);
							project.State = Common.CheckStringNull(reader["State"]);
							project.Zip = Common.CheckStringNull(reader["Zip"]);
						}
					}
				}

				project.Locations = new LocationRepository().GetLocations(project.ProjectId);
				return project;
			}
			catch (Exception ex)
			{
				throw new Exception("Problem Getting ProjectDetails", ex);
			}
		}

		public ProjectDetails GetProjectDetailsWithTrades(int ProjectId)
		{
			try
			{
				ProjectDetails project = new ProjectDetails();

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					SqlParameter[] param = new SqlParameter[1];
					param[0] = new SqlParameter("ProjectId", ProjectId);

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Project-GetProjectById", param))
					{
						while (reader.Read())
						{
							project.ProjectId = Common.CheckIntegerNull(reader["ProjectId"]);
							project.ProjectName = Common.CheckStringNull(reader["ProjectName"]);
							project.OwnerName = Common.CheckStringNull(reader["OwnerName"]);
							project.Address = Common.CheckStringNull(reader["Address"]);
							project.City = Common.CheckStringNull(reader["City"]);
							project.Email = Common.CheckStringNull(reader["Email"]);
							project.Phone = Common.CheckStringNull(reader["Phone"]);
							project.State = Common.CheckStringNull(reader["State"]);
							project.Zip = Common.CheckStringNull(reader["Zip"]);
						}
					}
				}

				project.Locations = new LocationRepository().GetLocationWithTradesAndMaterials(project.ProjectId);
				//project.Locations = new LocationRepository().GetLocations(project.ProjectId);
				return project;
			}
			catch (Exception ex)
			{
				throw new Exception("Problem Getting ProjectDetails", ex);
			}
		}

		public int SaveProject(Project project)
		{
			try
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();

					param.Add(new SqlParameter("ProjectId", project.ProjectId));
					param.Add(new SqlParameter("ProjectName", project.ProjectName));
					param.Add(new SqlParameter("OwnerName", project.OwnerName));
					param.Add(new SqlParameter("Address", project.Address));
					param.Add(new SqlParameter("City", project.City));
					param.Add(new SqlParameter("Email", project.Email));
					param.Add(new SqlParameter("Phone", project.Phone));
					param.Add(new SqlParameter("State", project.State));
					param.Add(new SqlParameter("Zip", project.Zip));
					param.Add(new SqlParameter("AspNetusersId", project.AspNetUsersId));

					object projectId = SqlHelper.ExecuteScalar(connection, System.Data.CommandType.StoredProcedure, "Project-SaveProject", param.ToArray());

					return Common.CheckIntegerNull(projectId);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
        //Code change by jeet on 12/9/18 for saving tags for multiple images
        public void UpdateMultipleSelectedImgTags(int Id, int ProjectId, int LocationId, int TradeId, int MaterialId, string AspNetUsersId, int ImageOrderSequence)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    List<SqlParameter> param = new List<SqlParameter>();

                    param.Add(new SqlParameter("@ProjectId", ProjectId));
                    param.Add(new SqlParameter("@TradeId", TradeId));
                    param.Add(new SqlParameter("@LocationId", LocationId));
                    param.Add(new SqlParameter("@MaterialId", MaterialId));
                    param.Add(new SqlParameter("@AspNetUsersId", AspNetUsersId));
                    param.Add(new SqlParameter("@ProjectLocationTradeMaterialImagesId", Id));
                    param.Add(new SqlParameter("@ImageOrderSequence", ImageOrderSequence));
                    //Code to enter this into the database
                    SqlHelper.ExecuteNonQuery(connection, System.Data.CommandType.StoredProcedure, "UpdateTradeMaterialMultipleImages", param.ToArray());

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Modified by :Jeet Bhandari
        //Modified date:02-Nov-2018
        public int SaveProjectTradeMaterialImage(byte[] image, int ProjectId, int LocationId, int TradeId, int MaterialId, string AspNetUsersId, int SequenceNumber)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    List<SqlParameter> param = new List<SqlParameter>();

                    param.Add(new SqlParameter("ProjectId", ProjectId));
                    param.Add(new SqlParameter("TradeId", TradeId));
                    param.Add(new SqlParameter("MaterialId", MaterialId));
                    param.Add(new SqlParameter("Image", image));
                    param.Add(new SqlParameter("LocationId", LocationId));
                    param.Add(new SqlParameter("AspNetUsersId", AspNetUsersId));
                    param.Add(new SqlParameter("ImageOrderSequence", SequenceNumber));

                    //Code to enter this into the database
                    object projectTradeMaterialId = SqlHelper.ExecuteScalar(connection, System.Data.CommandType.StoredProcedure, "Project-SaveProjectMaterialImages", param.ToArray());

                    return Common.CheckIntegerNull(projectTradeMaterialId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int SaveNoteHistory(int ProjectMaterialImageId, string Notes, DateTime dateTimeStamp, string AspNetUsersId)
		{
			try
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();

					param.Add(new SqlParameter("ProjectLocationTradeMaterialImagesId", ProjectMaterialImageId));
					param.Add(new SqlParameter("NoteName", Notes));
					param.Add(new SqlParameter("DateTimeStamp", dateTimeStamp));
					param.Add(new SqlParameter("AspNetUsersId", AspNetUsersId));

					//Code to enter this into the database
					object projectTradeMaterialId = SqlHelper.ExecuteScalar(connection, System.Data.CommandType.StoredProcedure, "Project-SaveNotesHistory", param.ToArray());

					return Common.CheckIntegerNull(projectTradeMaterialId);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		public List<ProjectTradeMaterialImage> GetProjectTradeMaterialImages(int ProjectId, int TradeId, int MaterialId)
		{
			try
			{
				List<ProjectTradeMaterialImage> projectTradeMaterialImages = new List<ProjectTradeMaterialImage>();

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();
					param.Add(new SqlParameter("ProjectId", ProjectId));
					param.Add(new SqlParameter("TradeId", TradeId));
					param.Add(new SqlParameter("MaterialId", MaterialId));

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Project-GetProjectTradeMaterialImages", param.ToArray()))
					{
						ProjectTradeMaterialImage projectTradeMaterialsImage = null;
						while (reader.Read())
						{
							projectTradeMaterialsImage = new ProjectTradeMaterialImage();
							projectTradeMaterialsImage.Id = Common.CheckIntegerNull(reader["Id"]);
							projectTradeMaterialsImage.ProjectId = Common.CheckIntegerNull(reader["ProjectId"]);
							projectTradeMaterialsImage.TradeId = Common.CheckIntegerNull(reader["TradeId"]);
							projectTradeMaterialsImage.MaterialId = Common.CheckIntegerNull(reader["MaterialId"]);
							projectTradeMaterialsImage.Image = Common.CheckImageNull(reader["Image"]);


							projectTradeMaterialImages.Add(projectTradeMaterialsImage);
						}
					}
				}
				return projectTradeMaterialImages;
			}
			catch (Exception ex)
			{
				throw new Exception("Problem Getting Images", ex);
			}
		}

		//Get all images based on project Id

		public List<ProjectTradeMaterialImage> GetProjectMultipleImagesWithoutTrade(int Id, string AspNetUsersId)
		{
			try
			{
				List<ProjectTradeMaterialImage> lstImages = new List<ProjectTradeMaterialImage>();

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();
					param.Add(new SqlParameter("@ProjectId", Id));
					param.Add(new SqlParameter("@AspNetUsersId", AspNetUsersId));

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "ProjectGetMultipleImages", param.ToArray()))
					{
						ProjectTradeMaterialImage projectImage = null;
						while (reader.Read())
						{
							projectImage = new ProjectTradeMaterialImage();
							projectImage.Image = Common.CheckImageNull(reader["Image"]);
							projectImage.Id = Common.CheckIntegerNull(reader["ProjectLocationTradeMaterialImagesId"]);
							projectImage.AspNetUsersId = AspNetUsersId;
							lstImages.Add(projectImage);
						}
					}
				}
				return lstImages;
			}
			catch (Exception ex)
			{
				throw new Exception("Problem Getting Images", ex);
			}
		}

		public List<ProjectTradeMaterialImage> GetSelectedCheckedImagesModelPopupForTag(List<int> Id, int ProjectId, string AspNetUsersId)
		{
			try
			{
				List<ProjectTradeMaterialImage> lstImages = new List<ProjectTradeMaterialImage>();
				string Ids = string.Empty;
				foreach (var item in Id)
				{
					using (SqlConnection connection = new SqlConnection(ConnectionString))
					{
						List<SqlParameter> param = new List<SqlParameter>();
						param.Add(new SqlParameter("@Ids", item.ToString()));
						param.Add(new SqlParameter("@ProjectId", ProjectId));
						param.Add(new SqlParameter("@AspNetUsersId", AspNetUsersId));

						using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "ProjectGetMultipleSelectedImages", param.ToArray()))
						{
							ProjectTradeMaterialImage projectImage = null;
							while (reader.Read())
							{
								projectImage = new ProjectTradeMaterialImage();
								projectImage.Image = Common.CheckImageNull(reader["Image"]);
								projectImage.Id = Common.CheckIntegerNull(reader["ProjectLocationTradeMaterialImagesId"]);
								projectImage.AspNetUsersId = AspNetUsersId;
								lstImages.Add(projectImage);
							}
						}
					}
				}
				return lstImages;
			}
			catch (Exception ex)
			{
				throw new Exception("Problem Getting Images", ex);
			}
		}
		//Get all the images according to projectid ,tradeid,materialid
		//Kadambini Sahoo
		//Get all the images according to projectid ,tradeid,materialid
		public List<ProjectTradeMaterialImage> GetAllProjectTradeMaterialImage(int ProjectId, int locationId, int TradeId, int MaterialId)
		{
			try
			{
				List<ProjectTradeMaterialImage> projectTradeMaterialImages = new List<ProjectTradeMaterialImage>();

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();
					param.Add(new SqlParameter("ProjectId", ProjectId));
					param.Add(new SqlParameter("LocationId", locationId));
					param.Add(new SqlParameter("TradeId", TradeId));
					param.Add(new SqlParameter("MaterialId", MaterialId));

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Project-GetProjectMaterialTradeImages", param.ToArray()))
					{
						ProjectTradeMaterialImage projectTradeMaterialsImage = null;
						NoteHistory projectTradeMaterialsImageNote = null;
						while (reader.Read())
						{
							projectTradeMaterialsImage = new ProjectTradeMaterialImage();
							projectTradeMaterialsImage.Id = Common.CheckIntegerNull(reader["ProjectLocationTradeMaterialImagesId"]);
							projectTradeMaterialsImage.ProjectId = Common.CheckIntegerNull(reader["ProjectId"]);
							projectTradeMaterialsImage.TradeId = Common.CheckIntegerNull(reader["TradeId"]);
							projectTradeMaterialsImage.MaterialId = Common.CheckIntegerNull(reader["MaterialId"]);
							projectTradeMaterialsImage.Image = Common.CheckImageNull(reader["Image"]);
							// projectTradeMaterialsImage.Notes = Common.CheckStringNull(reader["NoteName"]);
							// DateTime dtStamp = Common.CheckDateTimeNull(reader["DateTimeStamp"]);
							// projectTradeMaterialsImage.DateTimeStamp = Common.CheckDateTimeNull(reader["DateTimeStamp"]);
							// projectTradeMaterialsImage.DateString = dtStamp.ToString("MM/dd/yyyy HH:mm");
							projectTradeMaterialsImage.AspNetUsersId = Common.CheckStringNull(reader["AspNetUsersId"]);
							projectTradeMaterialsImage.UserName = Common.CheckStringNull(reader["UserName"]);
							projectTradeMaterialsImage.FinalSelection = Common.CheckBooleanNull(reader["FinalSelection"]);
							projectTradeMaterialsImage.IsFinal = Common.CheckBooleanNull(reader["IsFinal"]);
							projectTradeMaterialsImage.LocationName = Common.CheckStringNull(reader["LocationName"]);
							projectTradeMaterialsImage.TradeName = Common.CheckStringNull(reader["TradeName"]);
							projectTradeMaterialsImage.MaterialName = Common.CheckStringNull(reader["MaterialName"]);
							projectTradeMaterialImages.Add(projectTradeMaterialsImage);
						}
					}
				}
				return projectTradeMaterialImages;
			}
			catch (Exception ex)
			{
				throw new Exception("Problem Getting Images", ex);
			}
		}


		public List<ProjectTradeMaterialImage> GetProjectTradeMaterialImageSequence(int ProjectId, int TradeId, int MaterialId, string AspNetUsersId)
		{
			try
			{
				List<ProjectTradeMaterialImage> projectTradeMaterialImages = new List<ProjectTradeMaterialImage>();

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();
					param.Add(new SqlParameter("ProjectId", ProjectId));
					param.Add(new SqlParameter("TradeId", TradeId));
					param.Add(new SqlParameter("MaterialId", MaterialId));
					param.Add(new SqlParameter("AspNetUsersId", AspNetUsersId));

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Project-GetProjectMaterialImagesOrderSequence", param.ToArray()))
					{
						ProjectTradeMaterialImage projectTradeMaterialsImage = null;
						while (reader.Read())
						{
							projectTradeMaterialsImage = new ProjectTradeMaterialImage();
							projectTradeMaterialsImage.ImageOrderSequence = Common.CheckIntegerNull(reader["ImageOrderSequence"]);

							projectTradeMaterialImages.Add(projectTradeMaterialsImage);
						}
					}
				}
				return projectTradeMaterialImages;
			}
			catch (Exception ex)
			{
				throw new Exception("Problem Getting Images", ex);
			}

		}

		public List<NoteHistory> GetNotes(int Id)
		{
			try
			{
				List<NoteHistory> notes = new List<NoteHistory>();

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					SqlParameter[] param = new SqlParameter[1];
					param[0] = new SqlParameter("Id", Id);

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Project-GetProjectTradeMaterialImagesNoteById", param.ToArray()))
					{
						NoteHistory note = null;
						while (reader.Read())
						{
							note = new NoteHistory();
							note.Notes = Common.CheckStringNull(reader["NoteName"]);
							notes.Add(note);
						}
					}
				}

				return notes;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public List<NoteHistory> GetDetailsByID(int Id, int tradeId, int materialId)
		{
			try
			{
				List<NoteHistory> notes = new List<NoteHistory>();

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					SqlParameter[] param = new SqlParameter[1];
					param[0] = new SqlParameter("Id", Id);

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Project-GetProjectTradeMaterialImagesNoteById", param.ToArray()))
					{
						NoteHistory note = null;
						while (reader.Read())
						{
							note = new NoteHistory();
							note.Notes = Common.CheckStringNull(reader["NoteName"]);
							notes.Add(note);
						}
					}
				}

				return notes;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public List<NoteHistory> GetNotesHistory(int Id)
		{
			try
			{
				List<NoteHistory> notes = new List<NoteHistory>();

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					SqlParameter[] param = new SqlParameter[1];
					param[0] = new SqlParameter("Id", Id);

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Project-GetNoteHistory", param.ToArray()))
					{
						NoteHistory note = null;
						while (reader.Read())
						{
							note = new NoteHistory();
							note.Notes = Common.CheckStringNull(reader["NoteName"]);
							note.UserName = Common.CheckStringNull(reader["UserName"]);
							DateTime dtStamp = Common.CheckDateTimeNull(reader["DateTimeStamp"]);
							note.DateTimeStamp = Common.CheckDateTimeNull(reader["DateTimeStamp"]);
							note.DateString = dtStamp.ToString("MM/dd/yyyy HH:mm");
							notes.Add(note);
						}
					}
				}

				return notes;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
        //NoteKS : We get the associated tags for the image according to the image id and projecct id
        public List<ProjectTags> GetProjectTagsByImageId(int ProjectId,int Id)
        {
            try
            {
                List<ProjectTags> projectTags = new List<ProjectTags>();

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlParameter[] param = new SqlParameter[2];
                    param[0] = new SqlParameter("@ProjectId", ProjectId);
                    param[1] = new SqlParameter("@ProjectLocationTradeMaterialImageId", Id);

                    using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "ProjectTags-GetTagsByImageId", param.ToArray()))
                    {
                        ProjectTags pTag = null;
                        while (reader.Read())
                        {
                            pTag = new ProjectTags();
                            pTag.TagId = Common.CheckIntegerNull(reader["TagId"]);
                            pTag.ProjectId = Common.CheckIntegerNull(reader["ProjectId"]);
                            pTag.LocationId = Common.CheckIntegerNull(reader["LocationId"]);
                            pTag.TradeId = Common.CheckIntegerNull(reader["TradeId"]);
                            pTag.MaterialId = Common.CheckIntegerNull(reader["MaterialId"]);
                            pTag.ProjectLocationTradeMaterialImageId = Common.CheckIntegerNull(reader["ProjectLocationTradeMaterialImageId"]);
                            pTag.LocationName= Common.CheckStringNull(reader["LocationName"]);
                            pTag.TradeName = Common.CheckStringNull(reader["TradeName"]);
                            pTag.MaterialName = Common.CheckStringNull(reader["MaterialName"]);
                            projectTags.Add(pTag);
                        }
                    }
                }

                return projectTags;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Get all the images according to projectid ,tradeid,materialid
        public List<ProjectTradeMaterialImage> GetProjectTradeMaterialImageById(int Id)
		{
			try
			{
				List<ProjectTradeMaterialImage> projectTradeMaterialImages = new List<ProjectTradeMaterialImage>();

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();
					param.Add(new SqlParameter("Id", Id));


					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Project-GetProjectTradeMaterialImagesById", param.ToArray()))
					{
						ProjectTradeMaterialImage projectTradeMaterialsImage = null;
						while (reader.Read())
						{
							projectTradeMaterialsImage = new ProjectTradeMaterialImage();
							projectTradeMaterialsImage.ProjectId = Common.CheckIntegerNull(reader["ProjectId"]);
							projectTradeMaterialsImage.TradeId = Common.CheckIntegerNull(reader["TradeId"]);
							projectTradeMaterialsImage.MaterialId = Common.CheckIntegerNull(reader["MaterialId"]);
							projectTradeMaterialsImage.Image = Common.CheckImageNull(reader["Image"]);
							projectTradeMaterialsImage.AspNetUsersId = Common.CheckStringNull(reader["AspNetUsersId"]);
							projectTradeMaterialsImage.DateTimeStamp = Common.CheckDateTimeNull(reader["DateTimeStamp"]);
							projectTradeMaterialImages.Add(projectTradeMaterialsImage);
						}
					}
				}
				return projectTradeMaterialImages;
			}
			catch (Exception ex)
			{
				throw new Exception("Problem Getting Images", ex);
			}
		}


		public List<ProjectTradeMaterialImage> GetProjectTradeMaterialImagesUserNotes(int ProjectId, int TradeId, int MaterialId)
		{
			try
			{
				List<ProjectTradeMaterialImage> projectTradeMaterialImages = new List<ProjectTradeMaterialImage>();

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();
					param.Add(new SqlParameter("ProjectId", ProjectId));
					param.Add(new SqlParameter("TradeId", TradeId));
					param.Add(new SqlParameter("MaterialId", MaterialId));

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Project-GetProjectMaterialImages", param.ToArray()))
					{
						ProjectTradeMaterialImage projectTradeMaterialsImage = null;
						while (reader.Read())
						{
							projectTradeMaterialsImage = new ProjectTradeMaterialImage();
							projectTradeMaterialsImage.Id = Common.CheckIntegerNull(reader["Id"]);
							projectTradeMaterialsImage.ProjectId = Common.CheckIntegerNull(reader["ProjectId"]);
							projectTradeMaterialsImage.TradeId = Common.CheckIntegerNull(reader["TradeId"]);
							projectTradeMaterialsImage.MaterialId = Common.CheckIntegerNull(reader["MaterialId"]);
							projectTradeMaterialsImage.Image = Common.CheckImageNull(reader["Image"]);
							projectTradeMaterialsImage.Notes = Common.CheckStringNull(reader["Notes"]);
							projectTradeMaterialsImage.DateTimeStamp = Common.CheckDateTimeNull(reader["DateTimeStamp"]);
							projectTradeMaterialsImage.AspNetUsersId = Common.CheckStringNull(reader["AspNetUsersId"]);
							projectTradeMaterialImages.Add(projectTradeMaterialsImage);
						}
					}
				}
				return projectTradeMaterialImages;
			}
			catch (Exception ex)
			{
				throw new Exception("Problem Getting Images", ex);
			}
		}


		public bool DeleteMaterialImage(int imageId)
		{
			try
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();
					param.Add(new SqlParameter("Id", imageId));

					SqlHelper.ExecuteNonQuery(connection, System.Data.CommandType.StoredProcedure, "Project-DeleteTradeMaterialImage", param);

				}

				return true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public int SaveProjectTrade(Trade trade, int ProjectLocationId)
		{
			try
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();

					param.Add(new SqlParameter("ProjectLocationId", ProjectLocationId));
					param.Add(new SqlParameter("TradeId", trade.TradeId));
					param.Add(new SqlParameter("TradeName", trade.Name));

					object tradeId = SqlHelper.ExecuteScalar(connection, System.Data.CommandType.StoredProcedure, "Project-SaveProjectTrade", param.ToArray());

					return Common.CheckIntegerNull(tradeId);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		//To save the project tags -KS 7/12/2018
		public int SaveProjectTags(ProjectTags tag)
		{
			try
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();

					param.Add(new SqlParameter("ProjectId", tag.ProjectId));
					param.Add(new SqlParameter("LocationId", tag.LocationId));
					param.Add(new SqlParameter("TradeId", tag.TradeId));
					param.Add(new SqlParameter("MaterialId", tag.MaterialId));
					param.Add(new SqlParameter("ProjectLocationTradeMaterialImageId", tag.ProjectLocationTradeMaterialImageId));
                    param.Add(new SqlParameter("AspNetUsersId", tag.AspNetUsersId));

                    object tradeId = SqlHelper.ExecuteScalar(connection, System.Data.CommandType.StoredProcedure, "ProjectTags-SaveProjectTags", param.ToArray());

					return Common.CheckIntegerNull(tradeId);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

        //To delete the project tags -KS 12/12/2018
        public int DeleteProjectTags(ProjectTags tag)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    List<SqlParameter> param = new List<SqlParameter>();

                    param.Add(new SqlParameter("ProjectId", tag.ProjectId));
                    param.Add(new SqlParameter("LocationId", tag.LocationId));
                    param.Add(new SqlParameter("TradeId", tag.TradeId));
                    param.Add(new SqlParameter("MaterialId", tag.MaterialId));
                    param.Add(new SqlParameter("ProjectLocationTradeMaterialImageId", tag.ProjectLocationTradeMaterialImageId));
                    param.Add(new SqlParameter("AspNetUsersId", tag.AspNetUsersId));
                    object tradeId = SqlHelper.ExecuteScalar(connection, System.Data.CommandType.StoredProcedure, "ProjectTags-DeleteProjectTags", param.ToArray());

                    return Common.CheckIntegerNull(tradeId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SaveProjectMaterial(Material model, int ProjectLocationTradeId)
		{
			try
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();
					param.Add(new SqlParameter("ProjectLocationTradeMaterialId", model.ProjectLocationTradeMaterialId));
					param.Add(new SqlParameter("ProjectLocationTradeId", ProjectLocationTradeId));
					param.Add(new SqlParameter("MaterialId", model.MaterialId));
					param.Add(new SqlParameter("MaterialName", model.Name));
					param.Add(new SqlParameter("Model", model.Model));
					param.Add(new SqlParameter("Quantity", model.Quantity));
					param.Add(new SqlParameter("Estimate", model.Estimate));
					param.Add(new SqlParameter("Actual", model.Actual));
					param.Add(new SqlParameter("HighBudget", model.HighBudget));
					param.Add(new SqlParameter("LowBudget", model.LowBudget));
					param.Add(new SqlParameter("UnitOfMeasure", model.UnitMeasure));
					param.Add(new SqlParameter("Manufacturer", model.Manufacturer));
					param.Add(new SqlParameter("Finish", model.Finish));
					param.Add(new SqlParameter("Cost", model.Cost));
					param.Add(new SqlParameter("LeadTime", model.LeadTime));
					param.Add(new SqlParameter("PlanReference", model.PlanReference));
					param.Add(new SqlParameter("Description", model.Description));
					param.Add(new SqlParameter("HaveNeedBy", model.HaveNeedBy));

					object materialId = SqlHelper.ExecuteScalar(connection, System.Data.CommandType.StoredProcedure, "Project-SaveProjectMaterial", param.ToArray());

					return Common.CheckIntegerNull(materialId);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public void UpdateEstimateDetails(int projectLocationMaterialId, double cost, double qty, double estimate)
		{
			try
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();

					param.Add(new SqlParameter("projectLocationMaterialId", projectLocationMaterialId));
					param.Add(new SqlParameter("cost", cost));
					param.Add(new SqlParameter("qty", qty));
					param.Add(new SqlParameter("estimate", estimate));

					SqlHelper.ExecuteScalar(connection, System.Data.CommandType.StoredProcedure, "Location-UpdateLocationEstimateDetails", param.ToArray());
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool DeleteProject(int projectId, string AspNetUsersId)
		{
			try
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();
					param.Add(new SqlParameter("projectId", projectId));
					param.Add(new SqlParameter("AspNetUsersId", AspNetUsersId));

					SqlHelper.ExecuteNonQuery(connection, System.Data.CommandType.StoredProcedure, "Project-DeleteProject", param);

				}

				return true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool DeleteProjectLocation(int projectLocationId, string AspNetUsersId)
		{
			try
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();
					param.Add(new SqlParameter("projectLocationId", projectLocationId));
					param.Add(new SqlParameter("AspNetUsersId", AspNetUsersId));

					SqlHelper.ExecuteNonQuery(connection, System.Data.CommandType.StoredProcedure, "Project-DeleteProjectLocation", param);
				}

				return true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}


		public bool DeleteProjectTrade(int ProjectLocationTradeId)
		{
			try
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();
					param.Add(new SqlParameter("ProjectLocationTradeId", ProjectLocationTradeId));

					SqlHelper.ExecuteNonQuery(connection, System.Data.CommandType.StoredProcedure, "Project-DeleteProjectTrade", param);

				}

				return true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool DeleteProjectMaterial(int ProjectLocationTradeMaterialId)
		{
			try
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();
					param.Add(new SqlParameter("ProjectLocationTradeMaterialId", ProjectLocationTradeMaterialId));

					SqlHelper.ExecuteNonQuery(connection, System.Data.CommandType.StoredProcedure, "Project-DeleteProjectMaterial", param);
				}

				return true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		//update the Final Selection according to Id--Kadambini 5/12/2018
		public void UpdateFinalSelectionDetails(int Id, bool FinalSelection, string AspNetUsersId)
		{
			try
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					List<SqlParameter> param = new List<SqlParameter>();

					param.Add(new SqlParameter("ProjectLocationTradeMaterialImagesId", Id));					
                    param.Add(new SqlParameter("FinalSelection", FinalSelection));
                    param.Add(new SqlParameter("AspNetUsersId", AspNetUsersId));

                    SqlHelper.ExecuteScalar(connection, System.Data.CommandType.StoredProcedure, "Project-GetUpdateFinalSelection", param.ToArray());
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}


        //Code change by Shilpi to save project document details
        public void UpdateProjectDocument(int ProjectDocumentId, int ProjectId, int TradeId, string ProjectDocumentType)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    List<SqlParameter> param = new List<SqlParameter>();
                    param.Add(new SqlParameter("@ProjectId", ProjectId));
                    param.Add(new SqlParameter("@TradeId", TradeId));
                    param.Add(new SqlParameter("@ProjectDocumentId", ProjectDocumentId));
                    param.Add(new SqlParameter("@ProjectDocumentType", ProjectDocumentType));
                    //Code to enter this into the database
                    SqlHelper.ExecuteNonQuery(connection, System.Data.CommandType.StoredProcedure, "Project-DocumentSaveDetails", param.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SaveDocumentNoteHistory(int ProjectDocumentId, string Notes, DateTime dateTimeStamp, string AspNetUsersId)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    List<SqlParameter> param = new List<SqlParameter>();
                    param.Add(new SqlParameter("ProjectDocumentId", ProjectDocumentId));
                    param.Add(new SqlParameter("NoteName", Notes));
                    param.Add(new SqlParameter("DateTimeStamp", dateTimeStamp));
                    param.Add(new SqlParameter("AspNetUsersId", AspNetUsersId));
                    //Code to enter this into the database
                    object projectDocumentNoteHistoryId = SqlHelper.ExecuteScalar(connection, System.Data.CommandType.StoredProcedure, "Project-DocumentSaveNotesHistory", param.ToArray());
                    return Common.CheckIntegerNull(projectDocumentNoteHistoryId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Code change by shilpi on 12/12/18 to Get all files/documents based on project Id
        public List<ProjectDocument> GetAllProjectDocuments(int Id, string AspNetUsersId)
        {
            try
            {
                List<ProjectDocument> lstprojectDocument = new List<ProjectDocument>();

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    List<SqlParameter> param = new List<SqlParameter>();
                    param.Add(new SqlParameter("@ProjectId", Id));
                    param.Add(new SqlParameter("@AspNetUsersId", AspNetUsersId));

                    using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "ProjectGetMultipleDocument", param.ToArray()))
                    {
                        ProjectDocument projectDocument = null;
                        while (reader.Read())
                        {
                            projectDocument = new ProjectDocument();
                            projectDocument.Document = Common.CheckImageNull(reader["Document"]);
                            projectDocument.ProjectDocumentId = Common.CheckIntegerNull(reader["ProjectDocumentId"]);
                            projectDocument.AspNetUsersId = AspNetUsersId;
                            projectDocument.ProjectId = Id;
                            projectDocument.FileType = Common.CheckStringNull(reader["FileType"]);
                            projectDocument.ProjectDocumentType = Common.CheckStringNull(reader["ProjectDocumentType"]);
                            lstprojectDocument.Add(projectDocument);
                        }
                    }
                }
                return lstprojectDocument;
            }
            catch (Exception ex)
            {
                throw new Exception("Problem Getting Document", ex);
            }
        }
        //Added by :Shilpi
        //Modified date:12-12-2018 for doc add
        public int AddProjectDocument(byte[] Document, int ProjectId, int TradeId, string AspNetUsersId, string FileType, string DocumentName)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    List<SqlParameter> param = new List<SqlParameter>();
                    param.Add(new SqlParameter("ProjectId", ProjectId));
                    param.Add(new SqlParameter("TradeId", TradeId));
                    param.Add(new SqlParameter("ProjectDocumentType", ""));
                    param.Add(new SqlParameter("DateTimeStamp", DateTime.Now));
                    param.Add(new SqlParameter("Document", Document));
                    param.Add(new SqlParameter("AspNetUsersId", AspNetUsersId));
                    param.Add(new SqlParameter("FileType", FileType));
					param.Add(new SqlParameter("DocumentName", DocumentName));
					//Code to enter this into the database
					object projectDocumentUploadId = SqlHelper.ExecuteScalar(connection, System.Data.CommandType.StoredProcedure, "PojectDocumentAdd", param.ToArray());
                    return Common.CheckIntegerNull(projectDocumentUploadId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //code change by shilpi for opening Popup of Document
        public List<ProjectDocument> GetSelectedDocumentModelPopupDetails(int ProjectDocumentId, int ProjectId, string AspNetUsersId)
        {
            try
            {
                List<ProjectDocument> lstDocument = new List<ProjectDocument>();
                string Ids = string.Empty;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    List<SqlParameter> param = new List<SqlParameter>();
                    param.Add(new SqlParameter("@ProjectDocumentId", ProjectDocumentId));
                    param.Add(new SqlParameter("@ProjectId", ProjectId));
                    param.Add(new SqlParameter("@AspNetUsersId", AspNetUsersId));
                    using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "ProjectGetDocumentDetails", param.ToArray()))
                    {
                        ProjectDocument projectDocument = null;
                        while (reader.Read())
                        {
                            projectDocument = new ProjectDocument();
                            projectDocument.Document = Common.CheckImageNull(reader["Document"]);
                            projectDocument.ProjectDocumentId = Common.CheckIntegerNull(reader["ProjectDocumentId"]);
                            projectDocument.AspNetUsersId = AspNetUsersId;
                            projectDocument.ProjectId = Common.CheckIntegerNull(reader["ProjectId"]);
                            projectDocument.FileType = Common.CheckStringNull(reader["FileType"]);
                            projectDocument.ProjectDocumentType = Common.CheckStringNull(reader["ProjectDocumentType"]);
							projectDocument.DocumentName = Common.CheckStringNull(reader["DocumentName"]);
							projectDocument.TradeId = Common.CheckIntegerNull(reader["TradeId"]);
							lstDocument.Add(projectDocument);
                        }
                    }
                }

                return lstDocument;
            }
            catch (Exception ex)
            {
                throw new Exception("Problem Getting Images", ex);
            }
        }
        //code change by shilpi to get list of all trades
        public List<Trade> GetAllTradeList(int ProjectId)
        {
            try
            {
                List<Trade> lstTrade = new List<Trade>();
                string Ids = string.Empty;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    List<SqlParameter> param = new List<SqlParameter>();
                    param.Add(new SqlParameter("@ProjectId", ProjectId));
                    using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Project-GetAllTradeDetails", param.ToArray()))
                    {
                        Trade projectTrade = null;
                        while (reader.Read())
                        {
                            projectTrade = new Trade();
                            projectTrade.TradeId = Common.CheckIntegerNull(reader["ProjectLocationTradeId"]);
                            projectTrade.Name = Common.CheckStringNull(reader["TradeName"]);
                            lstTrade.Add(projectTrade);
                        }
                    }
                }
                return lstTrade;
            }
            catch (Exception ex)
            {
                throw new Exception("Problem Getting Trade", ex);
            }
        }
		//Kadambini Sahoo //18/12/2018
		//Get Note History by Projecctdocumentid
		public List<NoteHistory> NotesHistoryByDocId(int projectDocumentId)
		{
			try
			{
				List<NoteHistory> notes = new List<NoteHistory>();

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					SqlParameter[] param = new SqlParameter[1];
					param[0] = new SqlParameter("ProjectDocumentId", projectDocumentId);

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Project-GetNoteHistoryByProjectDocumentId", param.ToArray()))
					{
						NoteHistory note = null;
						while (reader.Read())
						{
							note = new NoteHistory();
							note.Notes = Common.CheckStringNull(reader["NoteName"]);
							note.UserName = Common.CheckStringNull(reader["UserName"]);
							DateTime dtStamp = Common.CheckDateTimeNull(reader["DateTimeStamp"]);
							note.DateTimeStamp = Common.CheckDateTimeNull(reader["DateTimeStamp"]);
							note.DateString = dtStamp.ToString("MM/dd/yyyy HH:mm");
							notes.Add(note);
						}
					}
				}

				return notes;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

	}
}
