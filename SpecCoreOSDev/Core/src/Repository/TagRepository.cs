﻿using Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Repository
{
    public class TagRepository
    {
        private string ConnectionString = AppConfiguration.ConnectionString;

        public List<Tag> GetTags()
        {
            try
            {
                List<Tag> tags = new List<Tag>();

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Tag-GetTags", null))
                    {
                        Tag tag = null;
                        while (reader.Read())
                        {
                            tag = new Tag();
                            tag.TagId = Common.CheckIntegerNull(reader["TagId"]);
                            tag.TagName = Common.CheckStringNull(reader["TagName"]);                           
                            tags.Add(tag);
                        }
                    }
                }

                return tags;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int SaveTag(Tag model)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    List<SqlParameter> param = new List<SqlParameter>();

                    param.Add(new SqlParameter("TagId", model.TagId));
                    param.Add(new SqlParameter("TagName", model.TagName));                   
                    param.Add(new SqlParameter("Status", 1));

                    object tagId = SqlHelper.ExecuteScalar(connection, System.Data.CommandType.StoredProcedure, "Tag-SaveTag", param.ToArray());

                    return Common.CheckIntegerNull(tagId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Tag GetTags(int tagId)
        {
            try
            {
                Tag tag = null;

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter("tagId", tagId);

                    using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "TagLocationTradeMaterial-GetTagById", param))
                    {
                        while (reader.Read())
                        {
                            tag = new Tag();
                            tag.TagId = Common.CheckIntegerNull(reader["TagId"]);
                            tag.LocationName = Common.CheckStringNull(reader["LocationName"]);
                            tag.TradeName = Common.CheckStringNull(reader["TradeName"]);
                            tag.MaterialName = Common.CheckStringNull(reader["MaterialName"]);
                            tag.TagName = Common.CheckStringNull(reader["TagName"]);
                        }
                    }
                }

                return tag;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteTag(int TagId)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter("TagId", TagId);

                    SqlHelper.ExecuteNonQuery(connection, System.Data.CommandType.StoredProcedure, "Tag-DeleteTag", param);
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
