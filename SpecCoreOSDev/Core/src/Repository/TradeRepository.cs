﻿using Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Repository
{
    public class TradeRepository
    {
        private string ConnectionString = AppConfiguration.ConnectionString;

        public List<Trade> GetTrades()
        {
            try
            {
                List<Trade> trades = new List<Trade>();

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Trade-GetTrades", null))
                    {
                        Trade trade = null;
                        while (reader.Read())
                        {
                            trade = new Trade();
                            trade.TradeId = Common.CheckIntegerNull(reader["TradeId"]);
                            trade.Name = Common.CheckStringNull(reader["Name"]);

                            trades.Add(trade);
                        }
                    }
                }

                return trades;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

		public List<LocationTrade> GetTradesByProjectId(int ProjectId)
		{
			try
			{
				List<LocationTrade> trades = new List<LocationTrade>();

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					SqlParameter[] param = new SqlParameter[1];
					param[0] = new SqlParameter("ProjectId", ProjectId);

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "LocationTrade-GetTradesByProjectId", param))
					{
						LocationTrade trade = null;
						while (reader.Read())
						{
							trade = new LocationTrade();

							trade.TradeId = Common.CheckIntegerNull(reader["TradeId"]);
							trade.TradeName = Common.CheckStringNull(reader["TradeName"]);

							trades.Add(trade);
						}
					}
				}

				return trades;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public List<string> GetUniqueTradesByProjectId(int projectId)
		{
			try
			{
				List<string> trades = new List<string>();

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					SqlParameter[] param = new SqlParameter[1];
					param[0] = new SqlParameter("ProjectId", projectId);

					using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "LocationTrade-GetUniqueTradesByProjectId", param))
					{
						while (reader.Read())
						{
							trades.Add(Common.CheckStringNull(reader["TradeName"]));
						}
					}
				}

				return trades;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public List<Trade> GetTradesByProjectLocationId(int ProjectLocationId)
        {
            try
            {
                List<Trade> trades = new List<Trade>();

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter("ProjectLocationId", ProjectLocationId);

                    using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "LocationTrade-GetTradesByProjectLocationId", param))
                    {
                        Trade trade = null;
                        while (reader.Read())
                        {
                            trade = new Trade();
                            trade.TradeId = Common.CheckIntegerNull(reader["TradeId"]);
                            trade.Name = Common.CheckStringNull(reader["TradeName"]);

                            trades.Add(trade);
                        }
                    }
                }

                return trades;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Trade GetTrade(int TradeId)
        {
            try
            {
                Trade trade = null;

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter("TradeId", TradeId);

                    using (SqlDataReader reader = SqlHelper.ExecuteReader(connection, System.Data.CommandType.StoredProcedure, "Trade-GetTradeById", param))
                    {
                        while (reader.Read())
                        {
                            trade = new Trade();

                            trade.TradeId = Common.CheckIntegerNull(reader["TradeId"]);
                            trade.Name = Common.CheckStringNull(reader["Name"]);
                        }
                    }
                }

                return trade;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SaveTrade(Trade model)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    List<SqlParameter> param = new List<SqlParameter>();

                    param.Add(new SqlParameter("TradeId", model.TradeId));
                    param.Add(new SqlParameter("Name", model.Name));

                    object tradeId = SqlHelper.ExecuteScalar(connection, System.Data.CommandType.StoredProcedure, "Trade-SaveTrade", param.ToArray());

                    return Common.CheckIntegerNull(tradeId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteTrade(int TradeId)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter("TradeId", TradeId);

                    SqlHelper.ExecuteNonQuery(connection, System.Data.CommandType.StoredProcedure, "Trade-DeleteTrade", param);
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
