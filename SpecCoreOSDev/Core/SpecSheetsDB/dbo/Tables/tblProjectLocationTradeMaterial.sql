﻿CREATE TABLE [dbo].[tblProjectLocationTradeMaterial] (
    [ProjectLocationTradeMaterialId] INT             IDENTITY (1, 1) NOT NULL,
    [ProjectLocationTradeId]         INT             NOT NULL,
    [MaterialId]                     INT             NULL,
    [MaterialName]                   NVARCHAR (150)  NULL,
    [Model]                          NVARCHAR (150)  NULL,
    [LowBudget]                      MONEY           NULL,
    [HighBudget]                     MONEY           NULL,
    [Estimate]                       MONEY           NULL,
    [Actual]                         MONEY           NULL,
    [ContactId]                      INT             NULL,
    [UnitOfMeasure]                  NVARCHAR (50)   NULL,
    [Quantity]                       NUMERIC (16, 2) NULL,
    [Finish]                         NVARCHAR (150)  NULL,
    [Cost]                           MONEY           NULL,
    [LeadTime]                       NVARCHAR (150)  NULL,
    [Manufacturer]                   NVARCHAR (150)  NULL,
    [PlanReference]                  NVARCHAR (150)  NULL,
    [Description]                    NVARCHAR (150)  NULL,
    [HaveNeedBy]                     NVARCHAR (150)  NULL,
    CONSTRAINT [PK_tblProjectLocationTradeMaterial] PRIMARY KEY CLUSTERED ([ProjectLocationTradeMaterialId] ASC)
);









