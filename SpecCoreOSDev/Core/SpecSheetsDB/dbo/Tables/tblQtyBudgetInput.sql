

CREATE TABLE [dbo].[tblQtyBudgetInput](
	[QtyBudgetInputId] [int] IDENTITY(1,1) NOT NULL,
	[UnitType] [nvarchar](100) NULL,
	[UnitQuantity] [decimal](18, 0) NULL,
	[UnitCost] [decimal](18, 0) NULL,
	[TotalCost] [decimal](18, 0) NULL,
	[ProjectLocationTradeMaterialId] [int] NULL,
	[ProjectLocationTradeId] [int] NULL,
	[ProjectId] [int] NULL,
	[Status] [bit] NULL,
	[DateTimeStamp] [datetime] NULL,
	[AspNetUsersId] [nvarchar](900) NULL,
 CONSTRAINT [PK_tblQtyBudgetInput] PRIMARY KEY CLUSTERED 
(
	[QtyBudgetInputId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

