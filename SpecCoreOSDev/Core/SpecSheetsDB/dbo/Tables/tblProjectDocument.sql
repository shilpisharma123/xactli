
CREATE TABLE [dbo].[tblProjectDocument](
	[ProjectDocumentId] [int] IDENTITY(1,1) NOT NULL,
	[ProjectId] [int] NULL,
	[TradeId] [int] NULL,
	[ProjectDocumentType] [varchar](100) NULL,
	[DateTimeStamp] [datetime] NULL,
	[AspNetUsersId] [nvarchar](900) NULL,
	[Document] [varbinary](max) NULL,
	[FileType] [varchar](100) NULL,
	[DocumentName] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ProjectDocumentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)



