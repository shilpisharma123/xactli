﻿CREATE TABLE [dbo].[tblProjectAspNetUsers] (
    [ProjectAspNetUsersId] INT            IDENTITY (1, 1) NOT NULL,
    [ProjectId]            INT            NOT NULL,
    [AspNetUsersId]        NVARCHAR (450) NOT NULL
);

