

CREATE TABLE [dbo].[tblProjectTags](
	[TagId] [int] IDENTITY(1,1) NOT NULL,
	[ProjectId] [int] NULL,
	[LocationId] [int] NULL,
	[TradeId] [int] NULL,
	[MaterialId] [int] NULL,
	[Status] [bit] NULL,
	[ProjectLocationTradeMaterialImageId] [int] NULL,
	[DateTimeStamp] [datetime] NULL,
	[AspNetUsersId] [nvarchar](900) NULL,
 CONSTRAINT [PK_tblPrjectTags] PRIMARY KEY CLUSTERED 
(
	[TagId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

