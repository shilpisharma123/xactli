﻿CREATE TABLE [dbo].[tblLocationTradeMaterial] (
    [LocationTradeMaterialId] INT        IDENTITY (1, 1) NOT NULL,
    [LocationTypeId]          NCHAR (10) NOT NULL,
    [TradeId]                 INT        NOT NULL,
    [MaterialId]              INT        NOT NULL,
    [DefaultQuantity]         NUMERIC(16, 2)        NULL
);



