﻿CREATE TABLE [dbo].[tblProjectTradeMaterialImages] (
    [Id]         INT             IDENTITY (1, 1) NOT NULL,
    [Image]      VARBINARY (MAX) NULL,
    [ProjectId]  INT             NOT NULL,
    [TradeId]    INT             NOT NULL,
    [MaterialId] INT             NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

