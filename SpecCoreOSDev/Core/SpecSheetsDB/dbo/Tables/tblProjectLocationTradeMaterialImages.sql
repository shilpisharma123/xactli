

CREATE TABLE [dbo].[tblProjectLocationTradeMaterialImages](
	[ProjectLocationTradeMaterialImagesId] [int] IDENTITY(1,1) NOT NULL,
	[ProjectId] [int] NOT NULL,
	[TradeId] [int] NOT NULL,
	[MaterialId] [int] NOT NULL,
	[Image] [varbinary](max) NULL,
	[AspNetUsersId] [nvarchar](450) NULL,
	[ImageOrderSequence] [int] NULL,
	[FinalSelection] [bit] NULL CONSTRAINT [df_FinalSelection]  DEFAULT ((0)),
	[LocationId] [int] NULL,
	[DateTimeStamp] [datetime] NULL CONSTRAINT [DF_DateTimeStamp]  DEFAULT (getdate()),
 CONSTRAINT [PK_tblProjectLocationTradeMaterialImages] PRIMARY KEY CLUSTERED 
(
	[ProjectLocationTradeMaterialImagesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)


