﻿CREATE TABLE [dbo].[tblProjectLocationTrade] (
    [ProjectLocationTradeID] INT           IDENTITY (1, 1) NOT NULL,
    [TradeName]              NVARCHAR (50) NULL,
    [TradeId]                INT           NULL,
    [LocationTypeId]         INT           NULL,
    [ProjectLocationID]      INT           NULL,
    CONSTRAINT [PK_tblProjectLocationTrade] PRIMARY KEY CLUSTERED ([ProjectLocationTradeID] ASC)
);

