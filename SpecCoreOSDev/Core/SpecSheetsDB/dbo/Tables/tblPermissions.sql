﻿CREATE TABLE [dbo].[tblPermissions] (
    [PermissionId] INT           IDENTITY (1, 1) NOT NULL,
    [Permission]   NVARCHAR (50) NULL,
    CONSTRAINT [PK_tblPermissions] PRIMARY KEY CLUSTERED ([PermissionId] ASC)
);

