﻿CREATE TABLE [dbo].[tblMaterial] (
    [MaterialId]  INT            IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (150) NOT NULL,
    [LowBudget]   MONEY          NULL,
    [HighBudget]  MONEY          NULL,
    [UnitOfMeasure] NVARCHAR (50)  NULL
);



