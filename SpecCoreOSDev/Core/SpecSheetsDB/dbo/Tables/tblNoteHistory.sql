

CREATE TABLE [dbo].[tblNoteHistory](
	[NoteId] [int] IDENTITY(1,1) NOT NULL,
	[ProjectLocationTradeMaterialImagesId] [int] NULL,
	[NoteName] [nvarchar](max) NOT NULL,
	[DateTimeStamp] [datetime] NULL,
	[AspNetUsersId] [nvarchar](450) NULL,
	[ProjectDocumentId] [int] NULL,
 CONSTRAINT [PK_tblNoteHistory] PRIMARY KEY CLUSTERED 
(
	[NoteId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)




