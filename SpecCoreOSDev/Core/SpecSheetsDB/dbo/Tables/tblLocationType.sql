﻿CREATE TABLE [dbo].[tblLocationType] (
    [LocationTypeId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]           NVARCHAR (150) NULL,
    [Category]       NVARCHAR (150) NULL
);

