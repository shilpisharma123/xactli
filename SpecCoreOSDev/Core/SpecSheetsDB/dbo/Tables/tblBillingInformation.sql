﻿CREATE TABLE [dbo].[BillingInformation]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [NameOnCard] NVARCHAR(200) NULL, 
    [BillingAddress] NVARCHAR(200) NULL, 
    [City] NVARCHAR(200) NULL, 
    [State] NVARCHAR(200) NULL, 
    [Zip] NVARCHAR(50) NULL, 
    [PhoneNumber] NVARCHAR(50) NULL, 
    [CreditCardNo] NVARCHAR(200) NULL, 
    [BillingDate] DATETIME NULL
)
