﻿CREATE TABLE [dbo].[tblProjectLocation] (
    [ProjectLocationId] INT            IDENTITY (1, 1) NOT NULL,
    [LocationTypeID]    INT            NOT NULL,
    [Name]              NVARCHAR (150) NOT NULL,
    [ProjectId]         INT            NOT NULL
);

