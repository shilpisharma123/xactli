﻿CREATE TABLE [dbo].[tblContact] (
    [ContactId]     INT            IDENTITY (1, 1) NOT NULL,
    [Name]          NVARCHAR (150) NOT NULL,
    [CompanyName]   NVARCHAR (150) NULL,
    [Email]         NVARCHAR (150) NULL,
    [Phone]         NVARCHAR (50)  NULL,
    [Notes]         NTEXT          NULL,
    [City]          NVARCHAR (150) NULL,
    [State]         NVARCHAR (150) NULL,
    [Zip]           NVARCHAR (50)  NULL,
    [StreetAddress] NVARCHAR (200) NULL,
    [AspNetUsersId] NVARCHAR (50)  NULL,
    [PackageId]     INT            NULL,
    [IsInvited]     BIT            NULL
);











