﻿CREATE TABLE [dbo].[tblProject] (
    [ProjectId]     INT            IDENTITY (1, 1) NOT NULL,
    [ProjectName]   NVARCHAR (150) NULL,
    [OwnerName]     NVARCHAR (150) NULL,
    [Address]       NVARCHAR (200) NULL,
    [City]          NVARCHAR (100) NULL,
    [State]         NVARCHAR (2)   NULL,
    [Zip]           NVARCHAR (15)  NULL,
    [Phone]         NVARCHAR (150) NULL,
    [Email]         NVARCHAR (200) NULL,
    [AspNetUsersId] NVARCHAR (450) NOT NULL,
    [CustomerId]    INT            NULL
);





