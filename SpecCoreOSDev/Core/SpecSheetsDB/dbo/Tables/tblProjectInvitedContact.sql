﻿CREATE TABLE [dbo].[tblProjectInvitedContact] (
    [ProjectInvitedContactId] INT              IDENTITY (1, 1) NOT NULL,
    [ProjectId]               INT              NOT NULL,
    [ProjectLocationTradeId]  INT              NULL,
    [InviteStatus]            INT              CONSTRAINT [DF_tblProjectInvitedContact_Status] DEFAULT ((0)) NOT NULL,
    [AspNetUsersId]           NVARCHAR (50)    NULL,
    [ContactId]               INT              NULL,
    [PermissionLevel]         INT              NULL,
    [DateInvited]             DATETIME         NULL,
    [DateReceived]            DATETIME         NULL,
    [InviteCode]              UNIQUEIDENTIFIER NULL,
    [IsInviteActive]          BIT              NULL
);









