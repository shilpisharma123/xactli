﻿



CREATE PROCEDURE [dbo].[Account-CheckIfUserInvited]

@AspNetUsersId nvarchar(50)
AS
BEGIN

IF EXISTS (SELECT PackageId FROM tblContact WHERE AspNetUsersId = @AspNetUsersId AND IsInvited = 0 )
SELECT 0 As IsInvitedUser;

ELSE
SELECT 1 AS IsInvitedUser;
END