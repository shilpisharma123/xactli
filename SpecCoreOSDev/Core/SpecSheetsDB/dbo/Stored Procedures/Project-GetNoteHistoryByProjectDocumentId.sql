
CREATE Procedure [dbo].[Project-GetNoteHistoryByProjectDocumentId]
@ProjectDocumentId int
AS
BEGIN
select NoteName,DateTimeStamp,AspNetUsers.UserName from [tblNoteHistory] NoteHistory Inner join
AspNetUsers on NoteHistory.AspNetUsersId=AspNetUsers.Id
where ProjectDocumentId=@ProjectDocumentId order by NoteId Desc

END
