﻿

CREATE PROCEDURE "Location-GetLocationTypes"
AS
SELECT [LocationTypeId]
      ,[Name]
      ,[Category]
  FROM [dbo].[tblLocationType]
