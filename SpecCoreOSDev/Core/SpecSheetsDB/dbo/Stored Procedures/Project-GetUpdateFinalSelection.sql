CREATE procedure [dbo].[Project-GetUpdateFinalSelection]
@ProjectLocationTradeMaterialImagesId int,
@FinalSelection bit,
@AspNetUsersId nvarchar(900)

as
begin--KS 12/12/2018 if the final selection exists for any image in the project ,trade, material; then we need to remove that image from final selection
--I have removed the code can work on future

/*
if(@FinalSelection=1)
begin
declare @imgid int,
@projectId int,
@locationId int,
@tradeId int,
@materialId int

--get projid,loctionid,tradeid,matid
select @locationId=locationId  ,@projectId=projectId
, @tradeId=tradeId , @materialId=materialId from tblProjectLocationTradeMaterialImages 
where ProjectLocationTradeMaterialImagesId=@ProjectLocationTradeMaterialImagesId and AspNetUsersId=@AspNetUsersId

--get img id whose final selection is 1
select @imgid=ProjectLocationTradeMaterialImagesId from tblProjectLocationTradeMaterialImages where 
FinalSelection=1 and locationId=@locationId and projectId=@projectId
and tradeId=@tradeId and materialId=@materialId and AspNetUsersId=@AspNetUsersId

--first remove the other image from final selection
update tblProjectLocationTradeMaterialImages set FinalSelection=0
where ProjectLocationTradeMaterialImagesId=@imgid
end
*/
--update the required image as final selection
update tblProjectLocationTradeMaterialImages set FinalSelection=@FinalSelection
where ProjectLocationTradeMaterialImagesId=@ProjectLocationTradeMaterialImagesId
end