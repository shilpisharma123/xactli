CREATE procedure "ProjectTags-SaveProjectTags"
(
@ProjectId int,
@LocationId int,
@TradeId int,
@MaterialId int,
@ProjectLocationTradeMaterialImageId int,
@AspNetUsersId nvarchar(900)
)
as

begin
--we save only if it does not exists in projecttags
declare @icount int
select @icount=count(*) from tblProjectTags 
where LocationId=@LocationId and ProjectId=@ProjectId 
and TradeId =@TradeId and MaterialId=@MaterialId and 
ProjectLocationTradeMaterialImageId=@ProjectLocationTradeMaterialImageId
and AspNetUsersId=@AspNetUsersId
if(@icount=0)
begin
insert into tblprojecttags(ProjectId ,
LocationId,
TradeId ,
MaterialId ,
ProjectLocationTradeMaterialImageId ,status,AspNetUsersId,DateTimeStamp)
values
(@ProjectId ,
@LocationId ,
@TradeId ,
@MaterialId ,
@ProjectLocationTradeMaterialImageId ,1,@AspNetUsersId,getdate()
)


end

end