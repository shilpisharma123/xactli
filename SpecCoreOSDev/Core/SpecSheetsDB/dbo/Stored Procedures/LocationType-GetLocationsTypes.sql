﻿

CREATE PROCEDURE [dbo].[LocationType-GetLocationsTypes]
AS
SELECT Category
      ,[Name]
      ,[LocationTypeId]
  FROM [dbo].[tblLocationType]
