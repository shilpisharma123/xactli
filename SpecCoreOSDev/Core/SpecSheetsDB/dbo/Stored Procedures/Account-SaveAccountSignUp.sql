﻿CREATE PROCEDURE [dbo].[Account-SaveAccountSignUp] 
	-- Add the parameters for the stored procedure here

@AspNetUsersId nvarchar(50),
@ProjectId int,
@ProjectInviteContactId int = 0

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF (@ProjectInviteContactId > 0)
	BEGIN

		UPDATE [dbo].[tblProjectInvitedContact]
			SET AspNetUsersId = @AspNetUsersId
		
		WHERE ProjectInvitedContactId = @ProjectInviteContactId

		UPDATE tblContact
			SET AspNetUsersId = @AspNetUsersId

		FROM tblContact JOIN tblProjectInvitedContact
			ON tblContact.ContactId = tblProjectInvitedContact.ContactId

		WHERE tblProjectInvitedContact.ProjectInvitedContactId = @ProjectInviteContactId

	END


	INSERT INTO tblProjectAspNetUsers(ProjectId, AspNetUsersId) 
				Values(@ProjectId, @AspNetUsersId)

END