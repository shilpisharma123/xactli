﻿
CREATE PROCEDURE [dbo].[Account-SaveNewUserSignUp] 


@Email nvarchar(150),
@Name nvarchar(150) = NUll,
@CompanyName nvarchar(150) = NUll,
@Phone nvarchar(50) = NUll,
@City nvarchar(50) = NUll,
@State nvarchar(50) = NUll,
@Zip nvarchar(50) = NUll,
@Address nvarchar(150) = NUll,
@Notes nvarchar(MAX) = NULL,
@AspNetUsersId nvarchar(50) = NULL,
@PackageId int = 0

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT NAME FROM tblContact WHERE AspNetUsersId = @AspNetUsersId)
	BEGIN

		INSERT INTO [dbo].[tblContact]
			   ([Name], [CompanyName], [Email], [Phone], [Notes], [City], [State], [Zip], [StreetAddress], AspNetUsersId, PackageId, IsInvited)

		 VALUES
			   (@Name, @CompanyName, @Email, @Phone, @Notes, @City, @State, @Zip, @Address, @AspNetUsersId, @PackageId, 0)

		SELECT SCOPE_IDENTITY()

	END

	ELSE
		BEGIN

			SELECT -1

		END

END