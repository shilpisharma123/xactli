﻿

CREATE PROCEDURE [dbo].[Location-GetLocationById]

@LocationId int,
@AspNetUsersId nvarchar(50)

AS

BEGIN

SELECT [tblProjectLocation].ProjectLocationId, [tblProjectLocation].LocationTypeID, tblLocationType.Name AS LocationTypeName,
		[tblProjectLocation].Name AS ProjectLocationName, [tblProjectLocationTradeMaterial].Quantity, [tblProjectLocationTradeMaterial].UnitOfMeasure,
		[tblProjectLocationTradeMaterial].LowBudget, [tblProjectLocationTradeMaterial].HighBudget, [tblProjectLocationTradeMaterial].Estimate, 
		[tblProjectLocationTradeMaterial].Actual, [tblProjectLocationTradeMaterial].ContactId, tblProjectLocationTrade.ProjectLocationTradeID AS TradeId, 
		tblProjectLocationTrade.TradeName AS TradeName, tblProjectLocationTradeMaterial.ProjectLocationTradeMaterialId AS MaterialId, 
		tblProjectLocationTradeMaterial.MaterialName AS MaterialName, [tblProjectLocationTradeMaterial].ProjectLocationTradeMaterialId,
		[tblProjectLocationTradeMaterial].Cost, tblProjectLocationTradeMaterial.Manufacturer, tblProjectLocationTradeMaterial.Model,
		tblProjectLocationTradeMaterial.Finish, tblProjectLocationTradeMaterial.[Description], tblProjectLocationTradeMaterial.LeadTime,
		tblProjectLocationTradeMaterial.PlanReference,
		
		tblProjectInvitedContact.ProjectId AS InvitedProjectId,
		ISNULL(tblProjectInvitedContact.ProjectLocationTradeId, 0) AS 'ProjectLocationTradeId',
		ISNULL(tblProjectInvitedContact.PermissionLevel, 1) AS 'PermissionLevel',

		(SELECT CASE WHEN tblProject.AspnetUsersId = @AspNetUsersId THEN 1 ELSE 0 END
		FROM tblProject WHERE tblProject.ProjectId = tblProjectLocation.ProjectId) AS IsOwner,

		tblProjectLocation.ProjectId


FROM   tblProjectLocation LEFT JOIN
             tblLocationType ON [tblProjectLocation].LocationTypeID = tblLocationType.LocationTypeId LEFT JOIN
			 tblProjectLocationTrade ON tblProjectLocationTrade.ProjectLocationID = tblProjectLocation.ProjectLocationId LEFT JOIN
             tblProjectLocationTradeMaterial ON tblProjectLocationTrade.ProjectLocationTradeID = [tblProjectLocationTradeMaterial].ProjectLocationTradeID

	LEFT JOIN tblProjectInvitedContact 		
		ON	tblProjectLocation.ProjectId = tblProjectInvitedContact.ProjectId AND tblProjectInvitedContact.AspNetUsersId = @AspNetUsersId

  WHERE [tblProjectLocation].ProjectLocationId = @LocationId
  AND (tblProjectInvitedContact.ProjectLocationTradeId IS NULL OR tblProjectInvitedContact.ProjectLocationTradeId = 0 OR tblProjectLocationTrade.ProjectLocationTradeID = tblProjectInvitedContact.ProjectLocationTradeId)

END