﻿

CREATE PROCEDURE [dbo].[LocationType-GetLocationTypeById]

@LocationTypeId int

AS
SELECT [LocationTypeId]
      ,[Name]
      ,[Category]
  FROM [dbo].[tblLocationType]
  WHERE [LocationTypeId] = @LocationTypeId