﻿
CREATE PROCEDURE [dbo].[Material-GetMaterialById]

@MaterialId int
AS

BEGIN

SELECT [MaterialId]
      ,[Name]
	  ,[LowBudget]
	  ,[HighBudget]
	  ,[UnitOfMeasure]

  FROM [dbo].[tblMaterial]

WHERE MaterialId = @MaterialId


END
