﻿

CREATE PROCEDURE [dbo].[LocationType-SaveLocationType]

@LocationTypeId int,
@Name nvarchar(150),
@Category nvarchar(150) = ''

AS

IF @LocationTypeId = 0

BEGIN

INSERT INTO [dbo].[tblLocationType]
           ([Name]
           ,[Category])
     VALUES
           (@Name
           ,@Category)

END

ELSE

BEGIN

UPDATE [dbo].[tblLocationType]
   SET [Name] = @Name,
      [Category] = @Category

 WHERE LocationTypeId = @LocationTypeId


 END

