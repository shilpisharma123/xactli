CREATE procedure "ProjectTags-DeleteProjectTags"
(
@ProjectId int,
@LocationId int,
@TradeId int,
@MaterialId int,
@ProjectLocationTradeMaterialImageId int,
@AspNetUsersId nvarchar(900)
)
as

begin
--delete tags -ks 12/12/2018
delete from tblProjectTags 
where LocationId=@LocationId and ProjectId=@ProjectId 
and TradeId =@TradeId and MaterialId=@MaterialId and 
ProjectLocationTradeMaterialImageId=@ProjectLocationTradeMaterialImageId
--and AspNetUsersId=@AspNetUsersId
declare @icount int
select @icount=count(*) from tblProjectLocationTradeMaterialImages 
where LocationId=@LocationId and ProjectId=@ProjectId 
and TradeId =@TradeId and MaterialId=@MaterialId and 
ProjectLocationTradeMaterialImagesId=@ProjectLocationTradeMaterialImageId

if(@icount>0)--we update the images table if it matches KS
begin
update tblProjectLocationTradeMaterialImages set LocationId=0 
, TradeId =0 , MaterialId=0 
where LocationId=@LocationId and ProjectId=@ProjectId 
and TradeId =@TradeId and MaterialId=@MaterialId and 
ProjectLocationTradeMaterialImagesId=@ProjectLocationTradeMaterialImageId

end

end