﻿CREATE PROCEDURE [dbo].[Project-SaveProjectMaterial]

@ProjectLocationTradeMaterialId int,
@ProjectLocationTradeId int,
@MaterialId int,
@MaterialName nvarchar(150) = NULL,
@Model nvarchar(150) = '',
@Quantity int = 0,
@UnitOfMeasure nvarchar(50) = '',
@Estimate money = 0,
@Actual money = 0,
@ContactId int = 0,
@LowBudget money = 0,
@HighBudget money = 0,
@Manufacturer nvarchar(150) = '',
@Finish nvarchar(150) = '',
@Cost money = 0,
@LeadTime nvarchar(150) = '',
@PlanReference nvarchar(150) = '',
@Description nvarchar(150) = '',
@HaveNeedBy nvarchar(150) = ''


AS

BEGIN

	IF @MaterialId > 0
	BEGIN
		SELECT @MaterialName = Name FROM dbo.tblMaterial WHERE MaterialId = @MaterialId
	END



IF @ProjectLocationTradeMaterialId > 0
BEGIN

	UPDATE [dbo].[tblProjectLocationTradeMaterial]

		SET 
			[Model] = @Model
           ,[UnitOfMeasure] = @UnitOfMeasure
           ,[Manufacturer] = @Manufacturer
		   ,[Finish] = @Finish
		   ,[LeadTime] = @LeadTime
           ,[PlanReference] = @PlanReference
           ,[Description] = @Description
           ,[HaveNeedBy] = @HaveNeedBy

	WHERE ProjectLocationTradeMaterialId = @ProjectLocationTradeMaterialId

	SELECT @ProjectLocationTradeMaterialId;
	
END

ELSE
	BEGIN

		SET @Estimate = @Quantity * @Cost;

		INSERT INTO [dbo].[tblProjectLocationTradeMaterial]
           ([ProjectLocationTradeID]
           ,[MaterialId]
           ,[MaterialName]
		   ,[Model]
           ,[Quantity]
           ,[UnitOfMeasure]
           ,[Estimate]
           ,[Actual]
           ,[ContactId]
           ,[LowBudget]
           ,[HighBudget]
           ,[Manufacturer]
		   ,[Finish]
		   ,[Cost]
		   ,[LeadTime]
           ,[PlanReference]
           ,[Description]
           ,[HaveNeedBy]
           )
     VALUES
           (@ProjectLocationTradeID
           ,@MaterialId
           ,@MaterialName
		   ,@Model
           ,@Quantity
           ,@UnitOfMeasure
           ,@Estimate
           ,@Actual
           ,@ContactId
           ,@LowBudget
           ,@HighBudget
           ,@Manufacturer
		   ,@Finish
		   ,@Cost
		   ,@LeadTime
           ,@PlanReference
           ,@Description
           ,@HaveNeedBy)

	SELECT SCOPE_IDENTITY();

	END
END