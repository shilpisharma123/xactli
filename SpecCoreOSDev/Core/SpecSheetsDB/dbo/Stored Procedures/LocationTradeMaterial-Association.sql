﻿
CREATE PROCEDURE [dbo].[LocationTradeMaterial-Association]

@LocationTypeId int,
@TradeId int,
@MaterialId int,
@DefaultQuantity numeric(16,2) = 0

AS

BEGIN

INSERT INTO [dbo].[tblLocationTradeMaterial]
           ([LocationTypeId]
           ,[TradeId]
           ,[MaterialId]
           ,DefaultQuantity)
     VALUES
           (@LocationTypeId
           ,@TradeId
           ,@MaterialId
           ,@DefaultQuantity)

	SELECT SCOPE_IDENTITY();

END
