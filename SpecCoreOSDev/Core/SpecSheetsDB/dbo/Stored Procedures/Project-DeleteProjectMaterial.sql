﻿

CREATE PROCEDURE [dbo].[Project-DeleteProjectMaterial]
	
	@ProjectLocationTradeMaterialId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM tblProjectLocationTradeMaterial
	WHERE ProjectLocationTradeMaterialId = @ProjectLocationTradeMaterialId
    
END