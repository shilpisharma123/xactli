﻿

CREATE PROCEDURE "Trade-SaveTrade"

@TradeId int,
@Name nvarchar(150)

AS

IF @TradeId = 0
BEGIN
INSERT INTO [dbo].[tblTrade]
           ([Name])
     VALUES
           (@Name)
SELECT SCOPE_IDENTITY();
END

ELSE

BEGIN

UPDATE [dbo].[tblTrade]
   SET [Name] = @Name

 WHERE TradeId = @TradeId


END