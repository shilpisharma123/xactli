
CREATE Procedure [dbo].[Project-GetNoteHistory]
@Id int
AS
BEGIN
select NoteName,DateTimeStamp,AspNetUsers.UserName from [tblNoteHistory] NoteHistory Inner join
AspNetUsers on NoteHistory.AspNetUsersId=AspNetUsers.Id
where ProjectLocationTradeMaterialImagesId=@Id order by NoteId Desc

END