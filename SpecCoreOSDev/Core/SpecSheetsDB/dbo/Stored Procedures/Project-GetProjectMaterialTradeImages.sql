CREATE PROCEDURE [dbo].[Project-GetProjectMaterialTradeImages] --'9','9','49'
@ProjectId int,
@TradeId int,
@MaterialId int
as
begin

select PLTMImages.ProjectLocationTradeMaterialImagesId,
PLTMImages.ProjectId,
PLTMImages.MaterialId,
PLTMImages.TradeId,
PLTMImages.Image,
PLTMImages.AspNetUsersId,
PLTMImages.ImageOrderSequence,
NoteHistory.NoteName,
NoteHistory.DateTimeStamp,
AspNetUsers.UserName
from [tblProjectLocationTradeMaterialImages] PLTMImages inner join [tblNoteHistory] NoteHistory
on PLTMImages.ProjectLocationTradeMaterialImagesId=NoteHistory.ProjectLocationTradeMaterialImagesId inner join
AspNetUsers on PLTMImages.AspNetUsersId=AspNetUsers.Id
where  ProjectId=@ProjectId
and TradeId=@TradeId
and MaterialId=@MaterialId 

END



--select * from [tblProjectLocationTradeMaterialImages]