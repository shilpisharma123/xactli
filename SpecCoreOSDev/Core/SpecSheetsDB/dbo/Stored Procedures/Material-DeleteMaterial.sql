﻿

CREATE PROCEDURE "Material-DeleteMaterial"

@MaterialId int
AS

DELETE FROM [dbo].[tblMaterial]
      WHERE MaterialId = @MaterialId
