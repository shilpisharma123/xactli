CREATE PROCEDURE [dbo].[Project-DocumentSaveDetails]
@ProjectId int,
@ProjectDocumentId int,
@TradeId int,
@ProjectDocumentType varchar(100)


AS
BEGIN
Update tblProjectDocument SET TradeId= @TradeId, ProjectDocumentType= @ProjectDocumentType Where ProjectDocumentId= @ProjectDocumentId AND ProjectId = @ProjectId 
END
