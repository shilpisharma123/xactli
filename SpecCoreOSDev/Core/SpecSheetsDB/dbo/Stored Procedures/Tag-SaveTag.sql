





CREATE PROCEDURE [dbo].[Tag-SaveTag]
@TagId int,
@TagName nvarchar(150),
@Status bit
AS
IF @TagId = 0
BEGIN
INSERT INTO [dbo].[tblTag]
           ([TagName],[Status])
     VALUES
           (@TagName,@Status)
SELECT SCOPE_IDENTITY();
END
ELSE
BEGIN
UPDATE [dbo].[tblTag]
   SET [TagName] = @TagName  
 WHERE TagId = @TagId
END

