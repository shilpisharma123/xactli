
--KS: We get the the associated tags as per the projectid and the imgae id with status=1  //11/12/2018	
	CREATE PROCEDURE [dbo].[ProjectTags-GetTagsByImageId]
	(
	@ProjectId int,
	@ProjectLocationTradeMaterialImageId int)

AS

BEGIN

	
SELECT [tblProjectTags].TagId,
	[tblProjectTags].ProjectId,
	[tblProjectTags].LocationId
		  ,[tblProjectLocation].Name AS LocationName
		  ,[tblProjectTags].TradeId
		  ,[tblProjectLocationTrade].TradeName AS TradeName
		  ,[tblProjectTags].MaterialId
		  ,[tblProjectLocationTradeMaterial].MaterialName AS MaterialName
		  ,[tblProjectTags].ProjectLocationTradeMaterialImageId
		  ,[tblProjectTags].Status
		  

	  FROM [SpecSheetsDB].[dbo].[tblProjectTags]
		inner join [tblProjectLocation] ON [tblProjectTags].LocationId = [tblProjectLocation] .ProjectLocationId
		inner join [tblProjectLocationTrade] ON [tblProjectTags].TradeId = tblProjectLocationTrade.ProjectLocationTradeId
		inner join [tblProjectLocationTradeMaterial] ON [tblProjectTags].MaterialId = [tblProjectLocationTradeMaterial].ProjectLocationTradeMaterialId

		
	WHERE Status=1 and [tblProjectTags].ProjectLocationTradeMaterialImageId=@ProjectLocationTradeMaterialImageId and [tblProjectTags].ProjectId=@ProjectId

	END



	