﻿

CREATE PROCEDURE [dbo].[Location-UpdateLocationEstimateDetails]

@projectLocationMaterialId int,
@cost float,
@qty float,
@estimate float

AS

BEGIN

	UPDATE tblProjectLocationTradeMaterial
	SET
		Cost = @cost,
		Quantity = @qty,
		Estimate = @estimate

	WHERE ProjectLocationTradeMaterialId = @projectLocationMaterialId

END