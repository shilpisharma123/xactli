﻿


CREATE PROCEDURE [dbo].[PopulateProjectLocationTradeMaterial_sp]
	@CustomerId int,
	@ProjectLocationId int
	
AS
BEGIN
	-- some logic here to check permission to update this project by CustomerID

	SET NOCOUNT ON;

	INSERT INTO [dbo].[tblProjectLocationTrade]
           
           ([TradeName]
		   ,[TradeID]
		   ,[LocationTypeId]
           ,[ProjectLocationID])
     
		 SELECT DISTINCT
		   tblTrade.Name AS TradeName
		  ,tblTrade.TradeId
		  ,[tblLocationTradeMaterial].LocationTypeId
		  ,@ProjectLocationID

  FROM [dbo].[tblLocationTradeMaterial] INNER JOIN tblTrade ON tblLocationTradeMaterial.TradeId = tblTrade.TradeId
	INNER JOIN tblProjectLocation ON tblProjectLocation.ProjectLocationId = @ProjectLocationId
	WHERE
			tblLocationTradeMaterial.LocationTypeId = tblProjectLocation.LocationTypeID

 --where LocationTypeId = (Select LocationTypeId from tblProjectLocation where ProjectLocationId = @ProjectLocationId)


  INSERT INTO [dbo].[tblProjectLocationTradeMaterial]
           ([ProjectLocationTradeID]
		   ,[MaterialId]
		   ,[MaterialName]
           ,[Quantity]
           ,[UnitOfMeasure]
           ,[Estimate]
           ,[Actual]
           ,[ContactId]
           ,[LowBudget]
           ,[HighBudget])

     SELECT
	   [tblProjectLocationTrade].ProjectLocationTradeID
	   ,[tblMaterial].MaterialId
	   ,[tblMaterial].Name
	   ,[tblLocationTradeMaterial].[DefaultQuantity]
	   ,[tblMaterial].[UnitOfMeasure]
	   ,0
	   ,0
	   ,0
      ,[tblMaterial].[LowBudget]
      ,[tblMaterial].[HighBudget]

	FROM [tblProjectLocationTrade] INNER JOIN tblProjectLocation ON tblProjectLocationTrade.ProjectLocationID = tblProjectLocation.ProjectLocationId
	  INNER JOIN [dbo].[tblLocationTradeMaterial] ON tblProjectLocationTrade.TradeId = [tblLocationTradeMaterial].TradeId and tblLocationTradeMaterial.LocationTypeId = tblProjectLocation.LocationTypeId
		INNER JOIN tblMaterial ON tblMaterial.MaterialId = [tblLocationTradeMaterial].MaterialId
		
	WHERE [tblProjectLocationTrade].ProjectLocationID = @ProjectLocationId
		  

 -- FROM [dbo].[tblLocationTradeMaterial] 

 -- JOIN tblProjectLocation ON tblProjectLocation.ProjectLocationId = @ProjectLocationId
 -- JOIN [tblProjectLocationTrade] 
	--ON [tblLocationTradeMaterial].TradeId = tblProjectLocationTrade.TradeId
  --where LocationTypeId = (Select LocationTypeId from tblProjectLocation where ProjectLocationId = @ProjectLocationId)
END

