CREATE PROCEDURE [dbo].[Project-SaveProjectMaterialImages]
@ProjectId int,
@LocationId int,
@TradeId int,
@MaterialId int,
@Image varbinary(Max),
@AspNetUsersId nvarchar(450),
@ImageOrderSequence int
AS
BEGIN
INSERT INTO [dbo].[tblProjectLocationTradeMaterialImages]
          ([ProjectId]
		  ,[LocationId]
		  ,[TradeId]
		  ,[MaterialId]
          ,[Image]		  
          ,[AspNetUsersId],[ImageOrderSequence])
           VALUES
          (@ProjectId,
		   @LocationId,
           @TradeId,
		   @MaterialId,
		   @Image,		  
           @AspNetUsersId,@ImageOrderSequence)
		 SELECT SCOPE_IDENTITY();
END

