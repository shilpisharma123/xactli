﻿

CREATE PROCEDURE "LocationType-GetAllTypes"
AS
SELECT [LocationTypeId]
      ,[Name]
      ,[Category]
  FROM [dbo].[tblLocationType]
