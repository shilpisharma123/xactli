﻿

CREATE PROCEDURE [dbo].[Reports-LocationsByTradeName]
@ProjectId int,
@TradeName nvarchar(150) = ''
AS

BEGIN

SELECT tblProjectlocation.ProjectLocationId, tblProjectlocation.LocationTypeID, tblLocationType.Name AS LocationTypeName, tblProjectlocation.Name AS ProjectLocationName,
		tblProjectLocationTradeMaterial.LowBudget, tblProjectLocationTradeMaterial.HighBudget, tblProjectLocationTradeMaterial.Estimate, tblProjectLocationTradeMaterial.Actual,
		tblProjectLocationTradeMaterial.ContactId, tblProjectLocationTrade.TradeId AS TradeId, tblProjectLocationTrade.TradeName AS TradeName, 
		tblProjectLocationTradeMaterial.MaterialId AS MaterialId, tblProjectLocationTradeMaterial.MaterialName AS MaterialName,
		tblProjectLocationTradeMaterial.Estimate, tblProjectLocationTradeMaterial.Cost, tblProjectLocationTradeMaterial.Quantity,
		tblProjectLocationTradeMaterial.Manufacturer, tblProjectLocationTradeMaterial.Model, tblProjectLocationTradeMaterial.[Description]

FROM tblProject project INNER JOIN tblProjectLocation ON project.ProjectId = tblProjectLocation.ProjectId
			 INNER JOIN tblLocationType ON tblProjectLocation.LocationTypeID = tblLocationType.LocationTypeId
			 INNER JOIN tblProjectLocationTrade ON tblProjectLocation.ProjectLocationId = tblProjectLocationTrade.ProjectLocationID
			 LEFT JOIN tblProjectLocationTradeMaterial ON tblProjectLocationTradeMaterial.ProjectLocationTradeID = tblProjectLocationTrade.ProjectLocationTradeID

  WHERE project.ProjectId = @ProjectId
  AND (@TradeName = 'ALL' OR tblProjectLocationTrade.TradeName = @TradeName)

END