﻿

CREATE PROCEDURE "Trade-DeleteTrade"

@TradeId int

AS

DELETE FROM [dbo].[tblTrade]
      WHERE TradeId = @TradeId
