﻿

CREATE PROCEDURE "Location-GetLocationsByProjectId"
@ProjectId int

AS

BEGIN

SELECT [ProjectLocationId]
      ,[LocationTypeID]
      ,[Name]
	  ,[ProjectId]
  FROM [dbo].[tblProjectLocation]

WHERE ProjectId = @ProjectId

END

