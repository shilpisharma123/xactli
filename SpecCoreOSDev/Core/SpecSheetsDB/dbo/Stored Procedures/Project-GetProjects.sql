﻿

CREATE PROCEDURE [dbo].[Project-GetProjects]
AS

BEGIN
SELECT p.[ProjectId]
      ,[ProjectName]
	  ,[OwnerName]
      ,[Address]
      ,[City]
      ,[State]
      ,[Zip]
      ,[Phone]
      ,[Email]
      ,u.[AspNetUsersId]
  FROM [dbo].[tblProject] p
  LEFT JOIN tblProjectAspNetUsers u ON p.ProjectId = u.ProjectId
END
