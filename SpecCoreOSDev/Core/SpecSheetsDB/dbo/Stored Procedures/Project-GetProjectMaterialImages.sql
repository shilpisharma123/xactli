CREATE PROCEDURE [dbo].[Project-GetProjectMaterialImages] --'9','9','49'
@ProjectId int,
@TradeId int,
@MaterialId int
as
begin
select [tblProjectLocationTradeMaterialImages].* ,AspNetUsers.UserName from
(select max(ProjectLocationTradeMaterialImagesId) as ProjectLocationTradeMaterialImagesId
,ProjectId,MaterialId,TradeId,Image
 from [dbo].[tblProjectLocationTradeMaterialImages]  where  ProjectId=@ProjectId
and TradeId=@TradeId
and MaterialId=@MaterialId 
group by 
ProjectId,MaterialId,TradeId,Image) as AImages
inner join [tblProjectLocationTradeMaterialImages]  on [tblProjectLocationTradeMaterialImages].ProjectLocationTradeMaterialImagesId=AImages.ProjectLocationTradeMaterialImagesId
inner join 
AspNetUsers on tblProjectLocationTradeMaterialImages.AspNetUsersId=AspNetUsers.Id 
order by [DateTimeStamp]
END