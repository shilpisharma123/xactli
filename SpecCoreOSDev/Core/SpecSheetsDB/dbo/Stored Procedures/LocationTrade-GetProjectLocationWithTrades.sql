﻿

CREATE PROCEDURE [dbo].[LocationTrade-GetProjectLocationWithTrades]
@ProjectLocationId int,
@TradeId int
AS

BEGIN

SELECT tblProjectLocation.Name AS LocationName, tblTrade.Name AS TradeName, 
		tblProjectLocationTradeMaterial.MaterialId, tblProjectLocationTradeMaterial.MaterialName As MaterialName, 
		tblProjectLocation.LocationTypeID, tblLocationType.Name AS LocationTypeName

FROM   tblProjectLocationTradeMaterial
			INNER JOIN tblProjectLocationTrade ON tblProjectLocationTradeMaterial.ProjectLocationTradeID = tblProjectLocationTrade.ProjectLocationId
			INNER JOIN tblProjectLocation ON tblProjectLocationTrade.ProjectLocationId = tblProjectLocation.ProjectLocationId
			INNER JOIN tblLocationType ON tblLocationType.LocationTypeId = tblProjectLocation.LocationTypeID
			INNER JOIN tblTrade ON tblProjectLocationTrade.TradeId = tblTrade.TradeId
			INNER JOIN tblMaterial ON tblMaterial.MaterialId = tblProjectLocationTradeMaterial.MaterialId

WHERE tblProjectLocation.ProjectLocationId = @ProjectLocationId
AND tblProjectLocationTrade.TradeId = @TradeId


END