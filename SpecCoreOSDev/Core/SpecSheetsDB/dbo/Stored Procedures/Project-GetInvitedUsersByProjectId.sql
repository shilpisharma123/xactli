﻿CREATE PROCEDURE [dbo].[Project-GetInvitedUsersByProjectId]

@ProjectId int

AS

BEGIN

	SELECT [ProjectInvitedContactId], [ProjectId], [tblProjectInvitedContact].[ProjectLocationTradeId], [tblProjectInvitedContact].[ContactId], 
	[tblContact].[Name] AS [ContactName], [InviteStatus], [PermissionLevel], [DateInvited], [DateReceived], ISNULL(tplt.TradeName, 'All') AS [TradeName],
	[tblContact].[Email] AS Email

	FROM tblProjectInvitedContact 
		JOIN tblContact
			ON tblProjectInvitedContact.ContactId = tblContact.ContactId
		LEFT JOIN tblProjectLocationTrade tplt
			ON tblProjectInvitedContact.ProjectLocationTradeId = tplt.ProjectLocationTradeId

	WHERE ProjectId = @ProjectId

END