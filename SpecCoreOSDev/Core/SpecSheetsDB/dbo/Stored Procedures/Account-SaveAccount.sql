﻿CREATE PROCEDURE [dbo].[Account-SaveAccount] 
	-- Add the parameters for the stored procedure here

@Email nvarchar(150) = NUll,
@Name nvarchar(150) = NUll,
@CompanyName nvarchar(150) = NUll,
@Phone nvarchar(50) = NUll,
@City nvarchar(50) = NUll,
@State nvarchar(50) = NUll,
@Zip nvarchar(50) = NUll,
@Address nvarchar(150) = NUll,
@Notes nvarchar(MAX) = NULL,
@AspNetUsersId nvarchar(50) = NULL

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT NAME FROM tblContact WHERE AspNetUsersId = @AspNetUsersId)
	BEGIN

		INSERT INTO [dbo].[tblContact]
			   ([Name], [Email], [Phone], [Notes], [City], [State], [Zip], [StreetAddress], AspNetUsersId)

		 VALUES
			   (@Name, @Email, @Phone, @Notes, @City, @State, @Zip, @Address, @AspNetUsersId)

		SELECT SCOPE_IDENTITY()

	END

	ELSE
		BEGIN

			UPDATE [dbo].[tblContact]
			   SET [Name] = @Name
			      ,[CompanyName] = @CompanyName
				  ,[Email] = @Email
				  ,[Phone] = @Phone
				  ,[Notes] = @Notes
				  ,[City] = @City
				  ,[State] = @State
				  ,[Zip] = @Zip
				  ,[StreetAddress] = @Address
			 
			 WHERE [AspNetUsersId] = @AspNetUsersId

		END

END