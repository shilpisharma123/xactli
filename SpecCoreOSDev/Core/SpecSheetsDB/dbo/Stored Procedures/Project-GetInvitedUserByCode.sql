﻿
CREATE PROCEDURE [dbo].[Project-GetInvitedUserByCode]

@InviteCode uniqueidentifier

AS

BEGIN

	SELECT [ProjectInvitedContactId], [ProjectId], [tblProjectInvitedContact].[ProjectLocationTradeId], [tblProjectInvitedContact].[ContactId], 
	[tblContact].[Name] AS [ContactName], [InviteStatus], [PermissionLevel], [DateInvited], [DateReceived], ISNULL(tplt.TradeName, 'All') AS [TradeName],
	[tblContact].[Email] AS Email, tblContact.AspNetUsersId

	FROM tblProjectInvitedContact 
		JOIN tblContact
			ON tblProjectInvitedContact.ContactId = tblContact.ContactId
		LEFT JOIN tblProjectLocationTrade tplt
			ON tblProjectInvitedContact.ProjectLocationTradeId = tplt.ProjectLocationTradeId

	WHERE InviteCode = @InviteCode AND IsInviteActive = 1


	UPDATE tblProjectInvitedContact
	SET IsInviteActive = 0,
		InviteStatus = 2,
		DateReceived = GETDATE()

	WHERE InviteCode = @InviteCode


END