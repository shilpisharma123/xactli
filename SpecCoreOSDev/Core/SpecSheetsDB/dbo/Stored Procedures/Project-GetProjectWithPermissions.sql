﻿

CREATE PROCEDURE [dbo].[Project-GetProjectWithPermissions]

@ProjectId int,
@AspNetUsersId nvarchar(50)

AS

BEGIN

SELECT tblProject.[ProjectId]
      ,[ProjectName]
	  ,[OwnerName]
      ,[Address]
      ,[City]
      ,[State]
      ,[Zip]
      ,[Phone]
      ,[Email]
      ,tblProject.[AspNetUsersId]
	  ,tblProjectInvitedContact.ProjectId AS InvitedProjectId
	  ,tblProjectInvitedContact.ProjectLocationTradeId
	  ,tblProjectInvitedContact.PermissionLevel

  FROM [dbo].[tblProject] 
	LEFT JOIN tblProjectInvitedContact 
		ON tblProject.ProjectId = tblProjectInvitedContact.ProjectId AND tblProjectInvitedContact.AspNetUsersId = @AspNetUsersId

WHERE tblProject.ProjectId = @ProjectId
END