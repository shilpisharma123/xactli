

CREATE PROCEDURE [dbo].[TagLocationTradeMaterial-GetTag]

AS

BEGIN

	SELECT [TagId]
		  ,[tblLocationType].Name AS LocationName
		  ,[tblTrade].Name AS TradeName
		  ,[tblMaterial].Name AS MaterialName
		  ,[tblTag].TagName

	  FROM [SpecSheetsDB].[dbo].[tblTag]
		JOIN [tblLocationType] ON [tblTag].LocationId = [tblLocationType].LocationTypeId
		JOIN [tblTrade] ON [tblTag].TradeId = [tblTrade].TradeId
		JOIN [tblMaterial] ON [tblTag].MaterialId = [tblMaterial].MaterialId

	WHERE Status=1

END

