﻿
CREATE PROCEDURE [dbo].[LocationTradeMaterial-GetAssociationById]

@AssociationId int
AS

BEGIN

	SELECT [LocationTradeMaterialId]
		  ,[tblLocationType].Name AS LocationName
		  ,[tblTrade].Name AS TradeName
		  ,[tblMaterial].Name AS MaterialName
		  ,[tblLocationTradeMaterial].DefaultQuantity

	  FROM [SpecSheetsDB].[dbo].[tblLocationTradeMaterial]
		JOIN [tblLocationType] ON [tblLocationTradeMaterial].LocationTypeId = [tblLocationType].LocationTypeId
		JOIN [tblTrade] ON [tblLocationTradeMaterial].TradeId = [tblTrade].TradeId
		JOIN [tblMaterial] ON [tblLocationTradeMaterial].MaterialId = [tblMaterial].MaterialId

	WHERE [tblLocationTradeMaterial].LocationTradeMaterialId = @AssociationId

END
