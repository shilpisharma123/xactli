﻿CREATE PROCEDURE "Reports-GetProjectDeNormalized"

@ProjectId INT

AS

BEGIN

SELECT        tblProject.ProjectId, tblProject.ProjectName, tblProject.OwnerName, tblProject.Address, tblProject.City, tblProject.State, tblProject.Zip, tblProject.Phone, tblProject.Email, tblProjectLocation.ProjectLocationId, 
                         tblProjectLocation.LocationTypeID, tblProjectLocation.Name AS LocationName, 
						 tblLocationType.Name AS LocationTypeName, tblLocationType.Category,
						 tblProjectLocationTrade.ProjectLocationTradeID, tblProjectLocationTrade.TradeName, 
                         tblProjectLocationTrade.TradeId, tblProjectLocationTradeMaterial.ProjectLocationTradeMaterialId, 
                         tblProjectLocationTradeMaterial.MaterialId, tblProjectLocationTradeMaterial.MaterialName, tblProjectLocationTradeMaterial.Model, 
                         tblProjectLocationTradeMaterial.LowBudget, tblProjectLocationTradeMaterial.HighBudget, tblProjectLocationTradeMaterial.Estimate, tblProjectLocationTradeMaterial.Actual, 
                         tblProjectLocationTradeMaterial.ContactId, tblProjectLocationTradeMaterial.UnitOfMeasure, tblProjectLocationTradeMaterial.Quantity, tblProjectLocationTradeMaterial.Finish, tblProjectLocationTradeMaterial.Cost, 
                         tblProjectLocationTradeMaterial.LeadTime, tblProjectLocationTradeMaterial.Manufacturer, tblProjectLocationTradeMaterial.PlanReference, tblProjectLocationTradeMaterial.Description, 
                         tblProjectLocationTradeMaterial.HaveNeedBy
FROM            tblProject INNER JOIN
                         tblProjectLocation ON tblProjectLocation.ProjectId = tblProject.ProjectId INNER JOIN
                         tblLocationType ON tblLocationType.LocationTypeId = tblProjectLocation.LocationTypeID INNER JOIN
                         tblProjectLocationTrade ON tblProjectLocationTrade.ProjectLocationID = tblProjectLocation.ProjectLocationId INNER JOIN
                         tblProjectLocationTradeMaterial ON tblProjectLocationTradeMaterial.ProjectLocationTradeId = tblProjectLocationTrade.ProjectLocationTradeID

WHERE tblProject.ProjectId = @ProjectId


END