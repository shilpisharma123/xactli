﻿

CREATE PROCEDURE [dbo].[Project-GetProjectLocationsWithPermissions]

@ProjectId int,
@AspNetUsersId nvarchar(50)

AS

BEGIN

SELECT [tblProjectLocation].[ProjectLocationId]
      ,[tblProjectLocation].[Name] AS ProjectLocationName
	  ,[tblProjectLocation].[ProjectId]

	  ,tblLocationType.[LocationTypeID]
	  ,tblLocationType.[Name] AS LocationTypeName
	  ,tblLocationType.Category AS LocationTypeCategory

	  ,tblProjectInvitedContact.ProjectId AS InvitedProjectId
	  ,tblProjectInvitedContact.ProjectLocationTradeId
	  ,tblProjectInvitedContact.PermissionLevel

  FROM [dbo].[tblProjectLocation]
  
	JOIN tblProject ON tblProject.ProjectId = tblProjectLocation.ProjectId
	JOIN tblLocationType ON tblProjectLocation.LocationTypeID = tblLocationType.LocationTypeId
	--JOIN tblProjectLocationTrade ON tblProjectLocationTrade.ProjectLocationID = tblProjectLocation.ProjectLocationId

	LEFT JOIN tblProjectInvitedContact 
		ON [tblProjectLocation].ProjectId = tblProjectInvitedContact.ProjectId AND tblProjectInvitedContact.AspNetUsersId = @AspNetUsersId


WHERE [tblProjectLocation].ProjectId = @ProjectId 
AND (tblProject.AspNetUsersId = @AspNetUsersId OR 
	((tblProjectInvitedContact.ProjectLocationTradeId = 0 
		OR tblProjectInvitedContact.ProjectLocationTradeId IN 
			(SELECT lt.ProjectLocationTradeID 
				FROM tblProjectLocation pl JOIN tblProjectLocationTrade lt ON pl.ProjectLocationId = lt.ProjectLocationID 
				WHERE pl.ProjectLocationId = tblProjectLocation.ProjectLocationId
			))))
			
END