﻿

CREATE PROCEDURE "LocationTradeMaterial-DeleteAssociation"

@LocationTradeMaterialId int

AS

DELETE FROM [dbo].[tblLocationTradeMaterial]
      WHERE LocationTradeMaterialId = @LocationTradeMaterialId

