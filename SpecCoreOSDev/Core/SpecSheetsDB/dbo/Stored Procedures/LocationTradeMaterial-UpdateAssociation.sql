﻿
CREATE PROCEDURE [dbo].[LocationTradeMaterial-UpdateAssociation]

@AssociationId int,
@DefaultQuantity int

AS

UPDATE [dbo].[tblLocationTradeMaterial]
   SET DefaultQuantity = @DefaultQuantity
WHERE LocationTradeMaterialId = @AssociationId