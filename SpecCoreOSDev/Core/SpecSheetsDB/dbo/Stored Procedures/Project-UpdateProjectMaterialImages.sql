
CREATE PROCEDURE [dbo].[Project-UpdateProjectMaterialImages]
@ProjectId int,
@TradeId int,
@MaterialId int,
@Image varbinary(MAX),
@DateTimeStamp datetime,
@Notes nvarchar(550),
@AspNetUsersId nvarchar(450)

as
begin


UPDATE [dbo].[tblProjectLocationTra
deMaterialImages]
         SET  ProjectId=@ProjectId
          ,TradeId=@TradeId
           ,MaterialId=@MaterialId
           ,[Image]=@Image
           ,DateTimeStamp=@DateTimeStamp
          ,[Notes]=@Notes
          ,[AspNetUsersId]=@AspNetUsersId

  
        
end
