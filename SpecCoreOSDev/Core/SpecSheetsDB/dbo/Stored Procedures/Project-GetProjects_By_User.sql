﻿

CREATE PROCEDURE [dbo].[Project-GetProjects_By_User]

@AspNetUsersId nvarchar(450)

AS

BEGIN


SELECT p.[ProjectId]
      ,[ProjectName]
	  ,[OwnerName]
      ,[Address]
      ,[City]
      ,[State]
      ,[Zip]
      ,[Phone]
      ,[Email]
      ,p.[AspNetUsersId]
	  ,1 AS IsOwner --(CASE WHEN u.[AspNetUsersId] = @AspNetUsersId THEN 1 ELSE 0 END) AS IsOwner

  FROM [dbo].[tblProject] p

	WHERE p.ProjectId IN (SELECT u.ProjectId FROM tblProjectAspNetUsers u WHERE u.AspNetUsersId = @AspNetUsersId)

END