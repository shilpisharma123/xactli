﻿CREATE PROCEDURE [dbo].[Project-DeleteTradeMaterialImage]
	@Id int
AS
	DELETE FROM [dbo].[tblProjectTradeMaterialImages]
	WHERE Id = @Id