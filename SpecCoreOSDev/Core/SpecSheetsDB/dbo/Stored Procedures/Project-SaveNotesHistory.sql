


CREATE PROCEDURE [dbo].[Project-SaveNotesHistory]
@ProjectLocationTradeMaterialImagesId int,
@NoteName [nvarchar](MAX),
@DateTimeStamp datetime,
@AspNetUsersId [nvarchar](450)
AS
BEGIN
INSERT INTO [tblNoteHistory]
          ([ProjectLocationTradeMaterialImagesId]
		  ,[NoteName]
		  ,[DateTimeStamp]
		  ,[AspNetUsersId]		  
		  )
           VALUES
          (@ProjectLocationTradeMaterialImagesId,
           @NoteName,
		   @DateTimeStamp,		   			
           @AspNetUsersId
		  )
END


