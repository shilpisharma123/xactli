﻿CREATE PROCEDURE [dbo].[Project-SaveProjectTrade]

@TradeId int, 
@TradeName nvarchar(150) = NULL,
@ProjectLocationId int

AS

BEGIN

IF @TradeId > 0
BEGIN
	SELECT @TradeName = Name FROM dbo.tblTrade WHERE TradeId = @TradeId
END

DECLARE @LocationTypeId int;
SELECT @LocationTypeId = LocationTypeID FROM tblProjectLocation WHERE ProjectLocationId = @ProjectLocationId

	
	INSERT INTO [dbo].[tblProjectLocationTrade]
           
           ([TradeName]
		   ,[TradeID]
		   ,[LocationTypeId]
           ,[ProjectLocationID])
     
		 SELECT
		   @TradeName
		  ,@TradeId
		  ,@LocationTypeId
		  ,@ProjectLocationID

	SELECT SCOPE_IDENTITY();

END