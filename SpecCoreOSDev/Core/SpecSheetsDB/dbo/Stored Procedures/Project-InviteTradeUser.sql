﻿CREATE PROCEDURE [dbo].[Project-InviteTradeUser]

@InviteCode uniqueidentifier,
@UserName nvarchar(150), 
@Email nvarchar(150),
@ProjectId int,
@TradeId int = 0,
@PermissionId int = 1

AS

BEGIN

DECLARE @ContactId int;
DECLARE @InviteStatus int;
DECLARE @AspNetUsersId nvarchar(50);

--Permission levels are: 

--1- read only
--2- edit
--3- update trade only.


--Invite Status is: 

--1- invite has been sent
--2- has been accepted

SET @InviteStatus = 1;


IF EXISTS (SELECT Email FROM tblContact WHERE Email = @Email)
BEGIN
	SELECT @ContactId = ContactId, @AspNetUsersId = AspNetUsersId FROM tblContact WHERE Email = @Email
END

ELSE

BEGIN
	INSERT INTO [dbo].[tblContact] ( [Name], [Email], IsInvited)
	VALUES (@UserName, @Email, 1)

	SET @ContactId = SCOPE_IDENTITY();
END


-- UserID will be updated when user will be signed in.
INSERT INTO [dbo].[tblProjectInvitedContact]
        ([ProjectId], [ProjectLocationTradeId], [ContactId], [InviteStatus], [PermissionLevel], [DateInvited], [DateReceived], [InviteCode], [IsInviteActive], AspNetUsersId)
    VALUES
        (@ProjectId, @TradeId, @ContactId, @InviteStatus, @PermissionId, GetDate(), NULL, @InviteCode, 1, @AspNetUsersId)

SELECT SCOPE_IDENTITY();

END