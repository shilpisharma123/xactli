﻿CREATE PROCEDURE [dbo].[Account-GetAccountInformation] 
	
@AspNetUsersId nvarchar(50)

AS
BEGIN

SELECT [ContactId]
      ,[Name]
      ,[Email]
      ,[Phone]
      ,[Notes]
      ,[City]
      ,[State]
      ,[Zip]
      ,[StreetAddress]
      ,[AspNetUsersId]
  FROM [dbo].[tblContact]

  WHERE [AspNetUsersId] = @AspNetUsersId

END