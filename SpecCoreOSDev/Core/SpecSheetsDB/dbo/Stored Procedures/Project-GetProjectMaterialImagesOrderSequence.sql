CREATE Procedure [dbo].[Project-GetProjectMaterialImagesOrderSequence]
@ProjectId int,
@TradeId int,
@MaterialId int,
@AspNetUsersId nvarchar(450)
AS 
Begin
Select Max(ImageOrderSequence) AS ImageOrderSequence
from [dbo].[tblProjectLocationTradeMaterialImages] where 
ProjectId=@ProjectId and
TradeId=@TradeId and
MaterialId=@MaterialId and
AspNetUsersId=@AspNetUsersId
END
