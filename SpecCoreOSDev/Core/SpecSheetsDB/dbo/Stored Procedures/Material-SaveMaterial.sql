﻿

CREATE PROCEDURE [dbo].[Material-SaveMaterial]

@MaterialId int,
@Name nvarchar(150),
@HighBudget money = 0,
@LowBudget money = 0,
@UnitOfMeasure nvarchar(20) = ''

AS

BEGIN

IF @MaterialId = 0

	BEGIN

		INSERT INTO [dbo].[tblMaterial]
				   ([Name], HighBudget, LowBudget, UnitOfMeasure)
			 VALUES
				   (@Name, @HighBudget, @LowBudget, @UnitOfMeasure)

		SELECT SCOPE_IDENTITY();

	END
ELSE

	BEGIN

		UPDATE [dbo].[tblMaterial]
		   SET [Name] = @Name,
		   HighBudget = @HighBudget,
		   LowBudget = @LowBudget,
		   UnitOfMeasure = @UnitOfMeasure

		 WHERE MaterialId = @MaterialId

		 SELECT @MaterialId

	END

END

