﻿
CREATE PROCEDURE [dbo].[Project-GetInvitedUserById]

@ProjectInvitedContactId int

AS

BEGIN

	
	UPDATE tblProjectInvitedContact
	SET IsInviteActive = 1,
		DateInvited = GETDATE()

	WHERE ProjectInvitedContactId = @ProjectInvitedContactId



	SELECT [ProjectInvitedContactId], [ProjectId], [tblProjectInvitedContact].[ProjectLocationTradeId], [tblProjectInvitedContact].[ContactId], 
	[tblContact].[Name] AS [ContactName], [InviteStatus], [PermissionLevel], [DateInvited], [DateReceived], ISNULL(tplt.TradeName, 'All') AS [TradeName],
	[tblContact].[Email] AS Email, [tblProjectInvitedContact].AspNetUsersId, InviteCode

	FROM tblProjectInvitedContact 
		JOIN tblContact
			ON tblProjectInvitedContact.ContactId = tblContact.ContactId
		LEFT JOIN tblProjectLocationTrade tplt
			ON tblProjectInvitedContact.ProjectLocationTradeId = tplt.ProjectLocationTradeId

	WHERE ProjectInvitedContactId = @ProjectInvitedContactId

END