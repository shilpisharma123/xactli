﻿

CREATE PROCEDURE [dbo].[Location-GetLocationWithTradesAndMaterialsbyProjectId]
@ProjectId int
AS

BEGIN

SELECT tblProjectlocation.ProjectLocationId, tblProjectlocation.LocationTypeID, tblLocationType.Name AS LocationTypeName, tblProjectlocation.Name AS ProjectLocationName,
		tblProjectLocationTradeMaterial.LowBudget, tblProjectLocationTradeMaterial.HighBudget, tblProjectLocationTradeMaterial.Estimate,
		tblProjectLocationTradeMaterial.Actual, tblProjectLocationTradeMaterial.ContactId, tblProjectLocationTrade.TradeId AS TradeId, 
		tblProjectLocationTrade.TradeName AS TradeName, tblProjectLocationTradeMaterial.MaterialId AS MaterialId, 
		tblProjectLocationTradeMaterial.MaterialName AS MaterialName, tblProjectLocationTradeMaterial.Cost, 
		tblProjectLocationTradeMaterial.Quantity, tblProjectLocationTradeMaterial.Manufacturer, tblProjectLocationTradeMaterial.Model, 
		tblProjectLocationTradeMaterial.Finish, tblProjectLocationTradeMaterial.[Description], tblProjectLocationTradeMaterial.LeadTime,
		tblProjectLocationTradeMaterial.PlanReference

FROM tblProject project INNER JOIN tblProjectLocation ON project.ProjectId = tblProjectLocation.ProjectId
			 INNER JOIN tblLocationType ON tblProjectLocation.LocationTypeID = tblLocationType.LocationTypeId
			 LEFT JOIN tblProjectLocationTrade ON tblProjectLocation.ProjectLocationId = tblProjectLocationTrade.ProjectLocationID
			 LEFT JOIN tblProjectLocationTradeMaterial ON tblProjectLocationTradeMaterial.ProjectLocationTradeID = tblProjectLocationTrade.ProjectLocationTradeID

  WHERE project.ProjectId = @ProjectId

END