﻿


CREATE PROCEDURE [dbo].[Project-DeleteProject]

@ProjectId int,
@AspNetUsersId nvarchar(50)

AS
BEGIN

IF EXISTS (SELECT tblProject.ProjectId FROM tblProject 
				WHERE tblProject.ProjectId = @ProjectId AND tblProject.AspNetUsersId = @AspNetUsersId) 

BEGIN

	DELETE FROM tblProjectLocationTradeMaterial
	WHERE ProjectLocationTradeId IN 
		(SELECT ProjectLocationTradeId 
			FROM tblProjectLocationTrade 
				WHERE ProjectLocationId IN 
					(Select ProjectLocationId 
						FROM tblProjectLocation WHERE ProjectId = @ProjectId))

	DELETE FROM tblProjectLocationTrade
	WHERE ProjectLocationId IN 
		(Select ProjectLocationId 
			FROM tblProjectLocation WHERE ProjectId = @ProjectId)

	DELETE FROM tblProjectLocation
	WHERE ProjectId = @ProjectId

	DELETE FROM tblProject
	WHERE ProjectId = @ProjectId

END

ELSE

BEGIN

	RAISERROR ('Access On Project Denied', 11,1)

END 




END