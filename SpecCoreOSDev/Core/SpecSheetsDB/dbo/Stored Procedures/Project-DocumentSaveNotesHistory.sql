


CREATE PROCEDURE [dbo].[Project-DocumentSaveNotesHistory]
@ProjectDocumentId int,
@NoteName [nvarchar](MAX),
@DateTimeStamp datetime,
@AspNetUsersId [nvarchar](450)
AS
BEGIN
INSERT INTO [tblNoteHistory]
          ([ProjectDocumentId]
		  ,[NoteName]
		  ,[DateTimeStamp]
		  ,[AspNetUsersId]		  
		  )
           VALUES
          (@ProjectDocumentId,
           @NoteName,
		   @DateTimeStamp,		   			
           @AspNetUsersId
		  )
END
