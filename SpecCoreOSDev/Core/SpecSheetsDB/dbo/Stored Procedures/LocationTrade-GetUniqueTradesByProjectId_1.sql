﻿

CREATE PROCEDURE [dbo].[LocationTrade-GetUniqueTradesByProjectId]
@ProjectId int
AS

BEGIN

SELECT DISTINCT plt.TradeName AS TradeName

FROM   tblProjectLocation AS location INNER JOIN
             tblProjectLocationTrade AS plt ON location.ProjectLocationId = plt.ProjectLocationID
             
			 
WHERE location.ProjectId = @ProjectId

END