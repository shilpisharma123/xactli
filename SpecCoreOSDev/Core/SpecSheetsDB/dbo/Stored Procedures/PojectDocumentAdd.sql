CREATE PROCEDURE [dbo].[PojectDocumentAdd]
@ProjectId int,
@TradeId int,
@ProjectDocumentType varchar(100),
@Document varbinary(Max),
@DateTimeStamp datetime,
@AspNetUsersId nvarchar(900),
@FileType varchar(100), 
@DocumentName varchar(Max)
AS
BEGIN
INSERT INTO [dbo].[tblProjectDocument]
          ([ProjectId]
		  ,[TradeId]
		  ,[ProjectDocumentType]
		  ,[DateTimeStamp]
          ,[Document]	
		  ,[FileType]			  
          ,[AspNetUsersId]
		  ,[DocumentName])

           VALUES
          (@ProjectId,
           @TradeId,
		   @ProjectDocumentType,
		   @DateTimeStamp,
		   @Document,
		   @FileType,		  
           @AspNetUsersId,
		   @DocumentName)
		 SELECT SCOPE_IDENTITY();
END