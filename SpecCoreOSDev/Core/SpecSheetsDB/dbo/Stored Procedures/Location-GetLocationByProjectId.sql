


CREATE PROCEDURE [dbo].[Location-GetLocationByProjectId] --'9','3ebf1046-190a-48da-bb1b-f2aa6816e933'

@LocationId int,
@AspNetUsersId nvarchar(50)

AS

BEGIN

SELECT Distinct [tblProjectLocation].ProjectLocationId, [tblProjectLocation].Name AS ProjectLocationName


FROM   tblProjectLocation LEFT JOIN
             tblLocationType ON [tblProjectLocation].LocationTypeID = tblLocationType.LocationTypeId LEFT JOIN
			 tblProjectLocationTrade ON tblProjectLocationTrade.ProjectLocationID = tblProjectLocation.ProjectLocationId LEFT JOIN
             tblProjectLocationTradeMaterial ON tblProjectLocationTrade.ProjectLocationTradeID = [tblProjectLocationTradeMaterial].ProjectLocationTradeID

	LEFT JOIN tblProjectInvitedContact 		
		ON	tblProjectLocation.ProjectId = tblProjectInvitedContact.ProjectId AND tblProjectInvitedContact.AspNetUsersId = @AspNetUsersId

  WHERE [tblProjectLocation].ProjectLocationId = @LocationId
  AND (tblProjectInvitedContact.ProjectLocationTradeId IS NULL OR tblProjectInvitedContact.ProjectLocationTradeId = 0 OR tblProjectLocationTrade.ProjectLocationTradeID = tblProjectInvitedContact.ProjectLocationTradeId)

END

