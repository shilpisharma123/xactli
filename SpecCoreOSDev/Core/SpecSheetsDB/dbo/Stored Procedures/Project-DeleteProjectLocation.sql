﻿


CREATE PROCEDURE [Project-DeleteProjectLocation]

@ProjectLocationId int,
@AspNetUsersId nvarchar(50)

AS
BEGIN

IF EXISTS (SELECT tblProject.ProjectId FROM tblProject 
			JOIN tblProjectLocation ON tblProject.ProjectId = tblProjectLocation.ProjectId 
				WHERE tblProjectLocation.ProjectLocationId = @ProjectLocationId AND tblProject.AspNetUsersId = @AspNetUsersId) 

BEGIN

	DELETE FROM tblProjectLocationTradeMaterial
	WHERE ProjectLocationTradeId IN 
		(SELECT ProjectLocationTradeId 
			FROM tblProjectLocationTrade 
				WHERE ProjectLocationId IN 
					(Select ProjectLocationId 
						FROM tblProjectLocation WHERE ProjectLocationId = @ProjectLocationId))

	DELETE FROM tblProjectLocationTrade
	WHERE ProjectLocationId IN 
		(Select ProjectLocationId 
			FROM tblProjectLocation WHERE ProjectLocationId = @ProjectLocationId)

	DELETE FROM tblProjectLocation
	WHERE ProjectLocationId = @ProjectLocationId

END

ELSE

BEGIN

	RAISERROR ('Access On Project Denied', 11,1)

END 


END