

CREATE PROCEDURE [dbo].[TagLocationTradeMaterial-GetTagById]

@tagId int
AS

BEGIN

	SELECT [TagId]
		  ,[tblLocationType].Name AS LocationName
		  ,[tblTrade].Name AS TradeName
		  ,[tblMaterial].Name AS MaterialName
		  ,[tblTag].TagName

	  FROM [SpecSheetsDB].[dbo].[tblTag]
		JOIN [tblLocationType] ON [tblTag].LocationId = [tblLocationType].LocationTypeId
		JOIN [tblTrade] ON [tblTag].TradeId = [tblTrade].TradeId
		JOIN [tblMaterial] ON [tblTag].MaterialId = [tblMaterial].MaterialId

	WHERE [tblTag].TagId = @tagId and Status=1

END

