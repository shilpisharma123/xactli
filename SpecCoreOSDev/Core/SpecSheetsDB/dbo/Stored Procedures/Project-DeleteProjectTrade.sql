﻿

CREATE PROCEDURE [dbo].[Project-DeleteProjectTrade]
	
	@ProjectLocationTradeId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM tblProjectLocationTrade
	WHERE ProjectLocationTradeID = @ProjectLocationTradeId

	DELETE FROM tblProjectLocationTradeMaterial
	WHERE ProjectLocationTradeID = @ProjectLocationTradeId
    
END