﻿


CREATE PROCEDURE [dbo].[LocationType-DeleteLocationType]

@LocationTypeId int
AS

DELETE FROM [dbo].[tblLocationType]
      WHERE LocationTypeId = @LocationTypeId
