

CREATE PROCEDURE [dbo].[TagLocationTradeMaterial-UpdateTag]
@TagId int,
@TagName nvarchar(150)
AS
UPDATE [dbo].[tblTag]
   SET TagName = @TagName
WHERE TagId = @TagId

