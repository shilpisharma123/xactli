﻿

CREATE PROCEDURE [dbo].[Project-GetProjects_By_User_Access]

@AspNetUsersId nvarchar(450)

AS

BEGIN

SELECT * FROM tblProject WHERE AspNEtUsersId = @AspNetUsersId

UNION

SELECT * FROM tblProject 
WHERE tblProject.ProjectId 

IN (Select ProjectId FROM tblProjectInvitedContact tpic 
	WHERE tpic.AspNetUsersId = @AspNetUsersId AND tpic.InviteStatus = 2 AND tpic.ProjectLocationTradeId = 0)


END