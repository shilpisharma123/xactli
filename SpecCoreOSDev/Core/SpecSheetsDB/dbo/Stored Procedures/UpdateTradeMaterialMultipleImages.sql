CREATE PROCEDURE [dbo].[UpdateTradeMaterialMultipleImages]
@ProjectLocationTradeMaterialImagesId int, 
@ProjectId int,
@LocationId int,
@TradeId int,
@MaterialId int,
@AspNetUsersId nvarchar(450),
@ImageOrderSequence int
AS
BEGIN
UPDATE [dbo].[tblProjectLocationTradeMaterialImages]
          SET [TradeId]=@TradeId,
		  [MaterialId]=	@MaterialId,
		  [LocationId]= @LocationId,
		  [ImageOrderSequence]=@ImageOrderSequence	  
          WHERE 
		  [ProjectLocationTradeMaterialImagesId]=@ProjectLocationTradeMaterialImagesId AND
		  [ProjectId]= @ProjectId AND [AspNetUsersId]= @AspNetUsersId
END
