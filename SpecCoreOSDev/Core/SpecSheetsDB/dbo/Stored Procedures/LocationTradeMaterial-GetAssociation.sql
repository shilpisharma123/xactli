﻿
CREATE PROCEDURE [dbo].[LocationTradeMaterial-GetAssociation]
AS

BEGIN

SELECT [LocationTradeMaterialId]
      ,location.Name AS LocationName
      ,trade.Name AS TradeName
      ,material.Name AS MaterialName
	  ,association.[DefaultQuantity]

  FROM [SpecSheetsDB].[dbo].[tblLocationTradeMaterial] association
	JOIN tblLocationType location ON association.LocationTypeId = location.LocationTypeId
	JOIN tblTrade trade ON association.TradeId = trade.TradeId
	JOIN tblMaterial material ON association.MaterialId = material.MaterialId

END 
