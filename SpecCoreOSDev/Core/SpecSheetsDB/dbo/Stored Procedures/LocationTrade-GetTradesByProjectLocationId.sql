﻿

CREATE PROCEDURE [dbo].[LocationTrade-GetTradesByProjectLocationId]
@ProjectLocationId int
AS

BEGIN

SELECT DISTINCT TradeId AS TradeId, TradeName AS TradeName

FROM   tblProjectLocationTrade
			 
WHERE tblProjectLocationTrade.ProjectLocationId = @ProjectLocationId


END