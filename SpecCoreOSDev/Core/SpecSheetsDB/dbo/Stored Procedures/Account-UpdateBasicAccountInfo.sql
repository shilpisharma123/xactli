﻿CREATE PROCEDURE [dbo].[Account-UpdateBasicAccountInfo] 
	-- Add the parameters for the stored procedure here

@Email nvarchar(150),
@Name nvarchar(150),
@Phone nvarchar(50),
@AspNetUsersId nvarchar(50)

AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dbo].[tblContact]
		SET [Name] = @Name
			,[Email] = @Email
			,[Phone] = @Phone
						 
		WHERE [AspNetUsersId] = @AspNetUsersId

END