﻿CREATE PROCEDURE [dbo].[Project-GetMaterialById]

@Id int
AS

BEGIN

SELECT
	   [ProjectLocationTradeMaterialId]
	  ,[MaterialId]
      ,[MaterialName]
	  ,[LowBudget]
	  ,[HighBudget]
	  ,[UnitOfMeasure]
	  ,[Quantity]
	  ,[Finish]
	  ,[Cost]
	  ,[LeadTime]
	  ,[Estimate]
	  ,[Actual]
	  ,[Manufacturer]
	  ,[PlanReference]
	  ,[Description]
	  ,[HaveNeedBy]
	  ,[Model]

  FROM [dbo].[tblProjectLocationTradeMaterial]

WHERE ProjectLocationTradeMaterialId = @Id


END


