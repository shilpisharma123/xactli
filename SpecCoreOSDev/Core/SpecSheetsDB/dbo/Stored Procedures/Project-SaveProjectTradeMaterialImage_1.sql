﻿CREATE PROCEDURE [dbo].[Project-SaveProjectTradeMaterialImage]
	@ProjectId int,
	@TradeId int,
	@MaterialId int,
	@Image varbinary(max) 
AS

BEGIN

	INSERT INTO [dbo].[tblProjectTradeMaterialImages]
        ([ProjectId], [TradeId] ,[MaterialId] ,[Image])
    VALUES
        (@ProjectId,@TradeId ,@MaterialId,@Image)

END