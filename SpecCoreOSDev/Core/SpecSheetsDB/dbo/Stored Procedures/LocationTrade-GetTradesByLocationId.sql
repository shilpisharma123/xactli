﻿

CREATE PROCEDURE [dbo].[LocationTrade-GetTradesByLocationId]
@LocationId int
AS

BEGIN

SELECT DISTINCT tblProjectLocationTrade.TradeId AS TradeId, tblProjectLocationTrade.TradeName AS TradeName

FROM tblProjectLocationTrade

WHERE tblProjectLocationTrade.ProjectLocationId = @LocationId

END