﻿CREATE PROCEDURE [dbo].[Project-GetProjectTradeMaterialImages]
	@ProjectId int,
	@TradeId int,
	@MaterialId int
AS
BEGIN
	SELECT [Id],[ProjectId],[TradeId],[MaterialId],[Image] FROM [dbo].[tblProjectTradeMaterialImages]
	WHERE [ProjectId] = @ProjectId AND [TradeId] = @TradeId AND [MaterialId] = @MaterialId
END