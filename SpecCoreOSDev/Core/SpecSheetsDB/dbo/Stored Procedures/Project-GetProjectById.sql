﻿

CREATE PROCEDURE [dbo].[Project-GetProjectById]
@ProjectId int
AS

BEGIN

SELECT [ProjectId]
      ,[ProjectName]
	  ,[OwnerName]
      ,[Address]
      ,[City]
      ,[State]
      ,[Zip]
      ,[Phone]
      ,[Email]
      ,[AspNetUsersId]
  FROM [dbo].[tblProject]

WHERE ProjectId = @ProjectId
END