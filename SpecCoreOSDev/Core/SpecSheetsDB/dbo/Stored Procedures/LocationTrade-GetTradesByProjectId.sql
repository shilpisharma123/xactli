﻿

CREATE PROCEDURE [dbo].[LocationTrade-GetTradesByProjectId]
@ProjectId int
AS

BEGIN

SELECT DISTINCT plt.ProjectLocationTradeID AS TradeId, plt.TradeName AS TradeName

FROM   tblProjectLocation AS location INNER JOIN
             tblProjectLocationTrade AS plt ON location.ProjectLocationId = plt.ProjectLocationID
             
			 
WHERE location.ProjectId = @ProjectId

END