﻿
CREATE PROCEDURE [dbo].[Material-GetMaterials]

AS
BEGIN

SELECT [MaterialId]
      ,[Name]
	  ,[LowBudget]
	  ,[HighBudget]
	  ,[UnitOfMeasure]

  FROM [dbo].[tblMaterial]

END
