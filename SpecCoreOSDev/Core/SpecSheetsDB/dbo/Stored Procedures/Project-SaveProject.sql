﻿
CREATE PROCEDURE [dbo].[Project-SaveProject]

@ProjectId int,
@ProjectName nvarchar(150),
@OwnerName nvarchar(150),
@Address nvarchar(200) = '',
@City nvarchar(100) = '',
@State nvarchar(2) = '',
@Zip nvarchar(15) = '',
@Phone nvarchar(150) = '',
@Email nvarchar(200) = '',
@AspNetusersId nvarchar(450) = ''

AS

BEGIN

IF @ProjectId = 0
BEGIN

INSERT INTO [dbo].[tblProject]
           ([ProjectName]
		   ,[OwnerName]
           ,[Address]
           ,[City]
           ,[State]
           ,[Zip]
           ,[Phone]
           ,[Email]
           ,[AspNetUsersId])
     VALUES
           (@ProjectName
		   ,@OwnerName
           ,@Address
           ,@City
           ,@State
           ,@Zip
           ,@Phone
           ,@Email
           ,@AspNetUsersId)

	SET @ProjectId = SCOPE_IDENTITY();

	INSERT INTO [dbo].[tblProjectAspNetUsers]
           ([ProjectId]
           ,[AspNetUsersId])
     VALUES
           (@ProjectId
           ,@AspNetusersId)

	SELECT @ProjectId;
END
ELSE
BEGIN

UPDATE [dbo].[tblProject]
   SET [ProjectName] = @ProjectName
	  ,[OwnerName] = @OwnerName
      ,[Address] = @Address
      ,[City] = @City
      ,[State] = @State
      ,[Zip] = @Zip
      ,[Phone] = @Phone
      ,[Email] = @Email
      ,[AspNetUsersId] = @AspNetusersId

 WHERE ProjectId = @ProjectId

 SELECT @ProjectId

END


END

