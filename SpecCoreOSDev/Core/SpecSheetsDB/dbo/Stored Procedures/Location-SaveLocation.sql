﻿


CREATE PROCEDURE [dbo].[Location-SaveLocation]

@ProjectLocationId int = 0,
@LocationTypeId int,
@Name nvarchar(150),
@ProjectId int

AS
BEGIN

IF @ProjectLocationId = 0
BEGIN

	INSERT INTO [dbo].[tblProjectLocation]
           ([LocationTypeID]
           ,[Name]
           ,[ProjectId])
     VALUES
           (@LocationTypeId
           ,@Name
           ,@ProjectId)

	SET @ProjectLocationId = SCOPE_IDENTITY();
	EXEC PopulateProjectLocationTradeMaterial_sp @CustomerId=0, @ProjectLocationId= @ProjectLocationId

END

ELSE
BEGIN

UPDATE [dbo].[tblProjectLocation]
   SET [Name] = @Name

 WHERE ProjectLocationId = @ProjectLocationId

 SELECT @ProjectLocationId

END

END

