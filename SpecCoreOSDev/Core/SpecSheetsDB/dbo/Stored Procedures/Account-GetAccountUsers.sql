﻿CREATE PROCEDURE [dbo].[Account-GetAccountUsers]

@AspNetUsersId NVARCHAR(50)

AS

BEGIN

	SELECT [ProjectInvitedContactId], tblProjectInvitedContact.[ProjectId], [tblProjectInvitedContact].[ProjectLocationTradeId], [tblProjectInvitedContact].[ContactId], 
	[tblContact].[Name] AS [ContactName], [InviteStatus], [PermissionLevel], [DateInvited], [DateReceived], ISNULL(tplt.TradeName, 'All') AS [TradeName],
	[tblContact].[Email] AS Email, tblProject.ProjectName

	FROM tblProjectInvitedContact 
		JOIN tblContact
			ON tblProjectInvitedContact.ContactId = tblContact.ContactId
		JOIN tblProject
			ON tblProject.ProjectId = tblProjectInvitedContact.ProjectId
		LEFT JOIN tblProjectLocationTrade tplt
			ON tblProjectInvitedContact.ProjectLocationTradeId = tplt.ProjectLocationTradeId

		WHERE tblProject.AspNetUsersId = @AspNetUsersId



END