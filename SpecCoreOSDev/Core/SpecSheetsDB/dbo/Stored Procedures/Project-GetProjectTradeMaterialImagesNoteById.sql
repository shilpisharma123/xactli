
CREATE PROCEDURE [dbo].[Project-GetProjectTradeMaterialImagesNoteById]
@Id int
AS
BEGIN
SELECT
 Top 1 NoteId,ProjectLocationTradeMaterialImagesId,NoteName,DateTimeStamp
FROM [tblNoteHistory]
WHERE  ProjectLocationTradeMaterialImagesId=@Id
Order By NoteId DESC

END