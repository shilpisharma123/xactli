﻿
CREATE PROCEDURE [dbo].[Project-DeleteInvitedUser]

@InvitedUserId int

AS

BEGIN

DELETE FROM tblProjectAspNetUsers
WHERE ProjectAspNetUsersId IN 

(
	SELECT ProjectAspNetUsersId 
		FROM  tblProjectAspNetUsers JOIN tblProjectInvitedContact
			ON tblProjectAspNetUsers.AspNetUsersId = tblProjectInvitedContact.AspNetUsersId
			AND tblProjectAspNetUsers.ProjectId = tblProjectInvitedContact.ProjectId

	WHERE tblProjectInvitedContact.ProjectInvitedContactId = @InvitedUserId

)


DELETE FROM [dbo].[tblProjectInvitedContact]
      WHERE [dbo].[tblProjectInvitedContact].ProjectInvitedContactId = @InvitedUserId

END